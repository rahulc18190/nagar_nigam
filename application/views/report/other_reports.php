<link rel="stylesheet" href="//code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.0/jquery-ui.js"></script>

<script>
$(function() {
    $( ".month" ).datepicker({
        changeMonth: true,
        changeYear: true,
        minDate: '-50Y',
        maxDate: '+0Y',
        yearRange: '-50:+0',
        dateFormat: 'MM-yy', 
		showButtonPanel: true,

		onClose: function() {
			var iMonth = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
			var iYear = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
			$(this).datepicker('setDate', new Date(iYear, iMonth, 1));
		 },
	
		 beforeShow: function(input) {

		   if ((selDate = $(this).val()).length > 0) 
		   {
			  iYear = selDate.substring(selDate.length - 4, selDate.length);
			  iMonth = jQuery.inArray(selDate.substring(0, selDate.length - 5), 
					   $(this).datepicker('option', 'monthNames'));
			  $(this).datepicker('option', 'defaultDate', new Date(iYear, iMonth, 1));
			  $(this).datepicker('setDate', new Date(iYear, iMonth, 1));
		   }
		   
			setTimeout(function () {
                $(input).datepicker("widget").find(".ui-datepicker-current").hide();
                var clearButton = $(input ).datepicker( "widget" ).find( ".ui-datepicker-close" );
                clearButton.unbind("click").bind("click",function(){$.datepicker._clearDate( input );});
            }, 1 );		   
		}
    });
});

function get_employee2(department_id){
	jQuery.post('<?php echo site_url('report/get_employee') ?>', {department_id : department_id}, function(r) {
		if(r != '') {
			jQuery('#employee_td').html(r);
		}
	});
}

function enable_report_items(report_name){
	
	$('input[type=text]').attr('disabled','disabled');
	$('select').attr('disabled','disabled');
	$('input[type=checkbox]').attr('disabled','disabled');
	$('.report_item').attr('disabled','disabled');
	$('.'+report_name).removeAttr('disabled','disabled');
}

/*$(document).ready(function(){

	setTimeout(function(){
	
		$('.ui-datepicker-curren').hide()
	
	},1000);
});
*/
</script>

<style>
.ui-datepicker-calendar {
	display: none !important;
}​
</style>

<div class="wrapper clear" >   

    <form action="<?php echo site_url('report/get_report'); ?>" method="post">    
    
        <div id="signin">
    
            <div class="signin-header">
    
                <h4 id="signinHeader">in  fjiksVZ </h4>
                <table width="100%" border="0" class="form">
                
                    <tr>
                        <th width="5%" rowspan="4" scope="row">
                        <input type="radio" name="report" id="designation_report"  onchange="enable_report_items('designation_report')" value="designation_report" /></th>
                        <th align="right" scope="row"><input type="radio" name="employee_designation_report" class="report_item designation_report" 
                        id="permanent_employee_designation_report" disabled="disabled" checked="checked" /></th>
                        <th scope="row">&nbsp;</th>
                        <th align="left" scope="row">  LFkkbZ deZpkjh </th>
                    </tr>
                
                    <tr>
                      <th align="right" scope="row"><input type="radio" disabled="disabled" name="employee_designation_report" class="report_item designation_report" 
                      id="temporary_employee_designation_report" /></th>
                      <th scope="row">&nbsp;</th>
                      <th align="left" scope="row">    vLFkkbZ deZpkjh </th>
                    </tr>
                
                    <tr>
                        <th align="right" scope="row"><input type="radio" disabled="disabled" name="employee_designation_report" class="report_item designation_report" 
                        id="samvida_employee_designation_report" /></th>
                        <th scope="row">&nbsp;</th>
                        <th align="left" scope="row">    lafonk deZpkjh </th>
                    </tr>
                
                    <tr>
                        <th width="43%" align="right" scope="row"><input type="checkbox" name="each_department_designation_report" 
                        class="designation_report" 
                        id="each_department_designation_report" disabled="disabled" /></th>
                        <th width="3%" scope="row">&nbsp;</th>
                        <th width="49%" align="left" scope="row">  çR;sd ist ij çR;sd foHkkx </th>
                    </tr>
                
                </table>

            </div>

			<hr />

			<div class="signin-header">

                <h4 id="signinHeader">ekfld deZpkjh fjiksVZ </h4>
    
                <table width="100%" border="0" class="form">
    
                    <tr>
                        <th width="6%" rowspan="3" scope="row">
                        <input type="radio" name="report" id="monthly_employee_report" onchange="enable_report_items('monthly_employee_report')" value="monthly_employee_report" /></th>
                        <th scope="row">foHkkx </th>
                        
                        <td><select name="department_id_monthly_employee_report" id="department_id_monthly_employee_report" class="monthly_employee_report" 
                        onchange="get_employee2(this.value)" disabled="disabled">
                        <option value="lHkh foHkkx">lHkh foHkkx</option>
						<?php
                        if($department){
                            foreach($department as $dep){
                                echo "<option value='".$dep['department_id']."'>".$dep['name']."</option>";
                            }
                        }
                        ?>
                        </select></td>
                    </tr>
    
                    <tr>
                        <th scope="row">deZpkjh </th>
                        <td id="employee_td"><select name="employee_id_monthly_employee_report" id="employee_id_monthly_employee_report" class="monthly_employee_report" 
                         disabled="disabled">
                        <option value="lHkh deZpkjh ">lHkh deZpkjh  </option>
                        </select></td>
                    </tr>
    
                    <tr>
                        <th width="38%" scope="row">eghuk </th>
                        <td width="56%"><input type="text" class="f-loto form-small-select monthly_employee_report month" id="month_from_monthly_employee_report" 
                        name="month_from_monthly_employee_report" disabled="disabled"  value="<?php echo date('F-Y',strtotime('-2 month')); ?>">
						ls <input type="text" class="f-loto form-small-select monthly_employee_report month" id="month_to_monthly_employee_report" 
                        name="month_to_monthly_employee_report" disabled="disabled"  value="<?php echo date('F-Y',strtotime('-1 month')); ?>"></td>
                    </tr>
    
                </table>
    
            </div>
            
            <hr />
    
            <div class="signin-header">
    
                <h4 id="signinHeader">lkekU; fjiksVZ </h4>
                <table width="100%" border="0" class="form">
    
                    <tr>
                        <th width="5%" rowspan="3" scope="row">
                        <input type="radio" name="report" id="general_report" onchange="enable_report_items('general_report')" /></th>
                        <th width="43%" align="right" scope="row"><input type="radio" name="code_general_report" id="department_code_general_report" 
                        class="report_item general_report" disabled="disabled" checked="checked"/></th>
                        <th width="3%" scope="row">&nbsp;</th>
                        <th width="49%" align="left" scope="row">  foHkkx dksM </th>
                    </tr>
    
                    <tr>
                        <th align="right" scope="row"><input type="radio" name="code_general_report" id="employee_code_general_report" 
                        class="report_item general_report" disabled="disabled"/>
                        </th>
                        <th scope="row">&nbsp;</th>
                        <th align="left" scope="row">    deZpkjh dksM</th>
                    </tr>
    
                    <tr>
                        <th align="right" scope="row"><input type="radio" name="code_general_report" id="load_code_general_report" 
                        class="report_item general_report" disabled="disabled" />
                        </th>
                        <th scope="row">&nbsp;</th>
                        <th align="left" scope="row">    yksM dksM </th>
                    </tr>
    
                </table>
    
            </div>
    
            <hr />
    
            <div class="signin-header">
    
                <h4 id="signinHeader">yksu dh foLr`r fjiksVZ </h4>

                <table width="100%" border="0" class="form">
                
                    <tr>
                        <th width="6%" rowspan="2" class="f-loto" scope="row">
                        <input type="radio" name="report" id="loan_full_report" value="loan_full_report" onchange="enable_report_items('loan_full_report')" />
                        </th>
                     
                        <th scope="row">yksu dk uke</th>
                        <td><select class="loan_full_report" id="loan_id_loan_full_report" name="loan_id_loan_full_report" disabled="disabled">
                        	<option value="All">lHkh yksu </option>
							<?php
                            if($loan){
                                foreach($loan as $ln){
                                    echo "<option value='".$ln['loan_id']."'>".$ln['name']."</option>";
                                }
                            }
                            ?>
                        </select></td>
                    
                    </tr>
                
                    <tr>
                        <th width="38%" scope="row">eghuk </th>
                        <td width="56%"><input type="text" class="f-loto loan_full_report month" id="month_loan_full_report" 
                        name="month_loan_full_report" disabled="disabled"  value="<?php echo date('F-Y',strtotime('-1 month')); ?>"></td>
                    </tr>
                
                </table>
    
            </div>

            <hr />
    
            <div class="signin-header">
    
                <h4 id="signinHeader">foHkkx fjiksVZ</h4>
                <table width="100%" border="0" class="form">
                
                    <tr>
                        <th width="6%" rowspan="3" scope="row">
                        <input type="radio" name="report" id="department_report_3" onchange="enable_report_items('department_report_3')" /></th>
                        <th scope="row">&nbsp;</th>
                        <th align="left">osrd i=d
                        <input type="checkbox" name="vetak_patrak_department_report_3" id="vetak_patrak_department_report_3" class="department_report_3" disabled="disabled" />
                        eghuk
                        <input type="text" class="f-loto form-small-select department_report_3 month" id="month_department_report_3" name="month_department_report_3" 
                         value="<?php echo date('F-Y',strtotime('-1 month')); ?>" disabled="disabled" ></th>
                    </tr>
            
                    <tr>
                        <th scope="row">  
                        foHkkx dk uke</th>
                        <td><select name="department_id_department_report_3" id="department_id_department_report_3" class="department_report_3" disabled="disabled" >
                        <option value="lHkh foHkkx">lHkh foHkkx</option>
                        <?php
                        if($department){
                            foreach($department as $dep){
                                echo "<option value='".$dep['department_id']."'>".$dep['name']."</option>";
                            }
                        }
                        ?>
                        </select></td>
                    </tr>
                    
                    <tr>
                        <th width="38%" scope="row">eghuk  </th>
                        <td width="56%"><input type="text" class="f-loto department_report_3 month" id="month_2_department_report_3" name="month_2_department_report_3" 
                         value="<?php echo date('F-Y',strtotime('-1 month')); ?>" disabled="disabled"></td>
                    </tr>

                </table>
    
            </div>
    
            <hr />
            <div class="signin-header">
    
                <h4 id="signinHeader">HkÙkksa dk fooj.k </h4>
                <table width="100%" border="0" class="form">
                
                    <tr>
                        <th width="6%" rowspan="3" scope="row">
                        <input type="radio" name="report" id="allowance_description_report" value="allowance_description_report" onchange="enable_report_items('allowance_description_report')" /></th>
                        <th scope="row">&nbsp;</th>
                    </tr>

                    <tr>
                        <th scope="row">  
                        foHkkx dk uke</th>
                        <td><select name="department_id_allowance_description_report" id="department_id_allowance_description_report" class="allowance_description_report" 
                        disabled="disabled">
                        <option value="All">lHkh foHkkx</option>
                        <?php
                        if($department){
                            foreach($department as $dep){
                                echo "<option value='".$dep['department_id']."'>".$dep['name']."</option>";
                            }
                        }
                        ?>
                        </select></td>
                    </tr>
                    
                    <tr>
                        <th width="38%" scope="row">eghuk  </th>
                        <td width="56%"><input type="text" class="f-loto allowance_description_report month" id="month_allowance_description_report" 
                        name="month_allowance_description_report" disabled="disabled"  value="<?php echo date('F-Y',strtotime('-1 month')); ?>"></td>
                    </tr>

                    <tr>
                        <th scope="row">&nbsp;</th>
                        <th scope="row">&nbsp;</th>
                        <td>&nbsp;</td>
                    </tr>
        
                    <tr >
                        <th align="right" scope="row" colspan="2"><input type="submit" id="fjiksVZ" class="btn btn-primary f-kruti" value="fjiksVZ"></th>
                        <th align="left"><input type="submit" id="submit2" class="btn btn-primary f-kruti" value="Dykst "></th>
                    </tr>
    
                </table>

            </div>

        </div>

    </form>

</div>