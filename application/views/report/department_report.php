<style>

@media print {

	#header, #common-banner, #bottom-ftr, #button-tr, #back-button, #topcontrol	{
		display:none;
	}
	body {
		padding-top:0px !important;
	}
	#signin {
		margin-top:15px !important;
	}
}
</style>

<div class="wrapper clear" >   

    <div id="signin">

        <h2 id="signinHeader" class="f-kruti cnt-head" align="center">foHkkx fjiksVZ</h2>
		
        <div class="f-kruti">
        	<div><?php echo ($department	!=	'') ? 'foHkkx dk uke '.$department : ''; ?></div>
        	<div>eghuk <?php echo "<span class='f-loto'>".$month."</span>"; ?></div>
        	<div>rkjh[k <?php echo "<span class='f-loto'>".date('d-m-Y')."</span>"; ?></div>
        </div>
        
        <div class="f-loto" align="right" id="back-button">
        	<a href="<?php echo site_url('report/reports'); ?>">Back</a>
        </div>
        
        <hr />
        
        <div class="signin-header">
        
            <table width="100%" border="0" class="form cus-tbl2">
            	
                <tbody>
                
                    <tr class="row-1">
						
                        <th>Sr.</th>
                        <th>deZpkjh dk uke</th>
                        <th>in dk uke</th>
                        <th>osru + Hk�ks</th>
                        <th>dVks=k</th>
                        <th class="f-loto">Total</th>
                        	   
                    </tr>
                    
                    <?php
					if(!empty($department_report)){
					
						foreach($department_report as $report){

							$salary_plus_allowance	=	$report['special_salary']+
														$report['dearly_allowance']+	
														$report['home_rent_allowance']+	
														$report['city_damage_allowance']+	
														$report['medical_allowance']+	
														$report['interim_relief']+	
														$report['travelling_allowance']+
														$report['current_salary'];

							$katotra				=	$report['GPF']+
														$report['GPF_Loan']+	
														$report['family_benifit_fund']+	
														$report['grain_loan']+	
														$report['professional_tax']+	
														$report['bima_premium']+	
														$report['CTD']+	
														$report['mo_cycle_loan']+	
														$report['cycle_loan']+	
														$report['house_loan']+	
														$report['napa_bhandar']+	
														$report['harijan_pedi']+	
														$report['home_rent']+	
														$report['water_rent']+	
														$report['electricity_rent'];	

							echo "<tr>";
								echo "<th class='row-2'>".$count."</th>";
								echo "<th class='row-2'>".$report['employee_name']."</th>";
								echo "<th class='row-2'>".$report['name']."</th>";
								echo "<th class='row-2'>".$salary_plus_allowance."</th>";
								echo "<th class='row-2'>".$katotra."</th>";
								echo "<th class='row-2'>".($salary_plus_allowance-$katotra)."</th>";
							echo "</tr>";
							$count++;
						} ?>

                        <tr id="button-tr">
                            <th colspan="2">
                                <input type="button" value="Print" class="btn btn-primary" onblur="window.print();">

                                <input type="button" value="Export PDF" class="btn btn-primary" 
                                onclick="generate_pdf();">

                                <input type="button" value="Export Excel" class="btn btn-primary" 
                                onclick="generate_excel();">
                            </th>
                            <th align="right" class="f-loto" colspan="2"><?php echo $links; ?></th>
                        </tr>
                    
                        <script>
                        function generate_pdf(){
                            window.top.location	=	'<?php echo site_url('report/generate_pdf/department_report'); ?>';
                        }
                        function generate_excel(){
                            window.top.location	=	'<?php echo site_url('report/generate_excel/department_report'); ?>';
                        }
                        </script>

				<?php } else { ?>                    
            		
                    <tr><td colspan="4" class="f-loto">No record found.</td></tr>
                    
				<?php } ?>                    

            	</tbody>	
                    
            </table>
                                   
        </div>
        
    </div>

</div>
