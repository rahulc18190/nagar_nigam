<style>

@media print {

	#header, #common-banner, #bottom-ftr, #button-tr, #back-button, #topcontrol	{
		display:none;
	}
	body {
		padding-top:0px !important;
	}
	#signin {
		margin-top:15px !important;
	}
}
</style>

<div class="wrapper clear" >   

    <div id="signin">

        <h2 id="signinHeader" class="f-kruti cnt-head" align="center">foHkkx fjiksVZ</h2>
		
        <div class="f-kruti">
        	<div>eghuk <?php echo "<span class='f-loto'>".$month."</span>"; ?></div>
        	<div>rkjh[k <?php echo "<span class='f-loto'>".date('d-m-Y')."</span>"; ?></div>
        </div>
        
        <div class="f-loto" align="right" id="back-button">
        	<a href="<?php echo site_url('report/reports'); ?>">Back</a>
        </div>
        
        <hr />
        
        <div class="signin-header">
        
            <table width="100%" border="0" class="form cus-tbl2">
            	
                <tbody>
                
                    <tr class="row-1">
						
                        <th>Sr.</th>
                        <th>foHkkx dk uke</th>
                        <th class="f-loto">Total Payment</th>
                        <th>th-ih-,Q yksu</th>
                        <th>QSfeyh csfufQV Q.M</th>
                        <th>xzsu yksu</th>
                        <th class="f-loto">Other Katotra</th>
                        <th class="f-loto">Cash Payment</th>
                    </tr>
                    
                    <?php
					foreach($department_report2 as $report){

						echo "<tr>";
							echo "<th class='row-2'>".$count."</th>";
							echo "<th class='row-2'>".$report['department_name']."</th>";
							echo "<th class='row-2'>".$report['total_payment']."</th>";
							echo "<th class='row-2'>".$report['GPF_Loan']."</th>";
							echo "<th class='row-2'>".$report['family_benifit_fund']."</th>";
							echo "<th class='row-2'>".$report['grain_loan']."</th>";
							echo "<th class='row-2'>".$report['other_katotra']."</th>";
							echo "<th class='row-2'>".$report['cash_payment']."</th>";
						echo "</tr>";
						$count++;
					} ?>

                        <tr id="button-tr">
                            <th colspan="2">
                                <input type="button" value="Print" class="btn btn-primary" onblur="window.print();">

                                <input type="button" value="Export PDF" class="btn btn-primary" 
                                onclick="generate_pdf();">

                                <input type="button" value="Export Excel" class="btn btn-primary" 
                                onclick="generate_excel();">
                            </th>
                            <th align="right" class="f-loto" colspan="2"><?php echo $links; ?></th>
                        </tr>
                    
                        <script>
                        function generate_pdf(){
                            window.top.location	=	'<?php echo site_url('report/generate_pdf/department_report'); ?>';
                        }
                        function generate_excel(){
                            window.top.location	=	'<?php echo site_url('report/generate_excel/department_report'); ?>';
                        }
                        </script>
                  

            	</tbody>	
                    
            </table>
                                   
        </div>
        
    </div>

</div>
