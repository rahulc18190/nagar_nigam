<link rel="stylesheet" href="//code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.0/jquery-ui.js"></script>

<script>

$(function() {
    $(".month" ).datepicker({
        changeMonth: true,
        changeYear: true,
        minDate: '-50Y',
        maxDate: '+0Y',
        yearRange: '-50:+0',
        dateFormat: 'MM-yy', 
		showButtonPanel: true,

		onClose: function() {
			var iMonth = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
			var iYear = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
			$(this).datepicker('setDate', new Date(iYear, iMonth, 1));
		 },
	
		 beforeShow: function() {

		   if ((selDate = $(this).val()).length > 0) 
		   {
			  iYear = selDate.substring(selDate.length - 4, selDate.length);
			  iMonth = jQuery.inArray(selDate.substring(0, selDate.length - 5), 
					   $(this).datepicker('option', 'monthNames'));
			  $(this).datepicker('option', 'defaultDate', new Date(iYear, iMonth, 1));
			  $(this).datepicker('setDate', new Date(iYear, iMonth, 1));
		   }		   
		}
    });
});

function get_employee(department_id){
	jQuery.post('<?php echo site_url('report/get_employee') ?>', {department_id : department_id}, function(r) {
		if(r != '') {
			jQuery('#employee_td').html(r);
		}
	});
}

function enable_report_items(report_name){
	
	$('input[type=text]').attr('disabled','disabled');
	$('select').attr('disabled','disabled');
	$('input[type=checkbox]').attr('disabled','disabled');
	$('.report_item').attr('disabled','disabled');
	$('.'+report_name).removeAttr('disabled','disabled');
}

</script>

<style>
.ui-datepicker-calendar {
    display: none !important;
}​
</style>

<form action="<?php echo site_url('report/get_report'); ?>" method="post">    

    <div class="wrapper clear" >   

        <div id="signin">

            <h2 id="signinHeader" class="f-kruti cnt-head" align="center">fjiksVZ</h2>
            <hr />
            
            <div class="signin-header">
            
                <h4 id="signinHeader">cSad fjiksVZ </h4>
            
                <table width="100%" border="0" class="form">
                
                    <tr>
                        <th width="6%" rowspan="2" class="f-loto" scope="row">
                        <input type="radio" class="radio" name="report" id="bank_report" onchange="enable_report_items('bank_report')" value="bank_report" required='required'/>
                        </th>
                     
                        <th scope="row">cSad dk uke</th>
                        <td><select name="bank_id_bank_report" id="bank_id_bank_report" class="bank_report" disabled="disabled">
                        <option value="All">lHkh cSad</option>
							<?php
                            if($bank){
                                foreach($bank as $bnk){
                                    echo "<option value='".$bnk['bank_id']."'>".$bnk['name']."</option>";
                                }
                            }
                            ?>
                        </select></td>
                    
                    </tr>
                
                    <tr>
                        <th width="38%" scope="row">eghuk </th>
                        <td width="56%"><input type="text" class="f-loto bank_report month" id="month_bank_report" name="month_bank_report" 
                        value="<?php echo date('F-Y',strtotime('-1 month')); ?>" disabled="disabled" ></td>
                    </tr>
                
                </table>
            
            </div>
            
            <hr />
                    
            <div class="signin-header">

                <h4 id="signinHeader">foHkkx ¼deZpkjh½ fjiksVZ </h4>

                <table width="100%" border="0" class="form">
                
                    <tr>
                        <th width="6%" rowspan="2" scope="row">
                        <input type="radio" class="radio" name="report" id="department_report" onchange="enable_report_items('department_report')" value="department_report" required='required' /></th>
                        <th scope="row">foHkkx dk uke  </th>
                        <td><select name="department_id_department_report" id="department_id_department_report" class="department_report" disabled="disabled" >
                        <option value="All">lHkh foHkkx </option>
							<?php
                            if($department){
                                foreach($department as $dep){
                                    echo "<option value='".$dep['department_id']."'>".$dep['name']."</option>";
                                }
                            }
                            ?>
                        </select></td>
                    </tr>

                    <tr>
                        <th width="38%" scope="row">eghuk  </th>
                        <td width="56%"><input type="text" class="f-loto department_report month" id="month_department_report" name="month_department_report" disabled="disabled" 
                         value="<?php echo date('F-Y',strtotime('-1 month')); ?>">
                        </td>
                    </tr>

                </table>

            </div>

            <hr />

            <div class="signin-header">

                <h4 id="signinHeader">foHkkx fjiksVZ</h4>
                <table width="100%" border="0" class="form">
                
                    <tr>
                        <th width="6%" rowspan="3" scope="row">
                        <input type="radio" class="radio" name="report" id="department_report_2" 
                         value="department_report_2" onchange="enable_report_items('department_report_2')" 
                         required='required' /></th>
                        <th scope="row">&nbsp;</th>
                        <td><input type="checkbox" name="vetak_patrak_department_report_2" id="vetak_patrak_department_report_2" class="department_report_2" disabled="disabled" value="1" />
                        osrd i=d</td>
                    </tr>

                    <tr>
                        <th scope="row">  
                        foHkkx dk uke</th>
                        <td><select name="department_department_report_2" id="department_department_report_2" disabled="disabled" class="department_report_2">
                        <option value="All">lHkh foHkkx </option>
						<?php
                        if($department){
                            foreach($department as $dep){
                                echo "<option value='".$dep['department_id']."'>".$dep['name']."</option>";
                            }
                        }
                        ?>
                        </select></td>
                    </tr>
                
                    <tr>
                        <th width="38%" scope="row">eghuk  </th>
                        <td width="56%"><input type="text" class="f-loto department_report_2 month" id="month_department_report_2" name="month_department_report_2" 
                        disabled="disabled"  value="<?php echo date('F-Y',strtotime('-1 month')); ?>"></td>
                    </tr>

                </table>

            </div>

            <hr />

            <div class="signin-header">

                <h4 id="signinHeader">is Lyhi </h4>

                <table width="100%" border="0" class="form">

                    <tr>
                        <th width="6%" rowspan="3" scope="row">
                        <input type="radio" class="radio" name="report" id="pay_sleep_report" onchange="enable_report_items('pay_sleep_report')" value="pay_sleep_report" required='required' /></th>
                        <th scope="row">foHkkx dk uke </th>
                        <td><select name="department_id_pay_sleep_report" id="department_id_pay_sleep_report" class="pay_sleep_report" onchange="get_employee(this.value)" disabled="disabled">
                                <option value="All">lHkh foHkkx </option>
								<?php
                                if($department){
                                    foreach($department as $dep){
                                        echo "<option value='".$dep['department_id']."'>".$dep['name']."</option>";
                                    }
                                }
                                ?>
                        </select></td>
                    </tr>
                    
                    <tr>
                        <th scope="row">deZpkjh </th>
                        <td id="employee_td"><select name="employee_id_pay_sleep_report" class="pay_sleep_report" id="employee_id_pay_sleep_report" disabled="disabled" >
                        <option value="All">lHkh deZpkjh </option>
						<?php
                        if($employee){
                            foreach($employee as $emp){
                                echo "<option value='".$emp['employee_id']."'>".$emp['name']."</option>";
                            }
                        }
                        ?>
                        </select></td>
                    </tr>

                    <tr>
                        <th width="38%" scope="row">eghuk </th>
                        <td width="56%"><input type="text" class="f-loto pay_sleep_report month" id="month_pay_sleep_report" name="month_pay_sleep_report" disabled="disabled"
                         value="<?php echo date('F-Y',strtotime('-1 month')); ?>"></td>                    
					</tr>

                </table>

            </div>

            <hr />
            
            <div class="signin-header">

                <h4 id="signinHeader">dVks=k fjiksVZ </h4>
                
                <table width="100%" border="0" class="form">
                
                    <tr>
                        <th width="6%" rowspan="2" scope="row">
                        <input type="radio" class="radio" name="report" id="katotra_report" value="katotra_report" onchange="enable_report_items('katotra_report')" 
                        required='required' /></th>
                        <th scope="row"></th>
                        <td><input type="checkbox" name="department_wise_katotra_report" id="department_wise_katotra_report" value="1" class="katotra_report" disabled="disabled"/>
                        &nbsp; foHkkx Øekuqlkj</td>
                    </tr>
                
                    <tr>
                        <th width="38%" scope="row">eghuk  </th>
                        <td width="56%"><input type="text" class="f-loto katotra_report month" id="month_katotra_report" name="month_katotra_report" disabled="disabled" 
                         value="<?php echo date('F-Y',strtotime('-1 month')); ?>"></td>
                    </tr>
                
                </table>
            
            </div>

            <hr />

            <div class="signin-header">

                <h4 id="signinHeader">yksu fjiksVZ </h4>

                <table width="100%" border="0" class="form">

                    <tr>
                      <th scope="row">&nbsp;</th>
                      <th scope="row">&nbsp;</th>
                      <td>&nbsp;</td>
                    </tr>

                    <tr>
                        <th width="6%" scope="row">
                        <input type="radio" class="radio" name="report" id="loan_report" value="loan_report" onchange="enable_report_items('loan_report')" 
                        required='required' /></th>
                        <th width="38%" scope="row">eghuk  </th>
                        <td width="56%"><input type="text" class="f-loto loan_report month" id="month_loan_report" name="month_loan_report" disabled="disabled" 
                         value="<?php echo date('F-Y',strtotime('-1 month')); ?>"></td>
                    </tr>
                    
                    <tr>
                        <th colspan="3" align="center" scope="row">&nbsp;</th>
                    </tr>

                    <tr>
                        <th colspan="3" align="center" scope="row">
                            <input type="submit" id="submit3" class="btn btn-primary f-kruti" value="fjiksVZ">
                            <input type="submit" id="submit2" class="btn btn-primary f-kruti" value="Dykst">&nbsp;
                        </th>
                    </tr>

                </table>

            </div>

        </div>

    </div>

</form>