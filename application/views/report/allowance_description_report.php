<style>

@media print {

	#header, #common-banner, #bottom-ftr, #button-tr, #back-button, #topcontrol	{
		display:none;
	}
	body {
		padding-top:0px !important;
	}
	#signin {
		margin-top:15px !important;
	}
}
</style>

<div class="wrapper clear" >   

    <div id="signin">

        <h2 id="signinHeader" class="f-kruti cnt-head" align="center">Hk�kksa dk fooj.k</h2>
		
        <div class="f-kruti">
        	<div>eghuk <?php echo "<span class='f-loto'>".$month."</span>"; ?></div>
        	<div>rkjh[k <?php echo "<span class='f-loto'>".date('d-m-Y')."</span>"; ?></div>
        </div>
        
        <div class="f-loto" align="right" id="back-button">
        	<a href="<?php echo site_url('report/reports'); ?>">Back</a>
        </div>
        
        <hr />
        
        <div class="signin-header">
        
        	<?php
        	foreach($allowance_description_report as $report){
        	
				echo '<div class="f-kruti">'.$report['department_name'].'</div>';
				?>
                
				<table width="100%" border="0" class="form cus-tbl2">
					
					<tbody>
					
						<tr class="row-2">
							<th class="f-loto">Mul Betan</th>
							<th><?php echo $report['current_salary']; ?></th>
							<th>egaxkbZ Hk�kk</th>
							<th><?php echo $report['dearly_allowance']; ?></th>
							<th>esfMdy Hk�kk</th>
							<th><?php echo $report['medical_allowance']; ?></th>
						</tr>
	
						<tr class="row-2">
							<th class="f-loto">Mahangai Betan</th>
							<th><?php echo $mayhangai_betan	=	0; ?></th>
							<th>x`g fdjk;k Hk�kk</th>
							<th><?php echo $report['home_rent_allowance']; ?></th>
							<th>varfje jkgr</th>
							<th><?php echo $report['interim_relief']; ?></th>
						</tr>
	
						<tr class="row-2">
							<th class="f-loto">Naya mul betan</th>
							<th><?php echo $report['current_salary']+$mayhangai_betan; ?></th>
							<th>uxj {kfriwfrZ Hk�kk</th>
							<th><?php echo $report['city_damage_allowance']; ?></th>
							<th>okgu Hk�kk</th>
							<th><?php echo $report['travelling_allowance']; ?></th>
						</tr>
	
						<tr class="row-2">
							<th>fo'ks"k osru</th>
							<th><?php echo $report['special_salary']; ?></th>
							<th class="">Vyaktigat Vetan</th>
							<th><?php echo 0; ?></th>
							<th colspan="2"></th>
						</tr>
	
					</tbody>	
						
				</table>
			
            <?php
            }
			?>

			<table>
            	
                <tr id="button-tr">

                    <th colspan="2">

                        <input type="button" value="Print" class="btn btn-primary" onblur="window.print();">

                        <input type="button" value="Export PDF" class="btn btn-primary" 
                        onclick="generate_pdf();">

                    </th>

                    <th align="right" class="f-loto" colspan="2"><?php echo $links; ?></th>

                </tr>
                
                <script>
                function generate_pdf(){
                    window.top.location	=	'<?php echo site_url('report/generate_pdf/bank_report'); ?>';
                }
                function generate_excel(){
                    window.top.location	=	'<?php echo site_url('report/generate_excel/bank_report'); ?>';
                }
                </script>                  

            </table>
                                               
        </div>
        
    </div>

</div>