<style>

@media print {

	#header, #common-banner, #bottom-ftr, #button-tr, #back-button, #topcontrol	{
		display:none;
	}
	body {
		padding-top:0px !important;
	}
	#signin {
		margin-top:15px !important;
	}
}
</style>

<div class="wrapper clear" >   

    <div id="signin">

        <h2 id="signinHeader" class="f-kruti cnt-head" align="center">yksu dh foLr`r fjiksVZ</h2>
		
        <div class="f-kruti">
        	<div><?php echo ($loan	!=	'') ? 'yksu dk uke '.$loan : ''; ?></div>
        	<div>eghuk <?php echo "<span class='f-loto'>".$month."</span>"; ?></div>
        	<div>rkjh[k <?php echo "<span class='f-loto'>".date('d-m-Y')."</span>"; ?></div>
        </div>
        
        <div class="f-loto" align="right" id="back-button">
        	<a href="<?php echo site_url('report/reports'); ?>">Back</a>
        </div>
        
        <hr />
        
        <div class="signin-header">
        
            <table width="100%" border="0" class="form cus-tbl2">
            	
                <tbody>
                
                    <tr class="row-1">
						
                        <th class="f-loto">Sr.</th>
                        <th class="f-loto">Code</th>
                        <th>deZpkjh dk uke</th>
                        <?php
                        echo ($loan == '') ? '<th>yksu dk uke </th>' : '' ;
						?>
                        <th class="f-loto">Amount</th>
                        	   
                    </tr>
                    
                    <?php
					if(!empty($loan_full_report)){
					
						foreach($loan_full_report as $report){
							echo "<tr>";
								echo "<th class='row-2'>".$count."</th>";
								echo "<th class='row-2'>".$report['id']."</th>";
								echo "<th class='row-2'>".$report['employee_name']."</th>";
								echo ($loan == '') ? '<th class="row-2">'.$report['loan_name'].'</th>' : '' ;
								echo "<th class='row-2'>".$report['amount']."</th>";
							echo "</tr>";
							$count++;
						} ?>

                        <tr id="button-tr">
                            <th colspan="2">
                                <input type="button" value="Print" class="btn btn-primary" onblur="window.print();">

                                <input type="button" value="Export PDF" class="btn btn-primary" 
                                onclick="generate_pdf();">

                                <input type="button" value="Export Excel" class="btn btn-primary" 
                                onclick="generate_excel();">
                            </th>
                            <th align="right" class="f-loto" colspan="2"><?php echo $links; ?></th>
                        </tr>
                    
                        <script>
                        function generate_pdf(){
                            window.top.location	=	'<?php echo site_url('report/generate_pdf/bank_report'); ?>';
                        }
                        function generate_excel(){
                            window.top.location	=	'<?php echo site_url('report/generate_excel/bank_report'); ?>';
                        }
                        </script>

				<?php } else { ?>                    
            		
                    <tr><td colspan="4" class="f-loto">No record found.</td></tr>
                    
				<?php } ?>                    

            	</tbody>	
                    
            </table>
                                   
        </div>
        
    </div>

</div>
