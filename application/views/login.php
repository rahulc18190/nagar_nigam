<button type="button" class="btn btn-primary" onclick="window.top.location='<?php echo site_url('home/developer_login'); ?>';">Developer Login</button>

<form action="<?php echo base_url().'home/login'; ?>" method="post">
	
    <div class="wrapper clear" >   

        <div id="signin">
    
            <div class="signin-header">
                    
                <table width="100%" border="0" class="login">
                
                    <?php if($this->session->flashdata('failure_msg')){ ?>

                        <tr>
                            <th colspan="2" class="f-loto" scope="row">
                            	<div class="alert alert-danger">
                                	<?php echo $this->session->flashdata('failure_msg'); ?>
                                </div>
                            </th>
                        </tr>
							
					<?php } ?>
                    
                    <tr>
                        <td class="f-loto">Username</td>
                        <td>
                            <input type="text" name="username" id="username" required="required">
                        </td>
                    </tr>
                    
                    <tr>
                        <td scope="row" class="f-loto">Password</td>
                        <td>
                            <input type="password" name="password" id="password" required="required">
                        </td>
                    </tr>
                
                    <tr>
                        <td colspan="2" align="center">
                            <input type="submit" name="login" id="login" class="btn btn-primary" value="Login">
                        </td>
                    </tr>
                
                </table>
    
            </div>
    
        </div>
    
    </div>

</form>
