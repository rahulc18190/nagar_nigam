<table width="100%">
    <tr class="f-loto all-entry row-1">
        <td>Sr.</td>
        <td>Designation Name</td>
        <td>Department</td>
        <td>Class</td>
        <td>Action</td>
    </tr>

	<?php
	if($designation){
		foreach($designation as $key=>$val){
			echo "<tr class='all-entry' id='item-".$val['id']."'>";
				echo "<td>".($key+1)."</td>";
				echo "<td>".$val['name']."</td>";
				echo "<td>".$val['department']."</td>";
				echo "<td>".$val['class']."</td>";
				echo "<td>							
					<button type='button' class='f-loto' onClick='view_full_detail(".$val['id'].");' value='View'>View</button>
					<button type='button' class='f-loto' onClick='delete_item(".$val['id'].");' value='Delete'>Delete</button>
				</td>";
			echo "</tr>";
	?>

            <tr class='personal-entry' id='personal-entry-<?php echo $val['id']; ?>' style='display:none;'>
                <td colspan='5'>
                    <table width="100%">
                        <tr><td colspan="2" align="right"><a href="#" class="f-loto" onClick="back_to_all(<?php echo $val['id']; ?>)">Back</a></td>
                        </tr>                        
                        <tr><td colspan="2">&nbsp;</td></tr>
                        <tr><td colspan="2"><hr /></td></tr>
                        <tr><td colspan="2" class="cnt-head">in fooj.k</td></tr>
                        <tr><td colspan="2">&nbsp;</td></tr>
                        
                        <tr>
                            <td>in dk uke  </td>
                            <td><?php echo $val['name']; ?></td>
                        </tr>
                        <tr>
                            <td>foHkkx dk uke  </td>
                            <td><?php echo $val['department']; ?></td>
                        </tr>
                        <tr>
                            <td>Js.kh   </td>
                            <td><?php echo $val['class']; ?></td>
                        </tr>
                        
                        <tr><td colspan="2">&nbsp;</td></tr>
                        <tr><td colspan="2"><hr /></td></tr>
                        <tr><td colspan="2" class="cnt-head">Hk�ks</td></tr>
                        <tr><td colspan="2">&nbsp;</td></tr>
                        
                        <tr>
                            <td>fo'ks"k osru   </td>
                            <td><?php echo $val['special_salary']; ?></td>
                        </tr>
        
                        <tr>
                            <td>egaxkbZ Hk�kk  </td>
                            <td><?php echo $val['dearly_allowance']; ?></td>
                        </tr>
        
                        <tr>
                            <td>x`g fdjk;k Hk�kk  </td>
                            <td><?php echo $val['home_rent_allowance']; ?></td>
                        </tr>
        
                        <tr>
                            <td>uxj {kfriwfrZ Hk�kk   </td>
                            <td><?php echo $val['city_damage_allowance']; ?></td>
                        </tr>
        
                        <tr>
                            <td>esfMdy Hk�kk   </td>
                            <td><?php echo $val['medical_allowance']; ?></td>
                        </tr>
        
                        <tr>
                            <td>varfje jkgr  </td>
                            <td><?php echo $val['interim_relief']; ?></td>
                        </tr>
        
                        <tr>
                            <td>okgu Hk�kk  </td>
                            <td><?php echo $val['travelling_allowance']; ?></td>
                        </tr>
        
                        <tr><td colspan="2">&nbsp;</td></tr>
                        <tr><td colspan="2"><hr /></td></tr>
                        <tr><td colspan="2" class="cnt-head">osru Js.kh</td></tr>
                        <tr><td colspan="2">&nbsp;</td></tr>
                        
                        <tr>
                            <td>U;wure   </td>
                            <td><?php echo $val['minimum_salary']; ?></td>
                        </tr>
        
                        <tr>
                            <td>osru o`f) �fr o"kZ   </td>
                            <td><?php echo $val['salary_increment_per_year']; ?></td>
                        </tr>
        
                        <tr>
                            <td>vf/kdre   </td>
                            <td><?php echo $val['maximum_salary']; ?></td>
                        </tr>
        
                        <tr>
                            <td>f}rh; osru o`f)   </td>
                            <td><?php echo $val['second_increment']; ?></td>
                        </tr>
        
                        <tr>
                            <td>f}rh; vf/kdre   </td>
                            <td><?php echo $val['second_increment_maximum']; ?></td>
                        </tr>
        
                        <tr>
                            <td>6 osru lewg   </td>
                            <td><?php echo $val['6_pay_group_id']; ?></td>
                        </tr>
        
                        <tr><td colspan="2">&nbsp;</td></tr>
                        <tr><td colspan="2"><hr /></td></tr>
                        <tr><td colspan="2" class="cnt-head">dVks=k</td></tr>
                        <tr><td colspan="2">&nbsp;</td></tr>
        
                        <tr>
                            <td>th-ih Q.M  </td>
                            <td><?php echo $val['GPF']; ?></td>
                        </tr>
        
                        <tr>
                            <td>th-ih-,Q yksu  </td>
                            <td><?php echo $val['GPF_Loan']; ?></td>
                        </tr>
        
                        <tr>
                            <td>QSfeyh csfufQV Q.M  </td>
                            <td><?php echo $val['family_benifit_fund']; ?></td>
                        </tr>
        
                        <tr>
                            <td>xzsu yksu  </td>
                            <td><?php echo $val['grain_loan']; ?></td>
                        </tr>
        
                        <tr>
                            <td>�ksQsluy VSDl   </td>
                            <td><?php echo $val['professional_tax']; ?></td>
                        </tr>
        
                        <tr>
                            <td>chek �hfe;e   </td>
                            <td><?php echo $val['bima_premium']; ?></td>
                        </tr>
        
                        <tr>
                            <td>lh-Vh-Mh  </td>
                            <td><?php echo $val['CTD']; ?></td>
                        </tr>
        
                        <tr>
                            <td>eks- lkbdy yksu  </td>
                            <td><?php echo $val['mo_cycle_loan']; ?></td>
                        </tr>
        
                        <tr>
                            <td>lkbdy yksu  </td>
                            <td><?php echo $val['cycle_loan']; ?></td>
                        </tr>
        
                        <tr>
                            <td>gkml yksu  </td>
                            <td><?php echo $val['house_loan']; ?></td>
                        </tr>
        
                        <tr>
                            <td>u-ik HkaMkj  </td>
                            <td><?php echo $val['napa_bhandar']; ?></td>
                        </tr>
        
                        <tr>
                            <td>gfjtu is??h  </td>
                            <td><?php echo $val['harijan_pedi']; ?></td>
                        </tr>
        
                        <tr>
                            <td>x`g fdjk;k  </td>
                            <td><?php echo $val['home_rent']; ?></td>
                        </tr>
        
                        <tr>
                            <td>ikuh fdjk;k  </td>
                            <td><?php echo $val['water_rent']; ?></td>
                        </tr>
        
                        <tr>
                            <td>fctyh fdjk;k  </td>
                            <td><?php echo $val['electricity_rent']; ?></td>
                        </tr>
        
                    </table>
                </td>
            </tr>
        
		<?php
        }
    }
    ?>
    
</table>	
