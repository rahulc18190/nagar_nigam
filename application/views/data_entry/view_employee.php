<?php if($this->session->userdata('department_id')){ ?>
	
	<script>
	
		$(document).ready(function(){
			
			get_employees('<?php echo $this->session->userdata('department_id'); ?>',0);								
		});								
	
    </script>

<?php } ?>

<script>

function edit_employee(id){
	
	window.top.location	=	'<?php echo site_url('data_entry/add_employee'); ?>/'+id;	
}

function view_full_detail(id){
	
	$('.all-entry').hide();
	$('.pagination-td').hide();							
	$('#personal-entry-'+id).show();
}

function delete_item(id){

	if(confirm("Are you sure you want to delete the item")	==	true){

		jQuery.post('<?php echo site_url('data_entry/delete_item') ?>', {id : id, table : 'employee'}, function(r) {
			if(r != '') {
				$('#item-'+id).hide();
				alert('Recorded successfully deleted.');
			}
		});	
	}
}

function back_to_all(id){
	
	$('.all-entry').show();
	$('.pagination-td').show();							
	$('#personal-entry-'+id).hide();
}

function get_employees(id,status){

	jQuery.post('<?php echo site_url('data_entry/get_employee') ?>', {department_id : id,status:status}, function(r) {
		if(r != '') {
			jQuery('#employee_th').html(r);
		}
	});	
}
</script>

<div class="wrapper clear">

	<div id="list-item">
    
        <table class="form-list-item cus-tbl2">

            <?php if($this->session->flashdata('success_msg')){ ?>

                <tr>
                    <th colspan="6" class="f-loto" scope="row">
                        <div class="alert alert-success">
                            <?php echo $this->session->flashdata('success_msg'); ?>
                        </div>
                    </th>
                </tr>
                    
            <?php } ?>
            
            <?php if($this->session->flashdata('failure_msg')){ ?>

                <tr>
                    <th colspan="6" class="f-loto" scope="row">
                        <div class="alert alert-danger">
                            <?php echo $this->session->flashdata('failure_msg'); ?>
                        </div>
                    </th>
                </tr>
                    
            <?php } ?>
            
            <tr>
				
                <th colspan="2" class="f-loto list-title">Employee List</th>

                <th scope="row" class="f-loto">Operations</th>

                <th>
                    <select name="operation" id="operation" class="f-loto" onChange="change_operation(this.value,'employee');">
                        <option value="add" >Add</option>
                        <option value="view" selected="selected">View</option>
                    </select>
                </th>

                <th scope="row" class="f-loto">Select Department</th>

                <th>
                    <select name="department_id" id="department_id" class="f-kruti" onChange="get_employees(this.value,1);">
                        
                        <option value=""></option>
						
                        <?php foreach($department as $val){ ?>
								<option value='<?php echo $val['department_id']; ?>' 
								<?php echo ($this->session->userdata('department_id') && $this->session->userdata('department_id')	==	$val['department_id']) ? 'selected="selected"' : '' ;?>>
								<?php echo $val['name']; ?></option>	
						<?php } ?>
                        
                    </select>
                </th>
                
			</tr>

            <tr>
                <th colspan="6" scope="row"><hr /></th>
            </tr>
        
            <!--                

            <tr>

				<th scope="row" class="f-loto">Employee Name</th>

                <th>
                	<form method="post" action="<?php echo site_url('data_entry/get_employee'); ?>">
                        <input type="text" name="employee_name" id="employee_name">
                    </form>
                </th>

                <th scope="row" class="f-loto">Employee Code</th>

                <th>
                	<form method="post" action="<?php echo site_url('data_entry/get_employee'); ?>">
                        <input type="text" name="employee_id" id="employee_id">
                    </form>
                </th>

            </tr>

            --> 

            <tr>
                <th colspan="6" scope="row"><hr /></th>
            </tr>
            
            <tr>
            	<th colspan="6" scope="row" id="employee_th"></th>
            </tr>
        
        </table>

    
    </div>

</div>
