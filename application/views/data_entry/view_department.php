<script>
function delete_item(id){

	if(confirm("Are you sure you want to delete the item")	==	true){

		jQuery.post('<?php echo site_url('data_entry/delete_item') ?>', {id : id, table : 'department'}, function(r) {
			if(r != '') {
				$('#item-'+id).hide();
				alert('Recorded successfully deleted.');
			}
		});	
	}
}

</script>
<div class="wrapper clear" >   

	<div id="list-item">
    
        <table class="form-list-item cus-tbl2">
                
			<?php if($this->session->flashdata('success_msg')){ ?>

                <tr>
                    <th colspan="3" class="f-loto" scope="row">
                        <div class="alert alert-success">
                            <?php echo $this->session->flashdata('success_msg'); ?>
                        </div>
                    </th>
                </tr>
                    
            <?php } ?>
            
            <?php if($this->session->flashdata('failure_msg')){ ?>

                <tr>
                    <th colspan="3" class="f-loto" scope="row">
                        <div class="alert alert-danger">
                            <?php echo $this->session->flashdata('failure_msg'); ?>
                        </div>
                    </th>
                </tr>
                    
            <?php } ?>
                
            <tr>
				
                <th class="f-loto list-title">Department List</th>

                <th colspan="2" scope="row" class="f-loto">
                	
                    Operations
                
                    <select name="operation" id="operation" class="f-loto" onChange="change_operation(this.value,'department');">
                        <option value="add" >Add</option>
                        <option value="view" selected="selected">View</option>
                    </select>
                </th>
                
			</tr>

            <tr>
                <th colspan="3" scope="row"><hr /></th>
            </tr>

            <tr>
                <th colspan="3" scope="row"><hr /></th>
            </tr>

            <tr class="f-loto row-1">
                <th>Sr.</th>
                <th>Department Name</th>
                <th>Action</th>
            </tr>

            <?php
            if($department){
                foreach($department as $key=>$val){
                    echo "<tr class='row-2' id='item-".$val['id']."'>";
                        echo "<th>".($key+1)."</th>";
                        echo "<th>".$val['name']."</th>";
                        echo "<th>
                                <button type='button' class='f-loto' onClick='delete_item(".$val['id'].");' value='Delete'>Delete</button>
                            </th>";
                    echo "</tr>";
                }
            }
            ?>
        
            <!--<tr><th colspan="3" align="right"><?php //echo $links; ?></th></tr>-->

        </table>
              
    </div>

</div>
