<?php if($this->session->userdata('department_id2')){ ?>
	
	<script>
	
		$(document).ready(function(){
			
			get_designation('<?php echo $this->session->userdata('department_id2'); ?>',0);								
		});								
	
    </script>

<?php } ?>


<script>

function delete_item(id){

	if(confirm("Are you sure you want to delete the item")	==	true){

		jQuery.post('<?php echo site_url('data_entry/delete_item') ?>', {id : id, table : 'designation'}, function(r) {
			if(r != '') {
				$('#item-'+id).hide();
				alert('Recorded successfully deleted.');
			}
		});	
	}
}

function view_full_detail(id){
	$('.all-entry').hide();
	$('.pagination-td').hide();							
	$('#personal-entry-'+id).show();
}
function back_to_all(id){
	$('.all-entry').show();
	$('.pagination-td').show();							
	$('#personal-entry-'+id).hide();
}
function get_designation(id,status){

	jQuery.post('<?php echo site_url('data_entry/get_designation_by_department_id') ?>', {department_id : id,status:status}, function(r) {
		if(r != '') {
			jQuery('#designation_th').html(r);
		}
	});	
}
</script>
                
<div class="wrapper clear" >   

	<div id="list-item">
    
        <table class="form-list-item cus-tbl2">
                
			<?php if($this->session->flashdata('success_msg')){ ?>

                <tr>
                    <th colspan="5" class="f-loto" scope="row">
                        <div class="alert alert-success">
                            <?php echo $this->session->flashdata('success_msg'); ?>
                        </div>
                    </th>
                </tr>
                    
            <?php } ?>
            
            <?php if($this->session->flashdata('failure_msg')){ ?>

                <tr>
                    <th colspan="5" class="f-loto" scope="row">
                        <div class="alert alert-danger">
                            <?php echo $this->session->flashdata('failure_msg'); ?>
                        </div>
                    </th>
                </tr>
                    
            <?php } ?>
                
            <tr>
				
                <th colspan="2" class="f-loto list-title">Designation List</th>

                <th scope="row" class="f-loto">Operations</th>

                <th>
                    <select name="operation" id="operation" class="f-loto" onChange="change_operation(this.value,'designation');">
                        <option value="add" >Add</option>
                        <option value="view" selected="selected">View</option>
                    </select>
                </th>

                <th scope="row" class="f-loto">Select Department</th>

                <th>
                    <select name="department_id" id="department_id" class="f-kruti" onChange="get_designation(this.value,1);">
                        
                        <option value=""></option>
						
                        <?php foreach($department as $val){ ?>
								<option value='<?php echo $val['department_id']; ?>' 
								<?php echo ($this->session->userdata('department_id2') && $this->session->userdata('department_id2')	==	$val['department_id']) ? 'selected="selected"' : '' ;?>>
								<?php echo $val['name']; ?></option>	
						<?php } ?>
                        
                    </select>
                </th>
                
			</tr>

            <tr>
                <th colspan="6" scope="row"><hr /></th>
            </tr>

            <tr>
                <th colspan="6" scope="row"><hr /></th>
            </tr>
            
            <tr>
            	<th colspan="6" scope="row" id="designation_th"></th>
            </tr>
                
        </table>
    
    </div>

</div>