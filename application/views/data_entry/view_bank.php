<script>
function delete_item(id){

	if(confirm("Are you sure you want to delete the item")	==	true){

		jQuery.post('<?php echo site_url('data_entry/delete_item') ?>', {id : id, table : 'bank'}, function(r) {
			if(r != '') {
				$('#item-'+id).hide();
				alert('Recorded successfully deleted.');
			}
		});	
	}
}

</script>
<div class="wrapper clear" >   

	<div id="list-item">
    
        <table class="form-list-item cus-tbl2">
            
            <?php if($this->session->flashdata('success_msg')){ ?>

                <tr>
                    <th colspan="6" class="f-loto" scope="row">
                        <div class="alert alert-success">
                            <?php echo $this->session->flashdata('success_msg'); ?>
                        </div>
                    </th>
                </tr>
                    
            <?php } ?>
            
            <?php if($this->session->flashdata('failure_msg')){ ?>

                <tr>
                    <th colspan="6" class="f-loto" scope="row">
                        <div class="alert alert-danger">
                            <?php echo $this->session->flashdata('failure_msg'); ?>
                        </div>
                    </th>
                </tr>
                    
            <?php } ?>
            
            <tr>
				
                <th class="f-loto list-title">Bank List</th>

                <th colspan="5" scope="row" class="f-loto">
                	
                    Operations
                
                    <select name="operation" id="operation" class="f-loto" onChange="change_operation(this.value,'bank');">
                        <option value="add" >Add</option>
                        <option value="view" selected="selected">View</option>
                    </select>
                </th>
                
			</tr>

            <tr>
                <th colspan="6" scope="row"><hr /></th>
            </tr>

            <tr>
                <th colspan="6" scope="row"><hr /></th>
            </tr>
          
                
            <tr class="f-loto row-1">
                <td>Sr.</td>
                <td>Bank Name</td>
                <td>Branch</td>
                <td>Address</td>
                <td>Phone</td>
                <td>Action</td>
            </tr>
                
			<?php
            if($bank){
                foreach($bank as $key=>$val){
                    echo "<tr id='item-".$val['id']."'>";
                        echo "<td>".($key+1)."</td>";
                        echo "<td>".$val['name']."</td>";
                        echo "<td>".$val['branch']."</td>";
                        echo "<td>".$val['address']."</td>";
                        echo "<td>".$val['phone_number']."</td>";
                        echo "<td>
                                <button type='button' class='f-loto' onClick='delete_item(".$val['id'].");' value='Delete'>Delete</button>
                            </td>";
                    echo "</tr>";
                }
            }
            ?>
                            
        </table>
        
    </div>

</div>
