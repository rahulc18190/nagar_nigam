<table width="100%">
    <tr class="f-loto all-entry row-1">
        <td>Sr.</td>
        <td>Name</td>
        <td>Department</td>
        <td>Designation</td>
        <td>Designation Type</td>
        <td>Phone</td>
        <td>Action</td>
    </tr>

	<?php
    if($employee){
        foreach($employee as $key=>$val){
            echo "<tr class='all-entry' id='item-".$val['id']."'>";
                echo "<td>".($key+1)."</td>";
                echo "<td>".$val['name']."</td>";
                echo "<td>".$val['department_name']."</td>";
                echo "<td>".$val['designation_name']."</td>";
                echo "<td>";
                echo ($val['designation_type'] == 'T') ? 'Temperory' : 'Permanent' ;
                echo "</td>";
                echo "<td>".$val['std_code'].'-'.$val['phone_number']."</td>";
                echo "<td>
                        <button type='button' class='f-loto' onClick='view_full_detail(".$val['id'].");' value='View'>View</button>
                        <button type='button' class='f-loto' onClick='edit_employee(".$val['id'].");' value='Edit'>Edit</button>
                        <button type='button' class='f-loto' onClick='delete_item(".$val['id'].");' value='Delete'>Delete</button>
                    </td>";
            echo "</tr>";
        ?>

        <tr class='personal-entry' id='personal-entry-<?php echo $val['id']; ?>' style='display:none;'>
            <td colspan='7'>
                <table width="100%">
                    <tr><td colspan="2" align="right"><a href="#" class="f-loto" onClick="back_to_all(<?php echo $val['id']; ?>)">Back</a></td></tr>
                    
                    <tr><td colspan="2">&nbsp;</td></tr>
                    <tr><td colspan="2"><hr /></td></tr>
                    <tr><td colspan="2" class="cnt-head f-loto">Personal information</td></tr>
                    <tr><td colspan="2">&nbsp;</td></tr>
                    
                    <tr>
                        <td>deZpkjh vf/kdkjh uke</td>
                        <td><?php echo $val['name']; ?></td>
                    </tr>
                    <tr>
                        <td>fiV/ikuh dk uke </td>
                        <td><?php echo $val['father_or_husband_name']; ?></td>
                    </tr>
                    <tr>
                        <td>????? </td>
                        <td><?php echo $val['department_name']; ?></td>
                    </tr>
                    <tr>
                        <td>deZpkjh dk in </td>
                        <td><?php echo $val['designation_name']; ?></td>
                    </tr>
                    <tr>
                        <td>in Vkbi </td>
                        <td><?php echo ($val['designation_type'] == 'T') ? 'Temperory' : 'Permanent'; ?></td>
                    </tr>
                    <tr>
                        <td>in xzg.k frfFk </td>
                        <td><?php echo $val['date_of_joining']; ?></td>
                    </tr>
                    <tr>
                        <td>tUe rkjh[k </td>
                        <td><?php echo $val['date_of_birth']; ?></td>
                    </tr>
                    <tr>
                        <td>LFkkuh; irk </td>
                        <td><?php echo $val['address']; ?></td>
                    </tr>
                    <tr>
                        <td>Qksu uacj </td>
                        <td><?php echo $val['std_code'].' - '.$val['phone_number']; ?></td>
                    </tr>
                    <tr>
                        <td>fyax </td>
                        <td><?php echo $val['gender']; ?></td>
                    </tr>
                    <tr>
                        <td><span class="f-loto">open balance</span> </td>
                        <td><?php echo $val['open_balance']; ?></td>
                    </tr>
                    <tr>
                        <td>osru iqjk.k / u;k </td>
                        <td><?php echo $val['current_salary']; ?></td>
                    </tr>
                    <tr>
                        <td>osru o`f) eghuk </td>
                        <td><?php echo $val['increment_month']; ?></td>
                    </tr>
                    <tr>
                        <td><span class="f-loto">Six basic</span></td>
                        <td><?php echo $val['SixBasic']; ?></td>
                    </tr>

                    <tr>
                        <td>6 csl osru </td>
                        <td><?php echo $val['six_bas_pay']; ?></td>
                    </tr>
                    <tr>
                        <td><span class="f-loto"> Old basic </span> </td>
                        <td><?php echo $val['old_basic']; ?></td>
                    </tr>
                    <tr>
                        <td><span class="f-loto"> Old base pay </span> </td>
                        <td><?php echo $val['old_basepay']; ?></td>
                    </tr>

                    <tr>
                        <td><span class="f-loto"> Entry date </span></td>
                        <td><?php echo date('d-m-Y',strtotime($val['current_time'])); ?></td>
                    </tr>

                    <tr>
                        <td scope="row">osru o`f)</td>
                        <td><form><input type="radio" name="increment_applicable" id="increment_applicable" value="Y" disabled="disabled" 
                         <?php echo ($val['increment_applicable']	==	'Y') ? 'checked="checked"' : '' ; ?> /> ns; &nbsp;
                        <input type="radio" name="increment_applicable" id="increment_applicable2" value="N" disabled="disabled" 
                         <?php echo ($val['increment_applicable']	==	'N') ? 'checked="checked"' : '' ; ?> /> ns; ????</form></td>
                    </tr>
                
                    <tr>
                        <td colspan="2"  scope="row">
                        <input type="checkbox" name="sanvida_employee" id="sanvida_employee" value="Y" 
                        <?php echo ($val['six_pay_apply']	==	'Y') ? 'checked="checked"' : '' ; ?> disabled="disabled" />6 is ykxq  
                        <input type="checkbox" name="sanvida_employee" id="sanvida_employee" value="Y" 
                        <?php echo ($val['sanvida_employee']	==	'Y') ? 'checked="checked"' : '' ; ?> disabled="disabled" />lafonk deZpkjh 
                        <input type="checkbox" name="maintenance_allowance" id="maintenance_allowance" value="Y" 
                        <?php echo ($val['maintenance_allowance']	==	'Y') ? 'checked="checked"' : '' ; ?> disabled="disabled" /> fuokZg Hk�kk</td>
                    </tr>
                


                    <tr><td colspan="2">&nbsp;</td></tr>
                    <tr><td colspan="2"><hr /></td></tr>
                    <tr><td colspan="2" class="cnt-head f-loto">Bank Account Information</td></tr>
                    <tr><td colspan="2">&nbsp;</td></tr>

                    <tr>
                        <td>cSad dk uke </td>
                        <td><?php echo $val['bank_name']; ?></td>
                    </tr>

                    <tr>
                        <td>czkap dk uke </td>
                        <td><?php echo $val['branch']; ?></td>
                    </tr>

                    <tr>
                        <td>vdkmaV uacj </td>
                        <td><?php echo $val['bank_account_number']; ?></td>
                    </tr>

                    <tr>
                        <td>isu uacj </td>
                        <td><?php echo $val['pan_number']; ?></td>
                    </tr>
                </table>
            </td>                                                                            
        </tr>
        <?php
    }
}
?>
</table>	
