<link rel="stylesheet" href="//code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.0/jquery-ui.js"></script>

<script>

$(function() {
    $( "#date_of_joining" ).datepicker({
        changeMonth: true,
        changeYear: true,
        minDate: '-10Y',
        maxDate: '+1Y',
        yearRange: '-10:+1',
        dateFormat: 'dd-mm-yy', 
    });
});

$(function() {
    $( "#date_of_birth" ).datepicker({
        changeMonth: true,
        changeYear: true,
        minDate: '-60Y',
        maxDate: '+1Y',
        yearRange: '-60:+1',
        dateFormat: 'dd-mm-yy', 
    });
});

function get_pay_band(designation_id) {
	jQuery.post('<?php echo site_url('data_entry/get_pay_band') ?>', {designation_id : designation_id}, function(r) {
		if(r != '') {

			var arr	=	r.split('-');
			jQuery('#employee_pay_band').val(r);
			jQuery('#employee_pay_band').attr('readonly','readonly');
			jQuery('#grade_pay').val(arr[2]);
			jQuery('#grade_pay').attr('readonly','readonly');
			get_six_basic();
		}
	});
}

function get_six_basic(){

	jQuery('#six_basic').val(Number(jQuery('#six_base_pay').val())+Number(jQuery('#grade_pay').val()));
	jQuery('#six_basic').attr('readonly','readonly');
}

$(document).ready(function(){
	jQuery('#six_pay_apply').click(function(){
		$('#6th_pay_td').toggle();
	});
});

function get_branch(bank_name) {
	jQuery.post('<?php echo site_url('data_entry/get_branch') ?>', {bank_name : bank_name}, function(r) {
		if(r != '') {
			jQuery('#branch_td').html(r);
		}
	});
}

function get_designation(department_id) {
	jQuery.post('<?php echo site_url('data_entry/get_designation') ?>', {department_id : department_id}, function(r) {
		if(r != '') {
			jQuery('#designation_td').html(r);
		}
	});
}
</script>            

<form action="<?php echo ($employee_data != '') ? site_url('data_entry/edit_employee') : site_url('data_entry/add_item/employee') ; ?>" method="post">

    <div class="wrapper clear" >   

        <div id="signin">
                
            <div class="signin-header">

                <table width="100%" border="0" class="form">

                    <?php if($this->session->flashdata('success_msg')){ ?>

                        <tr>
                            <th colspan="2" class="f-loto" scope="row">
                            	<div class="alert alert-success">
                                	<?php echo $this->session->flashdata('success_msg'); ?>
                                </div>
                            </th>
                        </tr>
							
					<?php } ?>
                    
                    <?php if($this->session->flashdata('failure_msg')){ ?>

                        <tr>
                            <th colspan="2" class="f-loto" scope="row">
                            	<div class="alert alert-danger">
                                	<?php echo $this->session->flashdata('failure_msg'); ?>
                                </div>
                            </th>
                        </tr>
							
					<?php } ?>

                    <tr>
                        <th colspan="2" class="f-loto" scope="row"><h2 id="signinHeader" class="f-kruti cnt-head" align="center">deZpkjh *,M^</h2> </th>
                    </tr>
                  
                    <tr>
                        <th scope="row" class="f-loto">Operations</th>
                        <td>
                            <select name="operation" id="operation" class="f-loto" onChange="change_operation(this.value,'employee');">
                                
								<option value="add" <?php echo ($employee_id != '') ? 'selected="selected"' : ''; ?>>Add</option>
                                <option value="view">View</option>
								<?php echo ($employee_id == '') ? '<option value="edit" selected="selected">Edit</option>' : ''; ?>
                            </select>
                        </td>
                    </tr>
                  
                    <tr>
                        <th colspan="2" align="left" scope="row"><hr /></th>
                    </tr>
                    
                    <tr>
                        <th colspan="2" align="left" scope="row"><h4 id="signinHeader">deZpkjh dk fooj.k</h4></th>
                    </tr>
   

                    <tr>
                        <th colspan="2" scope="row">&nbsp;</th>
                    </tr>
                
                    <tr>
                        <th scope="row">deZpkjh dk uacj </th>
                        <td><input type="text" name="employee_id" id="employee_id" readonly="readonly" value="<?php echo ($employee_id != '') ? $employee_id : $employee_data->employee_id; ?>"></td>
                    </tr>
                
                    <tr>
                        <th width="48%" scope="row">deZpkjh vf/kdkjh uke</th>
                        <td width="52%"><select class="form-small-select" id="name_prefix" name="name_prefix">
                        <option value="Jh" <?php echo ($employee_data->name_prefix	==	'Jh') ? 'selected="selected"' : '' ; ?>>Jh</option>
                        <option value="Jhs" <?php echo ($employee_data->name_prefix	==	'Jhs') ? 'selected="selected"' : '' ; ?>>Jhs</option>
                        </select><input type="text" class="half-txtbox" name="name" id="name" value="<?php echo $employee_data->name; ?>"></td>
                    </tr>
                
                    <tr>
                        <th scope="row">fiV/ikuh dk uke</th>
                        <td><input type="text" name="father_or_husband_name" id="father_or_husband_name" value="<?php echo ($employee_data != '') ? $employee_data->father_or_husband_name : 'Jh '; ?>"></td>
                    </tr>
                
                    <tr>
                        <th scope="row">विभाग</th>
                        <td><select name="department_id" id="department_id"  onchange="get_designation(this.value)">
                        <option value=""></option>
                        <?php
                        if($department){
							foreach($department as $dep){ ?>

								<option <?php echo ($employee_data->department_id	==	$dep['department_id']) ? 'selected="selected"' : '' ; ?> 
                                value='<?php echo $dep['department_id']; ?>'><?php echo $dep['name']; ?></option>
							
							<?php }
						}
						?>
                        </select></td>
                    </tr>
                
                    <tr>
                        <th  scope="row">deZpkjh dk in</th>
                        <td id="designation_td"><select name="designation_id" id="designation_id" onchange="get_pay_band(this.value)">
                        <option value=""></option>
                        <?php
						if($employee_data){
							if($designation){
								foreach($designation as $des){ ?>
	
									<option <?php echo ($employee_data->designation_id	==	$des['designation_id']) ? 'selected="selected"' : '' ; ?> 
									value='<?php echo $des['designation_id']; ?>'><?php echo $des['name']; ?></option>
								
								<?php }
							}
						}
						?>
                        </select></td>
                    </tr>
                
                    <tr>
                        <th scope="row" class="f-loto">Employee's Pay Bad</th>
                        <td><input type="text" id="employee_pay_band" class="f-loto" value=""></td>
                    </tr>
                
                    <tr>
                        <th scope="row">in Vkbi</th>
                        <td><select id="designation_type" name="designation_type">
                        <option <?php echo ($employee_data->designation_type	==	'P') ? 'selected="selected"' : '' ; ?> value="P">ijekusaV</option>
                        <option <?php echo ($employee_data->designation_type	==	'T') ? 'selected="selected"' : '' ; ?> value="T">temperory</option>
                        </select></td>
                    </tr>

                    <tr>
                        <th scope="row">in xzg.k frfFk</th>
                        <td><input type="text" name="date_of_joining" id="date_of_joining" class="f-loto" value="<?php echo ($employee_data != '') ? date('d-m-Y',strtotime($employee_data->date_of_joining)) : date('d-m-Y') ; ?>">
                        </td>
                    </tr>
                
                    <tr>
                        <th  scope="row">tUe rkjh[k</th>
                        <td ><input type="text" class="f-loto" name="date_of_birth" id="date_of_birth" value="<?php echo ($employee_data != '') ? date('d-m-Y',strtotime($employee_data->date_of_birth)) : ''; ?>"/></td>
                    </tr>
                
                    <tr>
                        <th  scope="row">LFkkuh; irk</th>
                        <td ><textarea name="address" id="address"><?php echo $employee_data->address; ?></textarea></td>
                    </tr>
                
                    <tr>
                        <th  scope="row">fyax</th>
                        <td ><select id="gender" name="gender">
                        <option <?php echo ($employee_data->gender	==	'M') ? 'selected="selected"' : '' ; ?> value="M">iqfYyax</option>
                        <option <?php echo ($employee_data->gender	==	'F') ? 'selected="selected"' : '' ; ?> value="F">female</option>
                        </select></td>
                    </tr>
                
                    <tr>
                        <th  scope="row">Qksu uacj</th>
                        <td ><input type="text" class="morehalf-txtbox f-loto" name="std_code" id="std_code" value="<?php echo ($employee_data) ? $employee_data->std_code : '0734'; ?>"> 
                        <span class="f-loto"> - </span>
                        <input type="text" class="morehalf-txtbox f-loto" name="phone_number" id="phone_number" value="<?php echo $employee_data->phone_number; ?>"></td>
                    </tr>
                
                    <tr>
                        <th  scope="row">osru iqjk.k <span class="f-loto"> / </span> u;k </th>
                        <td ><input type="text" class="morehalf-txtbox f-loto" name="fifth_basic" id="fifth_basic"  value="<?php echo $employee_data->current_salary; ?>"/> &nbsp;                        
                            <input type="checkbox" name="six_pay_apply" id="six_pay_apply" value="Y" <?php echo ($employee_data->six_pay_apply	==	'Y') ? 'checked="checked"' : '' ; ?> />  6 is ykxq
                        </td> 
                    </tr>

                    <tr id="6th_pay_td" style="display:none;">
                        <th  scope="row">6 csl osru</th>
                        <td ><input type="text" class="morehalf-txtbox f-loto" value="" name='six_base_pay' id="six_base_pay" onchange="get_six_basic()"> <span class="f-loto"> + </span>
                        <input type="text" class="morehalf-txtbox f-loto" value="" id="grade_pay"> <span class="f-loto"> = </span>
                        <input type="text" class="morehalf-txtbox f-loto" value="" name="six_basic" id="six_basic"></td>
                    </tr>
                
                    <tr>
                        <th  scope="row">osru o`f) eghuk</th>
                        <td ><select class="f-loto" name="increment_month" id="increment_month">
                        <option value="June 2014">June 2014</option>
                        </select></td>
                    </tr>
                
                    <tr>
                        <th scope="row">osru o`f)</th>
                        <td><input type="radio" name="increment_applicable" id="increment_applicable" value="Y" <?php echo ($employee_data->increment_applicable	==	'Y') ? 'checked="checked"' : '' ; ?> /> ns; </br>
                        <input type="radio" name="increment_applicable" id="increment_applicable2" value="N" <?php echo ($employee_data->increment_applicable	==	'N') ? 'checked="checked"' : '' ; ?>/> ns; नहीं</td>
                    </tr>
                
                    <tr>
                        <th colspan="2"  scope="row"><input type="checkbox" name="sanvida_employee" id="sanvida_employee" value="Y" <?php echo ($employee_data->sanvida_employee	==	'Y') ? 'checked="checked"' : '' ; ?>/>lafonk deZpkjh 
                        <input type="checkbox" name="maintenance_allowance" id="maintenance_allowance" value="Y" <?php echo ($employee_data->maintenance_allowance	==	'Y') ? 'checked="checked"' : '' ; ?>/> fuokZg HkÙkk</th>
                    </tr>
                
                    <tr>
                        <th colspan="2"  scope="row"><hr /></th>
                    </tr>

                </table>

                <h4 id="signinHeader">cSad vdkmaV fooj.k</h4>
                <table width="100%" border="0" class="form">
    
                    <tr>
                        <th width="48%" scope="row">cSad dk uke</th>
                        <td width="52%"><select onchange="get_branch(this.value)">
							<option></option>
							<?php
                            if($bank){
                                foreach($bank as $bnk){ ?>
                                    <option <?php echo ($employee_data->bank_id	==	$bnk['bank_id']) ? 'selected="selected"' : '' ; ?> value='<?php echo $bnk['name']; ?>'><?php echo $bnk['name']; ?></option>
                                <?php }
                            }
                            ?>
                        </select></td>
                    </tr>

                    <tr>
                        <th  scope="row">czkap dk uke </th>
                        <td id="branch_td"><select name="bank_id" id="bank_id">
                                <option></option>
								<?php
								if($branch){
									foreach($branch as $brc){ ?>
										<option <?php echo ($employee_data->bank_id	==	$brc['bank_id']) ? 'selected="selected"' : '' ; ?> value='<?php echo $brc['bank_id']; ?>'><?php echo $brc['branch']; ?></option>
								<?php }
								}
                                ?>
                        </select></td>
                    </tr>
                
                    <tr>
                        <th height="21"  scope="row">vdkmaV uacj</th>
                        <td ><input type="text" name="bank_account_number" id="bank_account_number" class="f-loto" value="<?php echo $employee_data->bank_account_number; ?>"/></td>
                    </tr>
                
                    <tr>
                        <th align=""  scope="row">isu uacj </th>
                        <td ><input type="text" name="pan_number" id="pan_number" class="f-loto" value="<?php echo $employee_data->pan_number; ?>"/></td>
                    </tr>
                
                    <tr>
                        <th align="right"  scope="row"><input type="submit" value="<?php echo ($employee_data != '') ? 'Edit' : ',M"' ; ?>" class="btn btn-primary f-kruti" id="submit" /></th>
                        <td ><input type="submit" value="Dykst " class="btn btn-primary f-kruti" id="submit4" /></td>
                    </tr>

				</table>

			</div>

		</div>

	</div>

</form>
