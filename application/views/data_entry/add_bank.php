<script>
function bank_list(){
	$('.bank_list').show();
}
function get_name(name){
	$('#name').val(name);
	$('.bank_list').hide();						
}
</script>

<form action="<?php echo site_url('data_entry/add_item/bank'); ?>" method="post">

    <div class="wrapper clear" >   
    
        <div id="signin">
        
            <div class="signin-header">
            
                <table width="100%" border="0" class="form">
                    
                    <?php if($this->session->flashdata('success_msg')){ ?>

                        <tr>
                            <th colspan="2" class="f-loto" scope="row">
                            	<div class="alert alert-success">
                                	<?php echo $this->session->flashdata('success_msg'); ?>
                                </div>
                            </th>
                        </tr>
							
					<?php } ?>
                    
                    <?php if($this->session->flashdata('failure_msg')){ ?>

                        <tr>
                            <th colspan="2" class="f-loto" scope="row">
                            	<div class="alert alert-danger">
                                	<?php echo $this->session->flashdata('failure_msg'); ?>
                                </div>
                            </th>
                        </tr>
							
					<?php } ?>
                    
                    <tr>
                        <th colspan="2" class="f-loto" scope="row"><h2 id="signinHeader" class="f-kruti cnt-head" align="center">cSad fooj.k *,M^</h2> </th>
                    </tr>

                    <tr>
                        <th scope="row" class="f-loto">Operations</th>
                        <td>
                            <select name="operation" id="operation" class="f-loto" onChange="change_operation(this.value,'bank');">
                                <option selected="selected" value="add">Add</option>
                                <option value="view">View</option>
                            </select>
                        </td>
                    </tr>
                  
                    <tr>
                        <th colspan="2" align="left" scope="row"><hr /></th>
                    </tr>
                    
                    <tr>
                        <th colspan="2" align="left" scope="row"><h4 id="signinHeader">cSad dk fooj.k </h4></th>
                    </tr>
                    
                    <tr>
                        <th width="48%" scope="row">cSad dk uke&nbsp;</th>
                        <td width="52%">
                        	<input type="text" name="name" id="name"/>
                    		<a href="#" class="button large small-red db f-loto" onclick="bank_list()">>></a>    
                    	</td>
                    </tr>

                    <tr class="bank_list" style="display:none">
                        <th width="48%" scope="row">बैंक का चयन</th>
                        <td width="52%">
                            <select name="bankid" id="bankid" onChange="get_name(this.value);">
                                <option></option>
                                <?php
                                if($bank){
                                    foreach($bank as $bnk){
                                        echo "<option value='".$bnk['name']."'>".$bnk['name']."</option>";
                                    }
                                }
                                ?>
                            </select>
                    	</td>
                    </tr>

                    <tr>
                        <th scope="row">czkp dk uke</th>
                        <td><input type="text" name="branch" id="branch" /></td>
                    </tr>
                    
                    <tr>
                        <th scope="row">cSad dk irk</th>
                        <td><textarea rows="12" name="address" id="address" ></textarea></td>
                    </tr>
                    
                    <tr>
                        <th scope="row">Qksu uacj</th>
                        <td>
                        	<input type="text" name="std_code" id="std_code" class="half-txtbox" value="0734"/>-
                        	<input type="text" name="phone_number" id="phone_number" class="half-txtbox"/>     
						</td>
                    </tr>
                    
                    <tr>
                        <th align="right" scope="row"><input type="submit" value=",M" class="btn btn-primary f-kruti" id="submit" name="submit" /></th>
                        <td align="left"><input type="submit" value="Dykst" class="btn btn-primary f-kruti" id="submit2" name="submit2" /></td>
                    </tr>
                
                </table>
            
            </div>
        
        </div>
    
    </div>

</form>