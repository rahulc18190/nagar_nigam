<form action="<?php echo site_url('data_entry/add_item/employee_loan'); ?>" method="post">

    <div class="wrapper clear" >   
    
        <div id="signin">
        
            <div class="signin-header">
            
                <table width="100%" border="0" class="form">
                    
                    <?php if($this->session->flashdata('success_msg')){ ?>

                        <tr>
                            <th colspan="2" class="f-loto" scope="row">
                            	<div class="alert alert-success">
                                	<?php echo $this->session->flashdata('success_msg'); ?>
                                </div>
                            </th>
                        </tr>
							
					<?php } ?>
                    
                    <?php if($this->session->flashdata('failure_msg')){ ?>

                        <tr>
                            <th colspan="2" class="f-loto" scope="row">
                            	<div class="alert alert-danger">
                                	<?php echo $this->session->flashdata('failure_msg'); ?>
                                </div>
                            </th>
                        </tr>
							
					<?php } ?>
                    
                    <tr>
                        <th colspan="2" class="f-loto" scope="row"><h2 id="signinHeader" class="f-kruti cnt-head" align="center">deZpkjh yksu *,M^</h2> </th>
                    </tr>

                    <tr>
                        <th scope="row" class="f-loto">Operations</th>
                        <td>
                            <select name="operation" id="operation" class="f-loto" onChange="change_operation(this.value,'bank');">
                                <option selected="selected" value="add">Add</option>
                                <option value="view">View</option>
                            </select>
                        </td>
                    </tr>
                  
                    <tr>
                        <th colspan="2" align="left" scope="row"><hr /></th>
                    </tr>
                    
                    <tr>
                        <th colspan="2" align="left" scope="row"><h4 id="signinHeader">yksu dk fooj.k </h4></th>
                    </tr>
                    
                    <tr>
                        <th width="48%" scope="row">deZpkjh dk uke&nbsp;</th>
                        <td width="52%">
                        	<select name="employee_id" id="employee_id"/>
                                <option></option>
                                <?php
                                if($loan){
                                    foreach($employee as $emp){
                                        echo "<option value='".$emp['employee_id']."'>".$emp['name']."</option>";
                                    }
                                }
                                ?>
                            </select>
                    	</td>
                    </tr>

                    <tr>
                        <th width="48%" scope="row">yksu dk uke</th>
                        <td width="52%">
                            <select name="loan_id" id="loan_id">
                                <option></option>
                                <?php
                                if($loan){
                                    foreach($loan as $ln){
                                        echo "<option value='".$ln['loan_id']."'>".$ln['name']."</option>";
                                    }
                                }
                                ?>
                            </select>
                    	</td>
                    </tr>

                    <tr>
                        <th scope="row">eghuk</th>
                        <td><input type="text" class="f-loto month" id="month" value="<?php echo date('F-Y'); ?>" disabled="disabled" ></td>
                    </tr>
                    
                    <tr>
                        <th scope="row" class="f-loto">Amount</th>
                        <td><input type="text" id="amount" name="amount" ></td>
                    </tr>
                    
                    <tr>
                        <th align="right" scope="row"><input type="submit" value=",M" class="btn btn-primary f-kruti" id="submit" /></th>
                        <td align="left"><input type="submit" value="Dykst" class="btn btn-primary f-kruti" id="submit2" /></td>
                    </tr>
                
                </table>
            
            </div>
        
        </div>
    
    </div>

</form>