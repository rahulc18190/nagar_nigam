<script>
function get_vetan_shreni(six_pay_grade_id){

	jQuery.post('<?php echo site_url('data_entry/get_vetan_shreni') ?>', {six_pay_grade_id : six_pay_grade_id}, function(r) {
		if(r != '') {

			var eData = JSON.parse(r);

			jQuery('#minimum_salary').val(eData['minimum_salary']);		
			jQuery('#salary_increment_per_year').val(eData['increment']);		
			jQuery('#maximum_salary').val(eData['maximum_salary']);		
			jQuery('#second_increment').val(eData['increment_2']);		
			jQuery('#second_increment_maximum').val(eData['maximum_salary_2']);		
		}
	});	
}
</script>

<form action="<?php echo site_url('data_entry/add_item/designation'); ?>" method="post">

    <div class="wrapper clear" >   

        <div id="signin">
            <h2 id="signinHeader" class="f-kruti cnt-head" align="center" >in</h2><hr />
            <div class="signin-header">

                <table width="100%" border="0" class="form">

                    <?php if($this->session->flashdata('success_msg')){ ?>

                        <tr>
                            <th colspan="2" class="f-loto" scope="row">
                            	<div class="alert alert-success">
                                	<?php echo $this->session->flashdata('success_msg'); ?>
                                </div>
                            </th>
                        </tr>
							
					<?php } ?>
                    
                    <?php if($this->session->flashdata('failure_msg')){ ?>

                        <tr>
                            <th colspan="2" class="f-loto" scope="row">
                            	<div class="alert alert-danger">
                                	<?php echo $this->session->flashdata('failure_msg'); ?>
                                </div>
                            </th>
                        </tr>
							
					<?php } ?>
                    
                    <tr>
                    
                        <th scope="row" class="">
                            <span class="themeClr">in *,M^</span>&nbsp;&nbsp;
                            <span class="f-loto">Operation</span>
                        </th>
                    
                        <td>
                            <span class="f-loto">
                                <select name="operation" id="operation" class="f-loto" onChange="change_operation(this.value,'designation');">
                                    <option value="add" selected="selected">Add</option>
                                    <option value="view">View</option>
                                </select>
                            </span>
                        </td>
                
                    </tr>
                
                    <tr>
                        <th colspan="2"  scope="row">&nbsp;</th>
                    </tr>
                
                    <tr>
                        <th colspan="2" align="left"  scope="row"><hr /></th>
                    </tr>
                
                    <tr>
                        <th colspan="2" align="left"  scope="row"><h4 id="signinHeader">in fooj.k</h4></th>
                    </tr>
                
                    <tr>
                        <th scope="row">&nbsp;</th>
                        <td>&nbsp;</td>
                    </tr>
                
                    <tr>
                        <th scope="row">foHkkx dk uke</th>
                        <td><select name="department_id" id="department_id"  onchange="get_designation(this.value)">
                        <option value=""></option>
                        <?php
                        if($department){
							foreach($department as $dep){
								echo "<option value='".$dep['department_id']."'>".$dep['name']."</option>";
							}
						}
						?>
                        </select></td>
                    </tr>
                
                    <tr>
                        <th width="48%" scope="row">Js.kh </th>
                        <td width="52%">
                            <select name="class_id" id="class_id">
                                <option value=""></option>
                                <?php
                                if($class){
                                    foreach($class as $cls){
                                        echo "<option value='".$cls['class_id']."'>".$cls['name']."</option>";
                                    }
                                }
                                ?>
							</select>
						</td>
                    </tr>
                
                    <tr>
                        <th scope="row">in dk uke</th>
                        <td><input type="text" name="name" id="name" /></td>
                    </tr>
                
                    <tr>
                        <th scope="row"></th>
                        <td>&nbsp;</td>
                    </tr>
                
                    <tr>
                        <th colspan="2" align="left" scope="row" ><hr /></th>
                    </tr>
                
                    <tr>
                        <th colspan="2" align="left" scope="row" ><h4 id="signinHeader">HkÙks </h4> </th>
                    </tr>
                
                    <tr>
                        <th  scope="row">fo'ks"k osru <strong></strong></th>
                        <td ><input type="text" placeholder="0."  class="f-loto align-right" name="special_salary" id="special_salary">
                        <span class="f-loto" id="bold">&nbsp;Rs.</span></td>
                    </tr>
                
                    <tr>
                        <th  scope="row">egaxkbZ HkÙkk</th>
                        <td>
                            <input type="text" placeholder="0." class="f-loto align-right" name="dearly_allowance" id="dearly_allowance">
                            <span class="f-loto" id="bold">%</span>
                        </td>
                    </tr>
                
                    <tr>
                        <th  scope="row">x`g fdjk;k HkÙkk </th>
                        <td ><input type="text" placeholder="0." class="f-loto align-right" name="home_rent_allowance" id="home_rent_allowance">
                        <span class="f-loto" id="bold">&nbsp;Rs.</span></td>
                    </tr>
                
                    <tr>
                        <th  scope="row">uxj {kfriwfrZ HkÙkk </th>
                        <td ><input type="text" placeholder="0." class="f-loto align-right" name="city_damage_allowance" id="city_damage_allowance">
                        <span class="f-loto" id="bold">&nbsp;Rs.</span></td>
                    </tr>
                
                    <tr>
                        <th  scope="row">esfMdy HkÙkk </th>
                        <td ><input type="text" placeholder="0." class="f-loto align-right" name="medical_allowance" id="medical_allowance">
                        <span class="f-loto" id="bold">&nbsp;Rs.</span></td>
                    </tr>
                
                    <tr>
                        <th  scope="row">varfje jkgr</th>
                        <td ><input type="text" placeholder="0." class="f-loto align-right" name="interim_relief" id="interim_relief">
                        <span class="f-loto" id="bold">&nbsp;Rs.</span></td>
                    </tr>
                
                    <tr>
                        <th  scope="row">okgu HkÙkk</th>
                        <td ><input type="text" placeholder="0." class="f-loto align-right" name="travelling_allowance" id="travelling_allowance">
                        <span class="f-loto" id="bold">&nbsp;Rs.</span></td>
                    </tr>
                
                    <tr>
                        <th  scope="row">&nbsp;</th>
                        <td >&nbsp;</td>
                    </tr>
                
                    <tr>
                        <th colspan="2"  scope="row" align="left"><hr /></th>
                    </tr>
                
                    <tr>
                        <th colspan="2"  scope="row" align="left"><h4 id="signinHeader">osru Js.kh</h4></th>
                    </tr>
                
                    <tr>
                        <th  scope="row">6 osru lewg </th>
                        <td>
                            <select id="6_pay_grade_id" name="6_pay_grade_id" class="f-loto" onchange="get_vetan_shreni(this.value)">
                            	
                                <option></option>
								<?php foreach($six_pay_scale as $key=>$val){ ?>
									
									<option value="<?php echo $val['six_pay_grade_id']; ?>"><?php echo $val['six_min_salary'].' - '.$val['six_max_salary'].' - '.$val['six_grade_pay']; ?></option>
								<?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th  scope="row">U;wure <br /></th>
                        <td ><input type="text" placeholder="0." class="f-loto align-right" name="minimum_salary" id="minimum_salary">
                        <span class="f-loto" id="bold">&nbsp;Rs.</span></td>
                    </tr>
                
                    <tr>
                        <th  scope="row">osru o`f) çfr o&quot;kZ </th>
                        <td ><input type="text" placeholder="0." class="f-loto align-right" name="salary_increment_per_year" id="salary_increment_per_year">
                        <span class="f-loto" id="bold">&nbsp;Rs.</span></td>
                    </tr>
                
                    <tr>
                        <th  scope="row">vf/kdre </th>
                        <td ><input type="text" placeholder="0." class="f-loto align-right" name="maximum_salary" id="maximum_salary">
                        <span class="f-loto" id="bold">&nbsp;Rs.</span></td>
                    </tr>
                
                    <tr>
                        <th  scope="row">f}rh; osru o`f) </th>
                        <td ><input type="text" placeholder="0." class="f-loto align-right" name="second_increment" id="second_increment"><span class="f-loto" id="bold">&nbsp;Rs.</span></td>
                    </tr>
                
                    <tr>
                        <th  scope="row">f}rh; vf/kdre </th>
                        <td>
                            <input type="text" placeholder="0." class="f-loto align-right" name="second_increment_maximum" id="second_increment_maximum">
                        </td>
                    </tr>
                    
                    <tr>
                        <th colspan="2" align="left"  scope="row"><hr /></th>
                    </tr>
                
                    <tr>
                        <th colspan="2" align="left"  scope="row"><h4 id="signinHeader">dVks=k</h4></th>
                    </tr>
                
                    <tr>
                        <th scope="row">th-ih Q.M<br /></th>
                        <td>
                            <input type="text" placeholder="0." class="f-loto align-right" name="GPF" id="GPF"/>
                            <span class="f-loto" id="bold">&nbsp;Rs.</span>
                        </td>
                    </tr>
                
                    <tr>
                        <th   scope="row">th-ih-,Q yksu</th>
                        <td >
                            <input type="text" placeholder="0." class="f-loto align-right" name="GPF_Loan" id="GPF_Loan" />
                            <span class="f-loto" id="bold">&nbsp;Rs.</span>
                        </td>
                    </tr>
                
                    <tr>
                        <th scope="row">QSfeyh csfufQV Q.M</th>
                        <td >
                            <input type="text" placeholder="0." class="f-loto align-right" name="family_benifit_fund" id="family_benifit_fund" />
                            <span class="f-loto" id="bold">&nbsp;Rs.</span>
                        </td>
                    </tr>
                
                    <tr>
                        <th scope="row">xzsu yksu</th>
                        <td>
                            <input type="text" placeholder="0." class="f-loto align-right" name="grain_loan" id="grain_loan" />
                            <span class="f-loto" id="bold">&nbsp;Rs.</span>
                        </td>
                    </tr>
                
                    <tr>
                        <th scope="row">çksQsluy VSDl </th>
                        <td>
                            <input type="text" placeholder="0." class="f-loto align-right" name="professional_tax" id="professional_tax"/>
                            <span class="f-loto" id="bold">&nbsp;Rs.</span>
                        </td>
                    </tr>
                
                    <tr>
                        <th scope="row">chek çhfe;e </th>
                        <td>
                            <input type="text" placeholder="0." class="f-loto align-right" name="bima_premium" id="bima_premium" />
                            <span class="f-loto" id="bold">&nbsp;Rs.</span>
                        </td>
                    </tr>
                
                    <tr>
                        <th   scope="row">lh-Vh-Mh</th>
                        <td>
                            <input type="text" placeholder="0." class="f-loto align-right" name="CTD" id="CTD" />
                            <span class="f-loto" id="bold">&nbsp;Rs.</span>
                        </td>
                    </tr>
                    <tr>
                        <th   scope="row">eks- lkbdy yksu</th>
                        <td>
                            <input type="text" placeholder="0." class="f-loto align-right" name="mo_cycle_loan" id="mo_cycle_loan"/>
                            <span class="f-loto" id="bold">&nbsp;Rs.</span>
                        </td>
                    </tr>
                
                    <tr>
                        <th scope="row">lkbdy yksu</th>
                        <td >
                            <input type="text" placeholder="0." class="f-loto align-right" name="cycle_loan" id="cycle_loan" />
                            <span class="f-loto" id="bold">&nbsp;Rs.</span>
                        </td>
                    </tr>
                    <tr>
                    
                        <th scope="row">gkml yksu</th>
                        <td>
                            <input type="text" placeholder="0." class="f-loto align-right" name="house_loan" id="house_loan" />
                            <span class="f-loto" id="bold">&nbsp;Rs.</span>
                        </td>
                    </tr>
                
                    <tr>
                        <th   scope="row">u-ik HkaMkj</th>
                        <td ><input type="text" placeholder="0." class="f-loto align-right" name="napa_bhandar" id="napa_bhandar" />
                        <span class="f-loto" id="bold">&nbsp;Rs.</span></td>
                    </tr>
                
                    <tr>
                        <th   scope="row">gfjtu isड़h</th>
                        <td ><input type="text" placeholder="0." class="f-loto align-right" name="harijan_pedi" id="harijan_pedi" />
                        <span class="f-loto" id="bold">&nbsp;Rs.</span></td>
                    </tr>
                
                    <tr>
                        <th   scope="row">x`g fdjk;k</th>
                        <td ><input type="text" placeholder="0." class="f-loto align-right" name="home_rent" id="home_rent" />
                        <span class="f-loto" id="bold">&nbsp;Rs.</span></td>
                    </tr>
                
                    <tr>
                        <th   scope="row">ikuh fdjk;k</th>
                        <td ><input type="text" placeholder="0." class="f-loto align-right" name="water_rent" id="water_rent" />
                        <span class="f-loto" id="bold">&nbsp;Rs.</span></td>
                    </tr>
                
                    <tr>
                        <th   scope="row">fctyh fdjk;k</th>
                        <td ><input type="text" placeholder="0." class="f-loto align-right" name="electricity_rent" id="electricity_rent" />
                        <span class="f-loto" id="bold">&nbsp;Rs.</span></td>
                    </tr>
                
                    <tr>
                        <th   scope="row">&nbsp;</th>
                        <td >&nbsp;</td>
                    </tr>
                    
                    
                    <tr>
                        <th align="right"   scope="row">
                        	<input type="submit" value=",M" class="btn btn-primary f-kruti" id="submit3" name="submit" />
                        </th>
                        <td align="left" ><input type="submit" value="Dykst " class="btn btn-primary f-kruti" id="submit2" name="submit2" /></td>
                    </tr>

                </table>

            </div>

        </div>

    </div>

</form>