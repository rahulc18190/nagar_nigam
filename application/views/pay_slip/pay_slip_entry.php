<link rel="stylesheet" href="//code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.0/jquery-ui.js"></script>

<script>

$(function() {
    $("#month" ).datepicker({
        changeMonth: true,
        changeYear: true,
        minDate: '-50Y',
        maxDate: '+0Y',
        yearRange: '-50:+0',
        dateFormat: 'MM-yy', 
		showButtonPanel: true,

		onClose: function() {
			var iMonth = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
			var iYear = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
			$(this).datepicker('setDate', new Date(iYear, iMonth, 1));
		 },
	
		 beforeShow: function() {

		   if ((selDate = $(this).val()).length > 0) 
		   {
			  iYear = selDate.substring(selDate.length - 4, selDate.length);
			  iMonth = jQuery.inArray(selDate.substring(0, selDate.length - 5), 
					   $(this).datepicker('option', 'monthNames'));
			  $(this).datepicker('option', 'defaultDate', new Date(iYear, iMonth, 1));
			  $(this).datepicker('setDate', new Date(iYear, iMonth, 1));
		   }		   
		}
    });
});

function get_employee(department_id) {
	jQuery.post('<?php echo site_url('pay_slip/get_employee') ?>', {department_id : department_id}, function(r) {
		if(r != '') {
			jQuery('#employee_list_td').html(r);
		}
	});
}
							
function get_employee_details(employee_id) {
	jQuery.post('<?php echo site_url('pay_slip/get_employee_details') ?>', {employee_id : employee_id}, function(r) {
		if(r != '') {

			var eData	=	JSON.parse(r);
						
			$('#designation').val(eData['designation_name']);
			$('#bank').val(eData['bank_name']);
			$('#branch').val(eData['branch']);
			$('#account_number').val(eData['account_number']);
			$('#six_pay_band').val(eData['six_pay_band']);


			$('.extra_items').css('display','table-row');
		}
	});
}
	
$(document).ready(function(){

	//$('.extra_items').css('display','none');
});							

</script>            

<style>
.ui-datepicker-calendar {
    display: none !important;
}​
</style>

<form action="" method="post">
          
    <div class="wrapper clear" >   
    
        <div id="list-item">
        
            <h2 id="signinHeader" class="f-kruti cnt-head" align="center" >is Lyhi ,sM </h2>
            <hr />

            <div class="signin-header">
            
                <table width="100%" border="0" class="form">
                    <tr>
                        <th scope="row" class="">&nbsp;&nbsp;<span class="f-loto">Operation </span></th>
                        <td><span class="f-loto">
                        <select class="f-loto" id="select6" name="select6">
                        <option value="Add">Add</option>
                        </select>
                        </span></td>
                    </tr>
                   
                    <tr>
                        <th colspan="2" class="" scope="row"><hr />&nbsp;</th>
                    </tr>
                    
                    <tr>
                        <th colspan="2" align="left"  scope="row"><h4 id="signinHeader">osrd i=d fooj.k </h4></th>
                    </tr>
                    

                    <tr>
                        <th scope="row" class="">foHkkx</th>
                        <td><select name="department_id" id="department_id"  onchange="get_employee(this.value)">
                        <option value=""></option>
                        <?php
                        if($department){
							foreach($department as $dep){ ?>

								<option <?php echo ($employee_data->department_id	==	$dep['department_id']) ? 'selected="selected"' : '' ; ?> 
                                value='<?php echo $dep['department_id']; ?>'><?php echo $dep['name']; ?></option>
							
							<?php }
						}
						?>
                        </select></td>
                    </tr>

                    <tr>
                        <th scope="row"><p>deZpkjh dk uke<br />
                        </p></th>
                        <td id="employee_list_td"><span class="f-loto">
                        <select id="select" name="select">
                        <option value=""></option>
                        </select>
                        </span></td>
                    </tr>
                    
                    <tr>
                        <th width="48%" scope="row">in</th>
                        <td width="52%"><input type="text" id="designation" name="designation" /></td>
                    </tr>
                    
                    <tr>
                        <th scope="row">eghuk o~ lky</th>
                        <td><input type="text" name="month" id="month" class="f-loto" value="<?php echo date('F-Y'); ?>">
                    </tr>
                    
                    <tr class="extra_items">
                        <th scope="row">cSad</th>
                        <td><input type="text" name="bank" id="bank" /></td>
                    </tr>
                    
                    <tr class="extra_items">
                        <th scope="row">czkap</th>
                        <td><input type="text" name="branch" id="branch" /></td>
                    </tr>
                    
                    <tr class="extra_items">
                        <th scope="row">vdkmaV uacj </th>
                        <td><input type="text" name="account_number" id="account_number" class="f-loto" /></td>
                    </tr>
                    
                    <tr class="extra_items">
                        <th height="31" scope="row">&nbsp;</th>
                        <td><input type="radio" name="nirvah_bhatta_per" id="nirvah_bhatta_per1" value="50%" />
                        50%
                        <input type="radio" name="nirvah_bhatta_per" id="nirvah_bhatta_per2" value="75%" />      
                        75% 
                        <input type="checkbox" name="nirvah_bhatta" id="nirvah_bhatta" /> fuokZg HkÙkk </td>
                    </tr>
                    
                    <tr class="extra_items">
                        <th scope="row">dqy mifLrfFk</th>
                        <td><input type="text" class="morehalf-txtbox" value="<?php echo date("t"); ?>" />&nbsp;fnu</td>
                    </tr>
                    
                    <tr class="extra_items">
                        <th scope="row">ewy osru </th>
                        <td ><input type="text" placeholder="0."  class="f-loto align-right" />
                        <span class="f-loto" id="bold">&nbsp;Rs.</span></td>
                    </tr>
                    
                    <tr class="extra_items">
                        <th scope="row">egaxkbZ ewy osru </th>
                        <td ><input type="text" placeholder="0."  class="f-loto align-right" />
                        <span class="f-loto" id="bold">&nbsp;Rs.</span></td>
                    </tr>
                    
                    <tr class="extra_items">
                        <th scope="row">dqy ewy osru</th>
                        <td ><input type="text" placeholder="0."  class="f-loto align-right" />
                        <span class="f-loto" id="bold">&nbsp;Rs.</span></td>
                    </tr>
                    
                    <tr class="extra_items">
                        <th scope="row">uxn Hkqxrku</th>
                        <td ><input type="text" placeholder="0."  class="f-loto align-right" />
                        <span class="f-loto" id="bold">&nbsp;Rs.</span></td>
                    </tr>
                    
                    <tr class="extra_items">
                        <th scope="row">fo'ks&quot;k osru </th>
                        <td ><input type="text" placeholder="0."  class="f-loto align-right" />
                        <span class="f-loto" id="bold">&nbsp;Rs.</span></td>
                    </tr>
                    
                    <tr class="extra_items">
                        <th scope="row">O;fäxr osru</th>
                        <td ><input type="text" placeholder="0."  class="f-loto align-right" />
                        <span class="f-loto" id="bold">&nbsp;Rs.</span></td>
                    </tr>
                    
                    <tr class="extra_items">
                        <th scope="row">osru Js.kh </th>
                        <td ><input type="text" placeholder="0."  class="f-loto align-right" />
                        <span class="f-loto" id="bold">&nbsp;Rs.</span></td>
                    </tr>
                    
                    <tr class="extra_items">
                        <th scope="row">6 osru lewg </th>
                        <td ><input type="text" placeholder="0."  class="f-loto align-right" name="six_pay_band" id="six_pay_band" />
                        <span class="f-loto" id="bold">&nbsp;Rs.</span></td>
                    </tr>
                    
                    <tr class="extra_items">
                        <th colspan="2" align="center" scope="row" >
                            <table width="100%" border="0">
                                <tr>
                                    <th width="84%" scope="row"> <input type="checkbox" name="checkbox2" id="checkbox2" />
                                    vftZr ;k y?kq—r vodk'k
                                    <input type="text" class="half-txtbox" />
                                    <input type="checkbox" name="checkbox3" id="checkbox3" />
                                    dksbZ osru ugha </th>
                                    <td width="16%" ><span class="signup f-loto"><a href="javascript:;" class="active" onclick="remark();">Remark</a></span></td>
                                </tr>
                            </table>
                        </th>
                    </tr>

					<script>
                        function remark(){										
                            $('#remark').show();
                        }
                    </script>
                    <tr style="display:none;" id="remark">
                        <th class="f-loto">Remark</th>
                        <th>
                            <textarea name="remark" id="remark"></textarea>
                        </th>
                        <hr />      
                    </tr>
                    

                    <tr class="extra_items">
                        <th colspan="2" align="left" scope="row" ><h4 id="signinHeader">HkÙks </h4> </th>
                    </tr>
                    
                    <tr class="extra_items">
                        <th  scope="row">egaxkbZ HkÙkk</th>
                        <td ><input type="text" placeholder="0." class="f-loto align-right" name="DA" id="DA">
                        <span class="f-loto" id="bold">%</span></td>
                    </tr>
                    
                    <tr class="extra_items">
                        <th  scope="row">x`g fdjk;k HkÙkk </th>
                        <td ><input type="text" placeholder="0." class="f-loto align-right" name="HRA" id="HRA"><span class="f-loto" id="bold">&nbsp;Rs.</span></td>
                    </tr>
                    
                    <tr class="extra_items">
                        <th  scope="row">uxj {kfriwfrZ HkÙkk </th>
                        <td ><input type="text" placeholder="0." class="f-loto align-right" name="city_damage_allowance" id="city_damage_allowance"><span class="f-loto" id="bold">&nbsp;Rs.</span></td>
                    </tr>
                    
                    <tr class="extra_items">
                        <th  scope="row">esfMdy HkÙkk </th>
                        <td ><input type="text" placeholder="0." class="f-loto align-right" name="MA" id="MA"><span class="f-loto" id="bold">&nbsp;Rs.</span></td>
                    </tr>
                    
                    <tr class="extra_items">
                        <th  scope="row">varfje jkgr</th>
                        <td ><input type="text" placeholder="0." class="f-loto align-right" name="IR" id="IR"><span class="f-loto" id="bold">&nbsp;Rs.</span></td>
                    </tr>
                    
                    <tr class="extra_items">
                        <th  scope="row">okgu HkÙkk</th>
                        <td ><input type="text" placeholder="0." class="f-loto align-right" name="TA" id="TA"><span class="f-loto" id="bold">&nbsp;Rs.</span></td>
                    </tr>
                    
                    <tr class="extra_items">
                        <th colspan="2" align="left"  scope="row"><hr /></th>
                    </tr>
                    
                    <tr class="extra_items">
                        <th colspan="2" align="left"  scope="row"><h4 id="signinHeader">dVks=k</h4></th>
                    </tr>
                    
                    <tr class="extra_items">
                        <th   scope="row">th-ih Q.M<br />    </th>
                        <td ><input type="text" placeholder="0." class="f-loto align-right" name="GPF" id="GPF" />
                        <span class="f-loto" id="bold">&nbsp;Rs.</span></td>
                    </tr>
                    
                    <tr class="extra_items">
                        <th   scope="row">th-ih-,Q yksu</th>
                        <td ><input type="text" placeholder="0." class="f-loto align-right" name="GPF_loan" id="GPF_loan" />
                        <span class="f-loto" id="bold">&nbsp;Rs.</span></td>
                    </tr>
                    
                    <tr class="extra_items">
                        <th   scope="row">QSfeyh csfufQV Q.M</th>
                        <td ><input type="text" placeholder="0." class="f-loto align-right" name="FBF" id="FBF" />
                        <span class="f-loto" id="bold">&nbsp;Rs.</span></td>
                    </tr>
                    
                    <tr class="extra_items">
                        <th   scope="row">xzsu yksu</th>
                        <td ><input type="text" placeholder="0." class="f-loto align-right" name="GL" id="GL" />
                        <span class="f-loto" id="bold">&nbsp;Rs.</span></td>
                    </tr>
                    
                    <tr class="extra_items">
                        <th   scope="row">çksQsluy VSDl </th>
                        <td ><input type="text" placeholder="0." class="f-loto align-right" name="PT" id="PT" />
                        <span class="f-loto" id="bold">&nbsp;Rs.</span></td>
                    </tr>
                    
                    <tr class="extra_items">
                        <th   scope="row">chek çhfe;e </th>
                        <td ><input type="text" placeholder="0." class="f-loto align-right" name="BP" id="BP" />
                        <span class="f-loto" id="bold">&nbsp;Rs.</span></td>
                    </tr>
                    
                    <tr class="extra_items">
                        <th   scope="row">lh-Vh-Mh</th>
                        <td ><input type="text" placeholder="0." class="f-loto align-right" name="CTD" id="CTD" />
                        <span class="f-loto" id="bold">&nbsp;Rs.</span></td>
                    </tr>
                    
                    <tr class="extra_items">
                        <th   scope="row">eks- lkbdy yksu</th>
                        <td ><input type="text" placeholder="0." class="f-loto align-right" name="mo_cycle_loan" id="mo_cycle_loan" />
                        <span class="f-loto" id="bold">&nbsp;Rs.</span></td>
                    </tr>
                    
                    <tr class="extra_items">
                        <th   scope="row">lkbdy yksu</th>
                        <td ><input type="text" placeholder="0." class="f-loto align-right" name="cycle_loan" id="cycle_loan" />
                        <span class="f-loto" id="bold">&nbsp;Rs.</span></td>
                    </tr>
                    
                    <tr class="extra_items">
                        <th   scope="row">gkml yksu</th>
                        <td ><input type="text" placeholder="0." class="f-loto align-right" name="house_loan" id="house_loan" />
                        <span class="f-loto" id="bold">&nbsp;Rs.</span></td>
                    </tr>
                    
                    <tr class="extra_items">
                        <th   scope="row">u-ik HkaMkj</th>
                        <td ><input type="text" placeholder="0." class="f-loto align-right" name="napa_bhandar" id="napa_bhandar" />
                        <span class="f-loto" id="bold">&nbsp;Rs.</span></td>
                    </tr>
                    
                    <tr class="extra_items">
                        <th   scope="row">gfjtu isड़h</th>
                        <td ><input type="text" placeholder="0." class="f-loto align-right" name="harijan_pedi" id="harijan_pedi" />
                        <span class="f-loto" id="bold">&nbsp;Rs.</span></td>
                    </tr>
                    
                    <tr class="extra_items">
                        <th   scope="row">x`g fdjk;k</th>
                        <td ><input type="text" placeholder="0." class="f-loto align-right" name="home_rent" id="home_rent" />
                        <span class="f-loto" id="bold">&nbsp;Rs.</span></td>
                    </tr>
                    
                    <tr class="extra_items">
                        <th   scope="row">ikuh fdjk;k</th>
                        <td ><input type="text" placeholder="0." class="f-loto align-right" name="water_rent" id="water_rent" />
                        <span class="f-loto" id="bold">&nbsp;Rs.</span></td>
                    </tr>
                    
                    <tr class="extra_items">
                        <th   scope="row">fctyh fdjk;k</th>
                        <td ><input type="text" placeholder="0." class="f-loto align-right" name="electric_rent" id="electric_rent" />
                        <span class="f-loto" id="bold">&nbsp;Rs.</span></td>
                    </tr>
                    
                    <tr class="extra_items">
                        <th   scope="row">&nbsp;</th>
                        <td >&nbsp;</td>
                    </tr>

                    <tr>
                        <th colspan="2" align="center"   scope="row"><input type="submit" value=",M" class="btn btn-primary f-kruti" id="submit3" name="submit3" />
                        <input type="submit" value="Dykst " class="btn btn-primary f-kruti" id="submit2" name="submit2" />
                        <input type="submit" value=",M vkSj fçaV " class="btn btn-primary f-kruti" id="submit" name="submit" /></th>
                    </tr>

                </table>

            </div>

        </div>

    </div>

</form>
