<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>UJJAIN NAGAR PALIK NIGAM</title>

	<?php echo $css_for_layout; ?>
    <?php echo $js_for_layout; ?>

</head>


<body>

    <header id="header">
    
        <div class="wrapper clear">
            <h1 class=""><a href="<?php echo site_url(); ?>">Nagar Nigam</a></h1>

			<?php if($this->session->userdata('is_admin_login')){ ?>

            <nav>
            
                <ul class="clear">
                
                    <h4></h4>  
                
                    <li class="link">
                        <a class="<?php echo ($menu == 'utilities') ? 'active' : '' ; ?>" href="#">Utilities</a>
                        <ul class="subnav">
                            <li class="link">
                            	<a class="<?php echo ($sub_menu == 'generate_sixth_pay') ? 'active' : '' ; ?>" href="#">Generate Sixth Pay </a>
                            </li>
                            <li class="link">
                            	<a class="<?php echo ($sub_menu == 'change_in_jan2006') ? 'active' : '' ; ?>" href="#">Change In Jan2006</a>
                            </li>
                            <li class="link">
                            	<a class="<?php echo ($sub_menu == 'change_password') ? 'active' : '' ; ?>" href="<?php echo base_url().'utilities/change_password'; ?>">Change Password </a>
                            </li>
                            <?php if($this->session->userdata('admin_username')	==	'admin'){ ?>
                                <li class="link">
                                    <a class="<?php echo ($sub_menu == 'create_user') ? 'active' : '' ; ?>" href="<?php echo base_url().'utilities/create_user'; ?>">Create User</a>
                                </li>
							<?php } ?>
                            <li class="link">
                            	<a class="<?php echo ($sub_menu == 'backup') ? 'active' : '' ; ?>" href="<?php echo base_url().'backup'; ?>" target="_blank">Backup</a>
                            </li>
                            <li class="link">
                            	<a class="<?php echo ($sub_menu == 'employee') ? 'active' : '' ; ?>" href="<?php echo base_url().'home/logout'; ?>">Logout</a>
                            </li>
                        </ul>
                    </li>
                    
                    <li class="link">
                        <a class="<?php echo ($menu == 'reports') ? 'active' : '' ; ?>" href="#">Reports</a>
                        <ul class="subnav">
                            <li class="link">
                            	<a class="<?php echo ($sub_menu == 'reports') ? 'active' : '' ; ?>" href="<?php echo base_url().'report/reports'; ?>">Reports </a>
                            </li>
                            <li class="link">
                            	<a class="<?php echo ($sub_menu == 'other_reports') ? 'active' : '' ; ?>" href="<?php echo base_url().'report/other_reports'; ?>">Other Reports</a>
                            </li>
                        </ul>
                    </li>
                    
                    <li class="link">
                        <a class="<?php echo ($menu == 'pay_slip') ? 'active' : '' ; ?>" href="#">Pay Slip</a>
                        <ul class="subnav">
                            <li class="link">
                            	<a class="<?php echo ($sub_menu == 'pay_slip') ? 'active' : '' ; ?>" href="<?php echo base_url().'pay_slip'; ?>">Pay Slip Entry</a>
                            </li>
                            <li class="link">
                            	<a class="<?php echo ($sub_menu == 'pay_slip_synchronization') ? 'active' : '' ; ?>" href="<?php echo base_url().'pay_slip/pay_slip_synchronization'; ?>">Pay Slip Synchronization</a>
                            </li>
                        </ul>
                    </li> 
                    
                    <li class="link">
                        <a class="<?php echo ($menu == 'others') ? 'active' : '' ; ?>" href="#">Others</a>
                        <ul class="subnav">
                            <li class="link">
                            	<a class="<?php echo ($sub_menu == 'transfer') ? 'active' : '' ; ?>" href="<?php echo base_url().'others/transfer'; ?>">Transfer</a>
                            </li>
                            <li class="link">
                            	<a class="<?php echo ($sub_menu == 'loan') ? 'active' : '' ; ?>" href="<?php echo base_url().'others/loan'; ?>">Loans</a>
                            </li>
                            <li class="link">
                            	<a class="<?php echo ($sub_menu == 'class') ? 'active' : '' ; ?>" href="<?php echo base_url().'others/classs'; ?>">Grade</a>
                            </li>
                            <li class="link">
                            	<a class="<?php echo ($sub_menu == 'increment') ? 'active' : '' ; ?>" href="<?php echo base_url().'others/increment'; ?>">Increment</a>
                            </li>
                            <li class="link">
                            	<a class="<?php echo ($sub_menu == 'increment_synchronization') ? 'active' : '' ; ?>" href="<?php echo base_url().'others/increment_synchronization'; ?>">Increment Synchronization</a>
                            </li>
                            <li class="link">
                            	<a class="<?php echo ($sub_menu == 'vetan_shreni') ? 'active' : '' ; ?>" href="<?php echo base_url().'others/vetan_shreni'; ?>">Vetan Shreni</a>
                            </li>
                        </ul>
                    </li>
                    
                    <li class="link">
                        <a class="<?php echo ($menu == 'data_entry') ? 'active' : '' ; ?>" href="#">Data Entry</a>
                        <ul class="subnav">
                            
                            <li class="link">
                            	<a class="<?php echo ($sub_menu == 'employee') ? 'active' : '' ; ?>" href="<?php echo base_url().'data_entry/add_employee'; ?>">Employee Entry</a>
							</li>
                            
                            <li class="link">
                            	<a class="<?php echo ($sub_menu == 'designation') ? 'active' : '' ; ?>" href="<?php echo base_url().'data_entry/add_designation'; ?>">Designation Entry</a>
                            </li>
                            
                            <li class="link">
                            	<a class="<?php echo ($sub_menu == 'department') ? 'active' : '' ; ?>" href="<?php echo base_url().'data_entry/add_department'; ?>">Department Entry</a>
                            </li>
                            
                            <li class="link">
                            	<a class="<?php echo ($sub_menu == 'bank') ? 'active' : '' ; ?>" href="<?php echo base_url().'data_entry/add_bank'; ?>">Bank Entry</a>
                            </li>
                        
                        </ul>
                    </li>

                </ul>

            </nav>
            
            <?php } ?>

        </div>

    </header>

    <section class="warmBg" id="common-banner">
        <div class="wrapper clear">
            <aside class="last">
                <img src="<?php echo base_url().'assets/images/header_image.png'; ?>" />
            </aside>
        </div>
        <div align="right">
        	<?php if($this->session->userdata('is_admin_login')){ ?>
                Welcome, <?php echo "<b>".$this->session->userdata('admin_username')."</b>"; ?>
			<?php } ?>
        </div>
    </section>


	<script>
        function change_operation(operation,item_name){
            if(operation	==	'add'){
                window.top.location	=	'<?php echo site_url('data_entry'); ?>/add_'+item_name;
            }
            else if(operation	==	'view'){
                window.top.location	=	'<?php echo site_url('data_entry'); ?>/view_'+item_name;
            }
        }
    </script>
  

     
	<?php echo $content_for_layout ?>
           
           

    <!--FOOTER :) -->        
    <footer>
        <!--/end of top section-->
        <section id="bottom-ftr">
            <div class="wrapper clear">
                <span class="fl">Narar Nigam &copy; 2014-2015 | All Rights Reserved</span>
                <span class="fr">Powered by &nbsp;SITS SOLUTIONS</span>
            </div>
        </section>
    </footer>  

</body>


</html>
