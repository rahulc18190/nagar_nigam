<script>

function show_password(){

	$('#password').attr('type','text');
	$('#confirm_password').attr('type','text');
	$('#show-lnk').hide();
	$('#hide-lnk').show();
}

function hide_password(){

	$('#password').attr('type','password');
	$('#confirm_password').attr('type','password');
	$('#show-lnk').show();
	$('#hide-lnk').hide();
}

function validate(){
	
	var new_password 		= $('#password').val();	
	var confirm_password 	= $('#confirm_password').val();	

	if(new_password != confirm_password){
		alert('Passwords are not matched.');
		$('#confirm_password').val('')
	}
}

/*function check_mail(email){

	var expr = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);

	if(!expr.test(email)) {
		alert("Invalid Email Id.");
		$("#email").val('');
	}
}*/

function check_availability(username){

	jQuery.post('<?php echo site_url('utilities/check_availability') ?>', {username : username}, function(r) {
		if(r == 1) {
			alert('Username already exist.');
			$('#username').val('')
		}
	});
}

</script>
<form action="<?php echo ($user_detail) ? site_url('utilities/insert_user/'.$user_detail->id) : site_url('utilities/insert_user') ; ?>" method="post">

    <div class="wrapper clear" >   
    
        <div id="signin">
        
            <h2 id="signinHeader" class="cnt-head f-loto" align="center">Create User</h2>
            <div align="right"><a href="<?php echo site_url('utilities/view_user'); ?>" class="f-loto cnt-head side-heading-small">View user</a></div>
            <hr />

            <div class="signin-header">
            
                <table width="100%" border="0" class="form">
                    
                    <?php if($this->session->flashdata('success_msg')){ ?>

                        <tr>
                            <th colspan="2" class="f-loto" scope="row">
                            	<div class="alert alert-success">
                                	<?php echo $this->session->flashdata('success_msg'); ?>
                                </div>
                            </th>
                        </tr>
							
					<?php } ?>
                    
                    <?php if($this->session->flashdata('failure_msg')){ ?>

                        <tr>
                            <th colspan="2" class="f-loto" scope="row">
                            	<div class="alert alert-danger">
                                	<?php echo $this->session->flashdata('failure_msg'); ?>
                                </div>
                            </th>
                        </tr>
							
					<?php } ?>

                    <tr>
                        <th scope="row" class="f-loto">Full Name</th>
                        <td><input type="text" name="full_name" id="full_name" class="f-loto" required='required' value="<?php echo $user_detail->full_name; ?>" /></td>
                    </tr>

<!--                    <tr>
                        <th scope="row" class="f-loto">Email</th>
                        <td><input type="text" name="email" id="email" class="f-loto" onblur="check_mail(this.value)" required='required' /></td>
                    </tr>

-->                    <tr>
                        <th scope="row" class="f-loto">Username</th>
                        <td><input type="text" name="username" id="username" class="f-loto" required='required' onblur="check_availability(this.value)" value="<?php echo $user_detail->username; ?>" <?php echo ($user_detail) ? 'disabled="disabled"' : '' ; ?>  /></td>
                    </tr>
                    
                    <tr>
                        <th scope="row" class="f-loto">Password</th>
                        <td><input type="password" name="password" id="password" onblur="validate()" class="f-loto" required='required' value="<?php echo base64_decode($user_detail->password); ?>"/>
                        <span class="f-loto" id="show-lnk" onclick="show_password()" style='font-size: 14px;text-decoration:underline;cursor:pointer;'>Show text</span>
                        <span class="f-loto" id="hide-lnk" onclick="hide_password()" style="display:none;font-size: 14px;text-decoration:underline;cursor:pointer;">Hide text</span>
                        </td>
                    </tr>
                    
                    <tr>
                        <th scope="row" class="f-loto">Confirm Password</th>
                        <td><input type="password" id="confirm_password"  onblur="validate()" class="f-loto" required='required' value="<?php echo base64_decode($user_detail->password); ?>"/></td>
                    </tr>
                    
                    <tr>
                        <th align="right" scope="row">
                        <input type="hidden" name="admin_type" id="admin_type" value="user">
                        <input type="submit" value="<?php echo ($user_detail) ? 'Edit' : ',M' ; ?>" class="btn btn-primary f-kruti" id="submit" /></th>
                        <td align="left"><input type="submit" value="Dykst" class="btn btn-primary f-kruti" id="submit2"/></td>
                    </tr>
                
                </table>
            
            </div>
        
        </div>
    
    </div>

</form>