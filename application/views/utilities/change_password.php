<script>
function validate(){
	
	var new_password 		= $('#new_password').val();	
	var confirm_password 	= $('#confirm_password').val();	

	if(new_password != confirm_password){
		alert('New password and confirm password are not same.');
		$('#confirm_password').val('')
	}
}
</script>

<form action="<?php echo site_url('utilities/insert_new_password'); ?>" method="post">

    <div class="wrapper clear" >   
    
        <div id="signin">
        
            <h2 id="signinHeader" class="cnt-head f-loto" align="center">Change Password</h2>
            <hr />

            <div class="signin-header">
            
                <table width="100%" border="0" class="form">
                    
                    <?php if($this->session->flashdata('success_msg')){ ?>

                        <tr>
                            <th colspan="2" class="f-loto" scope="row">
                            	<div class="alert alert-success">
                                	<?php echo $this->session->flashdata('success_msg'); ?>
                                </div>
                            </th>
                        </tr>
							
					<?php } ?>
                    
                    <?php if($this->session->flashdata('failure_msg')){ ?>

                        <tr>
                            <th colspan="2" class="f-loto" scope="row">
                            	<div class="alert alert-danger">
                                	<?php echo $this->session->flashdata('failure_msg'); ?>
                                </div>
                            </th>
                        </tr>
							
					<?php } ?>

                    <tr>
                        <th scope="row" class="f-loto">Old Password</th>
                        <td><input type="password" name="old_password" id="old_password" /></td>
                    </tr>
                    
                    <tr>
                        <th scope="row" class="f-loto">New Password</th>
                        <td><input type="password" name="new_password" id="new_password" /></td>
                    </tr>
                    
                    <tr>
                        <th scope="row" class="f-loto">Confirm Password</th>
                        <td><input type="password" name="confirm_password" id="confirm_password"  onblur="validate()"/></td>
                    </tr>
                    
                    <tr>
                        <th align="right" scope="row"><input type="submit" value=",M" class="btn btn-primary f-kruti" id="submit" name="submit" /></th>
                        <td align="left"><input type="submit" value="Dykst" class="btn btn-primary f-kruti" id="submit2" name="submit2" /></td>
                    </tr>
                
                </table>
            
            </div>
        
        </div>
    
    </div>

</form>