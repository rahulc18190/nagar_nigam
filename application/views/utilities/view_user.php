<script>
function show_password(id){
	$('.hidden-div-'+id).hide();
	$('.button-div-'+id).hide();
	$('.button2-div-'+id).show();
	$('.shown-div-'+id).show();
}
function hide_password(id){
	$('.hidden-div-'+id).show();
	$('.button-div-'+id).show();
	$('.button2-div-'+id).hide();
	$('.shown-div-'+id).hide();
}

function operation(operation,id){
	if(operation	==	-1){
		$confirm = confirm("Are you sure you want to delete ?");
		if($confirm == true){
			
			jQuery.post('<?php echo site_url('utilities/delete_user') ?>', {id : id}, function(r) {
				if(r != '') {
					window.top.location	=	'<?php echo site_url('utilities/create_user') ?>';	
				}
			});								
		}
	}
	else if(operation	==	0 || operation	==	1) {
	
		$confirm = confirm("Are you sure you want to change the user status ?");
		if($confirm == true){
			
			jQuery.post('<?php echo site_url('utilities/change_user_status') ?>', {status:operation, id : id}, function(r) {
				if(r != '') {
					window.top.location	=	'<?php echo site_url('utilities/create_user') ?>';	
				}
			});								
		}
	}
	else {
		window.top.location	=	'<?php echo site_url('utilities/create_user') ?>/'+id;	
	}
}
</script>

<div class="wrapper clear" >   

    <div id="signin">
    
        <h2 id="signinHeader" class="cnt-head f-loto" align="center">View User</h2>
        <div align="right"><a href="<?php echo site_url('utilities/create_user'); ?>" class="f-loto cnt-head side-heading-small">Back</a></div>
        <hr />

        <div class="signin-header">
        
            <table width="100%" border="0" class="form">
                
                <?php if($this->session->flashdata('success_msg')){ ?>

                    <tr>
                        <th colspan="2" class="f-loto" scope="row">
                            <div class="alert alert-success">
                                <?php echo $this->session->flashdata('success_msg'); ?>
                            </div>
                        </th>
                    </tr>
                        
                <?php } ?>
                
                <?php if($this->session->flashdata('failure_msg')){ ?>

                    <tr>
                        <th colspan="2" class="f-loto" scope="row">
                            <div class="alert alert-danger">
                                <?php echo $this->session->flashdata('failure_msg'); ?>
                            </div>
                        </th>
                    </tr>
                        
                <?php } ?>

            </table>

            <table width="100%" class="cus-tbl2 f-loto" >
            	<tbody>
                    <tr class="row-1">
                        <th width="10%" align="center" class="f-loto">Sr.No. </th>
                        <th width="30%" align="center" scope="row">Full Name </th>
                        <th width="20%" align="center" class="f-loto">Username </th>
                        <th width="20%" align="center" class="f-loto">Password </th>
                        <!--<th width="15%" align="center" class="f-loto">User Type </th>-->
                        <th width="20%" align="center" class="f-loto">Status</th>
                        <th width="20%" align="center" class="f-loto">Action</th>
                    </tr>
    
                    <?php
                    if($user){
                        foreach($user as $key=>$val){
                        
                        $status	=	($val['status']	==	1) ? 'Active' : 'Deactive' ; 
                            echo "<tr>";
                                echo "<th scope='row' class='row-2'>".($key+1)."</th>";
    
                                echo "<th scope='row' class='row-2'>".$val['full_name']."</th>";
    
                                echo "<th scope='row' class='row-2'>".$val['username']."</th>";
    
                                echo "<th scope='row' class='row-2'>";
                                    
                                    echo "<div class='hidden-div-".$val['id']."'>******</div>";
    
                                    echo "<div class='button-div-".$val['id']."' onClick='show_password(".$val['id'].")' style='font-size: 14px;text-decoration:underline;cursor:pointer;'>Show text</div>";
    
                                    echo "<div class='shown-div-".$val['id']."' style='display:none;'>".base64_decode($val['password'])."</div>";
    
                                    echo "<div class='button2-div-".$val['id']."' onClick='hide_password(".$val['id'].")' style='font-size: 14px;text-decoration:underline;cursor:pointer;display:none;'>Hide text</div>";
                                    
                                echo "</th>";
    
                                //echo "<th scope='row' class='row-2'>".$val['admin_type']."</th>";
    
                                echo "<th scope='row' class='row-2'>$status</th>";
    
                                echo "<th scope='row' class='row-2'>";
                                
                                if($val['username']	!=	'admin'){
                                    echo "<select class='f-loto' onChange='operation(this.value,".$val['id'].")'>";
                                        echo "<option>Select</option>";
                                        echo "<option value='1'>Activate</option>";
                                        echo "<option value='0'>Deactivate</option>";
                                        echo "<option value='-1'>Delete</option>";
                                        echo "<option value='2'>Edit</option>";
                                    echo "</select>";
                                }
                                echo "</th>";
    
    
                            echo "</tr>";
                        }
                    }
                    ?>
				</tbody>            
            </table>

        </div>
    
    </div>

</div>
