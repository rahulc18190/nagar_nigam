<script>
function edit(id){
	$('#div-id-'+id).hide();
	$('#text-id-'+id).show();
	$('#edit-id-'+id).hide();
	$('#save-id-'+id).show();
}								
function save(id){

	var name	=	$('#text-id-'+id).val();

	jQuery.post('<?php echo site_url('others') ?>/edit_class', {name : name,id : id});

	$('#loader-'+id).show();

	setTimeout(function(){
		$('#div-id-'+id).html(name);
		$('#div-id-'+id).show();
		$('#text-id-'+id).hide();
		$('#edit-id-'+id).show();
		$('#save-id-'+id).hide();
		$('#loader-'+id).hide();
	},2000);
}								
</script>

<form action="<?php echo site_url('others/add_class'); ?>" method="post">
          
    <div class="wrapper clear" >   
    
        <div id="signin">
    
            <h2 id="signinHeader" class="f-kruti cnt-head" align="center">Js.kh dk fooj.k </h2>
            <hr />
    
            <div class="signin-header">

                <table width="100%" border="0" class="form">

<!--                    <tr>
                        <th scope="row" class="f-loto">&nbsp;</th>
                        <td><select name="select" id="select" class="f-loto">
                        <option value="Add">Add</option>
                        </select></td>
                    </tr>
-->                    

                    <?php if($this->session->flashdata('success_msg')){ ?>

                        <tr>
                            <th colspan="2" class="f-loto" scope="row">
                            	<div class="alert alert-success">
                                	<?php echo $this->session->flashdata('success_msg'); ?>
                                </div>
                            </th>
                        </tr>
							
					<?php } ?>
                    
                    <?php if($this->session->flashdata('failure_msg')){ ?>

                        <tr>
                            <th colspan="2" class="f-loto" scope="row">
                            	<div class="alert alert-danger">
                                	<?php echo $this->session->flashdata('failure_msg'); ?>
                                </div>
                            </th>
                        </tr>
							
					<?php } ?>
                    
                    <tr>
                        <th width="48%" scope="row">dksM </th>
                        <td width="52%"><input type="text" value="<?php echo $class_id; ?>" name="class_id" id="class_id" readonly="readonly"/>  </td>
                    </tr>
                    
                    <tr>
                        <th scope="row">Js.kh dk uke </th>
                        <td><input type="text" name="name" id="name" required /></td>
                    </tr>

                    <tr>
                        <th colspan="2" align="center" scope="row">
                        
                            <table width="100%" class="cus-tbl" >
                                <tr class="row-1">
                                    <th width="48%" align="center" scope="row">dksM </th>
                                    <td width="52%" align="center">Js.kh dk uke </td>
                                    <td width="10%" align="center" class="f-loto">Edit</td>
                                    <td width="10%" style="visibility:hidden;"></td>
                                </tr>

                				<?php
								if($class){
									foreach($class as $key=>$val){
										echo "<tr>";
											echo "<th scope='row' class='row-2'>".($key+1)."</th>";
											echo "<th scope='row' class='row-2' id='td-name-".$val['id']."'>

													<div id='div-id-".$val['id']."'>".
													$val['name']
													."</div><input type='text' name='text-name-".$val['id']."' id='text-id-".$val['id']."' value='".$val['name']."' style='display:none;'>

												</th>";

											echo "<th scope='row' class='row-2' id='td-edit-".$val['id']."'>

													<a href='#' onClick='edit(".$val['id'].")' id='edit-id-".$val['id']."' class='f-loto'>
														<img src='".site_url('assets/images/edit.ico')."' width='25' title='Edit'>													
													</a>

													<a href='#' onClick='save(".$val['id'].")' id='save-id-".$val['id']."' class='f-loto' style='display:none;'>
														<img src='".site_url('assets/images/save.png')."' width='25' title='Save'>
													</a>

												</th>";

											echo "<th><img src='".site_url('assets/images/loader.gif')."' width='25' id='loader-".$val['id']."' 
													style='display: none;'>
												</th>";	

										echo "</tr>";
									}
								}
								?>

                            
                            </table>
                        
                        </th>
                    
                    </tr>

                    <tr>
                        <th align="right" scope="row"><input type="submit" value=",M" class="btn btn-primary f-kruti" id="submit3" name="submit3" /></th>
                        <td align="left"><input type="submit" value="Dykst" class="btn btn-primary f-kruti" id="submit2" name="submit2" /></td>
                    </tr>

                </table>

			</div>

		</div>

	</div>

</form>