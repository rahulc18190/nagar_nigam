
<script>
function start_synchronization(){
	
	$('#loader').show();

	$.post('<?php echo site_url('others/start_synchronization'); ?> ',function(r){
	
		if(r	==	0){

			setTimeout(function(){

				$('#loader').hide();
				$('#msg').html('<span class="alert alert-failure">Failure! Synchronization can be done only in the july month.</span>');

			},3000);	
		}
		else if(r	==	1){
		
			setTimeout(function(){

				$('#loader').hide();
				$('#synchronize').hide();
				$('#msg').html('<span class="alert alert-success">Success! Synchronization successfully done.</span>');

			},3000);	
		}
	});
}
</script>

<div class="wrapper clear" >   

    <div id="list-item">
    
        <h2 id="signinHeader" class="f-loto cnt-head" align="center" >Increment Synchronization</h2>
        <hr />
		
        <div class="f-loto list-title" style="font-size:18px !important;">
            Perform this yearly increment in the July month.		
		</div>
        
        <div>&nbsp;</div>

        <div align="center">
        
        	<?php if($status	==	''){ ?>
                <input type="button" value="Start Synchronization" class="btn btn-primary" id="synchronize" onclick="start_synchronization();">
			<?php } else { ?>
                <input type="button" value="Synchronization Already Done" class="btn" id="dont_synchronize">
			<?php } ?>
        </div>

        <div>&nbsp;</div>
        
        <div align="center" style="display:none" id="loader">
        
        	<img src="<?php echo site_url('assets/images/loader2.gif'); ?>" width="120">
        
        </div>
        
        <div>&nbsp;</div>

        <div id="msg" align="center"></div>
        
        <div>&nbsp;</div>
        <div>&nbsp;</div>
        
    </div>

</div>
