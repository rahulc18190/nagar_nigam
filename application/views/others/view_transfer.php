<div class="wrapper clear" >   

    <div id="signin">
    
        <div class="signin-header">
        
            <table width="100%" border="0" class="form">
                
                <?php if($this->session->flashdata('success_msg')){ ?>

                    <tr>
                        <th colspan="7" class="f-loto" scope="row">
                            <div class="alert alert-success">
                                <?php echo $this->session->flashdata('success_msg'); ?>
                            </div>
                        </th>
                    </tr>
                        
                <?php } ?>
                
                <?php if($this->session->flashdata('failure_msg')){ ?>

                    <tr>
                        <th colspan="7" class="f-loto" scope="row">
                            <div class="alert alert-danger">
                                <?php echo $this->session->flashdata('failure_msg'); ?>
                            </div>
                        </th>
                    </tr>
                        
                <?php } ?>
                
                <tr>
                    <th colspan="7" class="f-loto" scope="row"><h2 id="" class="cnt-head" align="center">Transfer List</h2> 
                    <div align="right"><a href="<?php echo site_url('others/transfer'); ?>" class="f-loto side-heading-small">Back</a></div>
                    </th>
                </tr>
              
                <tr>
                    <th colspan="7" align="left" scope="row"><hr /></th>
                </tr>
                
                <tr class="f-loto">
                    <td>Sr.</td>
                    <td>Employee Name</td>
                    <td>Old Department</td>
                    <td>Old Designation</td>
                    <td>New Department</td>
                    <td>New Designation</td>
                    <td>Transfer Date</td>
                </tr>
                
                <?php
                if($transfer){

                    foreach($transfer as $key=>$val){
                        echo "<tr>";
                            echo "<td>".$count."</td>";
                            echo "<td>".$val['employee_name']."</td>";
                            echo "<td>".$val['old_department']."</td>";
                            echo "<td>".$val['old_designation']."</td>";
                            echo "<td>".$val['new_department']."</td>";
                            echo "<td>".$val['new_designation']."</td>";
                            echo "<td>".$val['transfer_date']."</td>";
                        echo "</tr>";
                        $count++;
                    }
                }
                ?>
            
                <tr><td colspan="7" align="right"><?php echo $links; ?></td></tr>
                
            </table>
        
        </div>
    
    </div>

</div>
