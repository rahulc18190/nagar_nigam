<script>
	function get_designation_from(department_id) {
		jQuery.post('<?php echo site_url('others/get_designation_from') ?>', {department_id : department_id}, function(r) {
			if(r != '') {
				jQuery('#designation_td_from').html(r);
			}
		});
	}

	function get_designation_to(department_id) {
		jQuery.post('<?php echo site_url('others/get_designation_to') ?>', {department_id : department_id}, function(r) {
			if(r != '') {
				jQuery('#designation_td_to').html(r);
			}
		});
	}

	function get_employee(designation_id) {
		jQuery.post('<?php echo site_url('others/get_employee') ?>', {designation_id : designation_id}, function(r) {
			if(r != '') {
				jQuery('#employee_td').html(r);
			}
		});
	}
							
</script>            

<form action="<?php echo site_url('others/new_transfer'); ?>" method="post">
          
    <div class="wrapper clear" >   
    
        <div id="signin">
    
                <h2 id="signinHeader" class="f-kruti cnt-head" align="center">deZpkjh dk VªkalQj </h2>
                <div align="right"><a href="<?php echo site_url('others/view_transfer'); ?>" class="f-loto cnt-head side-heading-small">View old transfer</a></div>
                <hr />
    
                <div class="signin-header">

                    <h4 id="signinHeader">deZpkjh dk VªkalQj </h4>

                    <table width="100%" border="0" class="form">
                    
                        <tr>
                            <th scope="row">foHkkx </th>
                            <td><select name="department_id_from" id="department_id_from" onchange="get_designation_from(this.value)">
                                <option value=""></option>
                                <?php
                                if($department){
                                    foreach($department as $dep){
                                        echo "<option value='".$dep['department_id']."'>".$dep['name']."</option>";
                                    }
                                }
                                ?>
                            </select></td>
                        </tr>
                        
                        <tr>
                            <th width="48%" scope="row">deZpkjh dk in</th>
                            <td width="52%" id="designation_td_from"><select name="designation_id_from" id="designation_id_from" onchange="get_employee(this.value)">
                            <option value=""></option>
                            </select></td>
                        </tr>
                        
                        <tr>
                            <th scope="row">deZpkjh dk uke</th>
                            <td id="employee_td"><select id="employee_id" name="employee_id">
                            <option value=""></option>
                            </select></td>
                        </tr>
                        
                        <tr>
                            <th scope="row">VªkalQj foHkkx </th>
                            <td><select name="department_id_to" id="department_id_to" onchange="get_designation_to(this.value)">
                                <option value=""></option>
                                <?php
                                if($department){
                                    foreach($department as $dep){
                                        echo "<option value='".$dep['department_id']."'>".$dep['name']."</option>";
                                    }
                                }
                                ?>
                            </select></td>
                        </tr>
                        
                        <tr>
                            <th scope="row">VªkalQj in </th>
                            <td id="designation_td_to"><select name="designation_id_to" id="designation_id_to">
                            <option value=""></option>
                            </select></td>
                        </tr>
                        
                        <tr>
                            <th align="right" scope="row"><input type="submit" value="VªkalQj " class="btn btn-primary f-kruti" id="submit3" name="submit3" /></th>
                            <td align="left"><input type="submit" value="Dykst " class="btn btn-primary f-kruti" id="submit2" name="submit2" /></td>
                        </tr>
                    
                    </table>

                </div>

            </div>

        </div>

    </div>

</form>
