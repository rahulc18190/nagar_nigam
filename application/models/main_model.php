<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main_model extends CI_Model {

	public function __construct(){
		
		parent::__construct();
		$this->load->database();		
	}

	public function login($arr){

		$check	=	$this->db->get_where('admin',$arr);
		if($check->num_rows()>0){
			$this->session->set_userdata('admin_id',$check->row()->id);
			$this->session->set_userdata('admin_username',$check->row()->username);
			$this->session->set_userdata('is_admin_login',true);
		}	
		else {
			$this->session->set_flashdata('failure_msg','Failure! Incorrect uername or password.');
		}
	}	
	
	public function get_bank_id(){

		$this->db->order_by('id','desc');
		$this->db->limit('1');
		$data	=	$this->db->get('bank');

		if($data->num_rows()>0){
			$ex	=	explode('-',$data->row()->bank_id);
			return 'B-' . ($ex[1]+1);
		}
		else {
			return 'B-1';
		}
	}
	
	public function get_department_id(){

		$this->db->order_by('id','desc');
		$this->db->limit('1');
		$data	=	$this->db->get('department');

		if($data->num_rows()>0){
			$ex	=	explode('-',$data->row()->department_id);
			return 'DP-' . ($ex[1]+1);
		}
		else {
			return 'DP-1';
		}
	}
	
	public function get_designation_id(){

		$this->db->order_by('id','desc');
		$this->db->limit('1');
		$data	=	$this->db->get('designation');

		if($data->num_rows()>0){
			$ex	=	explode('-',$data->row()->designation_id);
			return 'D-' . ($ex[1]+1);
		}
		else {
			return 'D-1';
		}
	}
	
	public function get_employee_id(){

		$this->db->order_by('id','desc');
		$this->db->limit('1');
		$data	=	$this->db->get('employee');

		if($data->num_rows()>0){
			return $data->row()->employee_id+1;
		}
		else {
			return 1;
		}
	}
	
	public function add_item($table,$arr){

		$this->db->insert($table,$arr);
	}
	
	public function edit_employee($arr){

		$this->db->insert('employee',$arr);
	}
	
	public function get_employee_count($limit=1000, $start=0){
		
		$this->db->select('employee.*,department.name as department,designation.name as designation,bank.name as bank,bank.branch');
		$this->db->join('department','department.department_id = employee.department_id');
		$this->db->join('designation','designation.designation_id = employee.designation_id');
		$this->db->join('bank','bank.bank_id = employee.bank_id');
		return $this->db->limit($limit, $start)->order_by('employee.id','desc')->get('employee')->num_rows();
	}

	public function get_employee_list($limit=1000, $start=0){
		
		$this->db->select('employee.*,department.name as department,designation.name as designation,bank.name as bank,bank.branch');
		$this->db->join('department','department.department_id = employee.department_id');
		$this->db->join('designation','designation.designation_id = employee.designation_id');
		$this->db->join('bank','bank.bank_id = employee.bank_id');
		return $this->db->limit($limit, $start)->order_by('employee.id','desc')->get('employee')->result_array();
	}

	/*public function get_employee_list($limit=1000, $start=0){
		
		$this->db->select('employee.*,department.name as department,designation.name as designation,bank.name as bank,bank.branch');
		$this->db->join('department','department.department_id = employee.department_id');
		$this->db->join('designation','designation.designation_id = employee.designation_id');
		$this->db->join('bank','bank.bank_id = employee.bank_id');
		$arr	=	$this->db->limit($limit, $start)->order_by('employee.id','desc')->get('employee')->result_array();
	
		$temp	=	array();
		foreach($arr as $key=>$val){
		
			$t	=	1;
			if(!empty($temp)	&&	$key	==	'employee_id'){
				echo $key;
				foreach($temp as $key2=>$val2){
					
					if($key2	==	'employee_id'	&&	$val2 == $val){
						echo $val;
						$t	=	0;
						break;
					}
				}
			}
			if($t	==	1) {
				$temp[$key]['id']	=	$val['id'];
				$temp[$key]['employee_id']	=	$val['employee_id'];
				$temp[$key]['name']	=	$val['name'];
				$temp[$key]['father_or_husband_name']	=	$val['father_or_husband_name'];
				$temp[$key]['designation_type']	=	$val['designation_type'];
				$temp[$key]['date_of_joining']	=	$val['date_of_joining'];
				$temp[$key]['date_of_birth']	=	$val['date_of_birth'];
				$temp[$key]['address']	=	$val['address'];
				$temp[$key]['std_code']	=	$val['std_code'];
				$temp[$key]['phone_number']	=	$val['phone_number'];
				$temp[$key]['bank_account_number']	=	$val['bank_account_number'];
				$temp[$key]['pan_number']	=	$val['pan_number'];
				$temp[$key]['gender']	=	$val['gender'];
				$temp[$key]['open_balance']	=	$val['open_balance'];
				$temp[$key]['current_salary']	=	$val['current_salary'];
				$temp[$key]['increment_month']	=	$val['increment_month'];
				$temp[$key]['increment_applicable']	=	$val['increment_applicable'];
				$temp[$key]['sanvida_employee']	=	$val['sanvida_employee'];
				$temp[$key]['maintenance_allowance']	=	$val['maintenance_allowance'];
				$temp[$key]['SixBasic']	=	$val['SixBasic'];
				$temp[$key]['six_pay_apply']	=	$val['six_pay_apply'];
				$temp[$key]['six_bas_pay']	=	$val['six_bas_pay'];
				$temp[$key]['old_basic']	=	$val['old_basic'];
				$temp[$key]['old_basepay']	=	$val['old_basepay'];
				$temp[$key]['department']	=	$val['department'];
				$temp[$key]['designation']	=	$val['designation'];
				$temp[$key]['bank']	=	$val['bank'];
				$temp[$key]['branch']	=	$val['branch'];
			}
		}
		return $temp;
	}
*/
	private function is_in_array($array, $key, $key_value){

		$within_array = 'no';

		foreach( $array as $k=>$v ){

			if( is_array($v) ){
				$within_array = is_in_array($v, $key, $key_value);
				if( $within_array == 'yes' ){
					break;
				}
			} else {
					if( $v == $key_value && $k == $key ){
							$within_array = 'yes';
							break;
					}
			}

		}
		return $within_array;
	}

	public function get_designation_count($limit=1000, $start=0){

		$this->db->select('designation.*,department.name as department,class.name as class');
		$this->db->join('department','department.department_id = designation.department_id');
		$this->db->join('class','class.class_id = designation.class_id');
		return $this->db->limit($limit, $start)->order_by('designation.id','desc')->get('designation')->num_rows();
	}

	public function get_designation_list($limit=1000, $start=0){

		$this->db->select('designation.*,department.name as department,class.name as class');
		$this->db->join('department','department.department_id = designation.department_id');
		$this->db->join('class','class.class_id = designation.class_id');
		return $this->db->limit($limit, $start)->order_by('designation.id','desc')->get('designation')->result_array();
	}

	public function get_department_count($limit=1000, $start=0){

		return $this->db->limit($limit, $start)->order_by('id','desc')->get('department')->num_rows();
	}

	public function get_department_list($limit=1000, $start=0){

		return $this->db->limit($limit, $start)->order_by('id','desc')->get('department')->result_array();
	}

	public function get_six_pay_scale_list($limit=1000, $start=0){

		return $this->db->limit($limit, $start)->order_by('id','asc')->get('vetan_shreni')->result_array();
	}

	public function get_bank_count($limit=1000, $start=0){

		return $this->db->limit($limit, $start)->order_by('id','desc')->get('bank')->num_rows();
	}

	public function get_bank_list($limit=1000, $start=0){

		return $this->db->limit($limit, $start)->order_by('id','desc')->get('bank')->result_array();
	}

	public function get_branch_list($bank_id){

		return $this->db->order_by('id','desc')->get_where('bank',array('bank_id'=>$bank_id))->result_array();
	}

	public function get_bank_list_group_by(){

		return $this->db->group_by('name')->get('bank')->result_array();
	}

	public function get_class_list(){

		return $this->db->get('class')->result_array();
	}

	public function get_loan_list_count(){

		return $this->db->get_where('loan',array('status'=>1))->num_rows();
	}

	public function get_loan_list($limit=1000, $start=0){

		return $this->db->limit($limit,$start)->get_where('loan',array('status'=>1))->result_array();
	}

	public function get_branch($bank_name){

		return $this->db->get_where('bank',array('name'=>$bank_name))->result_array();
	}

	public function get_employee_by_department_id($department_id){

		$this->db->select('employee.*,bank.name as bank_name,bank.branch as branch,department.name as department_name,designation.name as designation_name');
		$this->db->join('department','department.department_id		=	employee.department_id');
		$this->db->join('designation','designation.designation_id	=	employee.designation_id');
		$this->db->join('bank','bank.bank_id						=	employee.bank_id');
		return $this->db->get_where('employee',array('employee.department_id'=>$department_id))->result_array();
	}

	public function get_designation_by_department_id($department_id){

		$this->db->select('designation.*,department.name as department,class.name as class');
		$this->db->join('department','department.department_id = designation.department_id');
		$this->db->join('class','class.class_id = designation.class_id');
		return $this->db->get_where('designation',array('designation.department_id'=>$department_id))->result_array();
	}

	public function get_employee_by_department_id2($department_id){

		return $this->db->get_where('employee',array('employee.department_id'=>$department_id))->result_array();
	}

	public function get_designation($department_id){

		return $this->db->get_where('designation',array('department_id'=>$department_id))->result_array();
	}

	public function get_employee_details($employee_id){

		$this->db->select('employee.*, vetan_shreni.six_min_salary, vetan_shreni.six_max_salary, vetan_shreni.six_grade_pay, bank.name as bank_name, bank.branch, designation.name as designation_name');
		$this->db->join('bank','bank.bank_id=employee.bank_id');
		$this->db->join('designation','designation.designation_id=employee.designation_id');
		$this->db->join('vetan_shreni','vetan_shreni.six_pay_grade_id=designation.6_pay_grade_id');
		return $this->db->get_where('employee',array('employee_id'=>$employee_id))->row();
	}

	public function get_employee($designation_id){

		return $this->db->get_where('employee',array('designation_id'=>$designation_id))->result_array();
	}

	public function get_employee_by_id($id){

		return $this->db->get_where('employee',array('id'=>$id))->row();
	}

	public function get_entry_count($table_name){

		return $this->db->get($table_name)->num_rows();
	}	

	public function change_password($arr){

		$this->db->select('*');
		$this->db->where(array('password'=>md5($arr['old_password'])));
		$this->db->from('admin');
		$getData = $this->db->get();
		if($getData->num_rows() > 0){
			$this->db->update('admin',array('password'=>md5($arr['new_password'])));
			return 1;
		}
	}
	
	public function get_user_list(){
	
		return $this->db->where('username !=','admin')->get('admin')->result_array();
	}
	
	public function check_availability($username){

		return $this->db->get_where('admin',array('username'=>$username))->num_rows();
	}

	public function create_user($arr,$id){
		
		if($id	==	0){
			return $this->db->insert('admin',$arr);
		}
		else {
			return $this->db->where('id',$id)->update('admin',$arr);
		}
	}
	
	public function delete_user($id){
		
		$this->db->delete('admin',array('id' => $id));
		$this->session->set_flashdata('success_msg','Success! Record deleted successfully.');
		return 1;
	}
	
	public function get_user_detail($id){
		
		return $this->db->get_where('admin',array('id'=>$id))->row();
	}
	
	public function change_user_status($status, $id){
		
		$this->db->where(array('id' => $id))->update('admin',array('status' => $status));
		$this->session->set_flashdata('success_msg','Success! Record updated successfully.');
		return 1;
	}
	
	public function transfer($arr){
		
		$this->db->insert('transfer',$arr);
		$this->db->where('employee_id',$arr['employee_id'])->update('employee',array('department_id'=>$arr['new_department'],'designation_id'=>$arr['new_designation']));
		$this->session->set_flashdata('success_msg','Success! Transfer done successfully.');
	}
	
	public function get_transfer_list($limit=1000, $start=0){
	
		$result	=	$this->db->limit($limit, $start)->order_by('id','desc')->get('transfer');
		
		$arr	=	array();
		if($result->num_rows()>0){
			
			$count = 0;
			foreach($result->result_array() as $res){
				
				$arr[$count]['employee_name']	=	$this->db->get_where('employee',array('employee_id'=>$res['employee_id']))->row()->name;
				$arr[$count]['old_department']	=	$this->db->get_where('department',array('department_id'=>$res['old_department']))->row()->name;
				$arr[$count]['old_designation']	=	$this->db->get_where('designation',array('designation_id'=>$res['old_designation']))->row()->name;
				$arr[$count]['new_department']	=	$this->db->get_where('department',array('department_id'=>$res['new_department']))->row()->name;
				$arr[$count]['new_designation']	=	$this->db->get_where('designation',array('designation_id'=>$res['new_designation']))->row()->name;
				$arr[$count]['transfer_date']	=	date('d-m-Y',strtotime($res['current_date_time']));
				$count++;
			}
		}
		return $arr;				
	}
	
	public function get_loan_id(){
		
		$loan_id	=	$this->db->limit(1)->order_by('id','desc')->get('loan')->row()->loan_id;
		if($loan_id	!=	''){
			return ($loan_id+1);
		}
		else {
			return 1;
		}
	}

	public function add_loan($arr){
		
		$this->db->insert('loan',$arr);
		$this->session->set_flashdata('success_msg','Success! New Record Added Successfully.');
	}	

	public function edit_loan($arr){
		
		$this->db->where(array('id'=>$arr['id']))->update('loan',array('name'=>$arr['name']));
	}	
	
	
	public function get_classs($arr){
		
		return $this->db->get_where('class',array('status'=>1))->result_array();
	}	

	public function get_class_id(){
		
		$class_id	=	$this->db->limit(1)->order_by('id','desc')->get('class')->row()->class_id;
		if($class_id	!=	''){
			return ($class_id+1);
		}
		else {
			return 1;
		}
	}

	public function add_class($arr){
		
		$this->db->insert('class',$arr);
		$this->session->set_flashdata('success_msg','Success! New Record Added Successfully.');
	}	

	public function edit_class($arr){
		
		$this->db->where(array('id'=>$arr['id']))->update('class',array('name'=>$arr['name']));
	}	
	
	public function get_vetan_shreni_id(){
		
		$vetan_shreni_id	=	$this->db->limit(1)->order_by('id','desc')->get('vetan_shreni')->row()->vetan_shreni_id;
		if($vetan_shreni_id	!=	''){
			return ($vetan_shreni_id+1);
		}
		else {
			return 1;
		}
	}
	
	public function get_increment(){
		
		return $this->db->get_where('increment',array('status'=>1))->row()->dearly_allowance_increment;
	}

	public function edit_increment($dearly_allowance_increment){
		
		return $this->db->where(array('status'=>1))->update('increment',array('dearly_allowance_increment'=>$dearly_allowance_increment));
	}
	// Report Section
	
	function get_bank_report($bank_id,$month,$limit=1000000, $start=0){
		
		if($bank_id	==	'All'){
			$this->db->select('employee.name as employee_name,employee.bank_account_number,employee.current_salary,bank.name as bank_name,bank.branch');
			$this->db->join('bank','bank.bank_id	=	employee.bank_id');
			return $this->db->limit($limit, $start)->get_where('employee',array('employee.date_of_joining <'=>date('Y-m-d',strtotime('01-'.$month))))->result_array();
		}
		else {
			$this->db->select('employee.name as employee_name,employee.bank_account_number,employee.current_salary,bank.name as bank_name,bank.branch');
			$this->db->join('bank','bank.bank_id	=	employee.bank_id');
			return $this->db->limit($limit, $start)->get_where('employee',array('employee.bank_id'=>$bank_id,'employee.date_of_joining <'=>date('Y-m-d',strtotime('01-'.$month))))->result_array();
		}
	}

	function get_bank_report_count($bank_id,$month,$limit=1000000, $start=0){
		
		if($bank_id	==	'All'){
			$this->db->select('employee.name as employee_name,employee.bank_account_number,employee.current_salary,bank.name as bank_name,bank.branch');
			$this->db->join('bank','bank.bank_id	=	employee.bank_id');
			return $this->db->limit($limit, $start)->get_where('employee',array('employee.date_of_joining <'=>date('Y-m-d',strtotime('01-'.$month))))->num_rows();
		}
		else {
			$this->db->select('employee.name as employee_name,employee.bank_account_number,employee.current_salary,bank.name as bank_name,bank.branch');
			$this->db->join('bank','bank.bank_id	=	employee.bank_id');
			return $this->db->limit($limit, $start)->get_where('employee',array('employee.bank_id'=>$bank_id,'employee.date_of_joining <'=>date('Y-m-d',strtotime('01-'.$month))))->num_rows();
		}
	}

	function get_department_report($department_id,$month,$limit=1000000, $start=0){
		
		if($department_id	==	'All'){
			$this->db->select('employee.name as employee_name,employee.current_salary as current_salary,designation.*');
			$this->db->join('designation','designation.designation_id	=	employee.designation_id');
			return $this->db->limit($limit, $start)->get_where('employee',array('employee.date_of_joining <'=>date('Y-m-d',strtotime('01-'.$month))))->result_array();
		}
		else {
			$this->db->select('employee.name as employee_name,employee.current_salary as current_salary,designation.*');
			$this->db->join('designation','designation.designation_id	=	employee.designation_id');
			return $this->db->limit($limit, $start)->get_where('employee',array('employee.department_id'=>$department_id,'employee.date_of_joining <'=>date('Y-m-d',strtotime('01-'.$month))))->result_array();
		}
	}

	function get_department_report_count($department_id,$month,$limit=1000000, $start=0){
		
		if($department_id	==	'All'){
			$this->db->select('employee.name as employee_name,employee.current_salary as current_salary,designation.*');
			$this->db->join('designation','designation.designation_id	=	employee.designation_id');
			return $this->db->limit($limit, $start)->get_where('employee',array('employee.date_of_joining <'=>date('Y-m-d',strtotime('01-'.$month))))->num_rows();
		}
		else {
			$this->db->select('employee.name as employee_name,employee.current_salary as current_salary,designation.*');
			$this->db->join('designation','designation.designation_id	=	employee.designation_id');
			return $this->db->limit($limit, $start)->get_where('employee',array('employee.department_id'=>$department_id,'employee.date_of_joining <'=>date('Y-m-d',strtotime('01-'.$month))))->num_rows();
		}
	}

	
	function get_department_report2($vetak_patrak,$department_id,$month,$limit=100000,$start=0){
		
		if($vetak_patrak	==	1){
		
		}
		else {
			
			if($department_id	!=	'All'){
			
				$where	=	array('department_id'=>$department_id,'status'=>1);
			}
			else {
			
				$where	=	array('status'=>1);
			}

			$department_list	=	$this->db->get_where('department',$where)->result_array();

			foreach($department_list as $key=>$dept){
			
				$temp[$key]['department_name']			=		$dept['name'];
				$temp[$key]['total_payment']			=		0;
				$temp[$key]['GPF_Loan']					=		0;
				$temp[$key]['family_benifit_fund']		=		0;
				$temp[$key]['grain_loan']				=		0;
				$temp[$key]['other_katotra']			=		0;
				$temp[$key]['cash_payment']				=		0;
				
				$this->db->join('designation','designation.designation_id	=	employee.designation_id');	
				$emp_data	=	$this->db->limit($limit, $start)->get_where('employee',
								array('employee.department_id'=>$dept['department_id']))->result_array();
				
				foreach($emp_data	as $val){
				
					$temp[$key]['GPF_Loan']					+=		$val['GPF_Loan'];
					$temp[$key]['family_benifit_fund']		+=		$val['family_benifit_fund'];
					$temp[$key]['grain_loan']				+=		$val['grain_loan'];
				}
			}

			return $temp;
		}
	}

	function get_department_report2_count($vetak_patrak,$department_id,$month){

		if($vetak_patrak	==	1){
		
		}
		else {
			
			if($department_id	!=	'All'){
			
				$where	=	array('department_id'=>$department_id,'status'=>1);
			}
			else {
			
				$where	=	array('status'=>1);
			}

			return $this->db->get_where('department',$where)->num_rows();
		}
	}

	function get_pay_sleep_report($department_id,$employee_id,$month,$limit=1000000, $start=0){
	
		if($department_id	==	'All' && $employee_id	==	'All'){	
			
			$where	=	array('employee.date_of_joining <'=>date('Y-m-d',strtotime('01-'.$month)));
		}
		else if($department_id	==	'All' && $employee_id	!=	'All'){

			$where	=	array('employee.employee_id'=>$employee_id,'employee.date_of_joining <'=>date('Y-m-d',strtotime('01-'.$month)));
		}
		else if($department_id	!=	'All' && $employee_id	==	'All'){

			$where	=	array('employee.department_id'=>$department_id,'employee.date_of_joining <'=>date('Y-m-d',strtotime('01-'.$month)));
		}
		else if($department_id	!=	'All' && $employee_id	!=	'All'){

			$where	=	array('employee.employee_id'=>$employee_id,'employee.date_of_joining <'=>date('Y-m-d',strtotime('01-'.$month)));
		}
		

		$this->db->select('employee.name as employee_name,employee.pan_number as pan_number, employee.current_salary as current_salary, employee.bank_account_number as account_number, bank.name as bank_name, bank.branch, department.name as department_name,designation.*');

		$this->db->join('designation','designation.designation_id	=	employee.designation_id');
		$this->db->join('department','department.department_id		=	employee.department_id');
		$this->db->join('bank','bank.bank_id						=	employee.bank_id');

		return $this->db->limit($limit, $start)->get_where('employee',$where)->result_array();
	}
	
	function get_pay_sleep_report_count($department_id,$employee_id,$month,$limit=1000000, $start=0){
	
		if($department_id	==	'All' && $employee_id	==	'All'){	
			
			$where	=	array('employee.date_of_joining <'=>date('Y-m-d',strtotime('01-'.$month)));
		}
		else if($department_id	==	'All' && $employee_id	!=	'All'){

			$where	=	array('employee.employee_id'=>$employee_id,'employee.date_of_joining <'=>date('Y-m-d',strtotime('01-'.$month)));
		}
		else if($department_id	!=	'All' && $employee_id	==	'All'){

			$where	=	array('employee.department_id'=>$department_id,'employee.date_of_joining <'=>date('Y-m-d',strtotime('01-'.$month)));
		}
		else if($department_id	!=	'All' && $employee_id	!=	'All'){

			$where	=	array('employee.employee_id'=>$employee_id,'employee.date_of_joining <'=>date('Y-m-d',strtotime('01-'.$month)));
		}
		

		$this->db->select('employee.name as employee_name,employee.pan_number as pan_number, employee.current_salary as current_salary, employee.bank_account_number as account_number, bank.name as bank_name, bank.branch, department.name as department_name,designation.*');

		$this->db->join('designation','designation.designation_id	=	employee.designation_id');
		$this->db->join('department','department.department_id		=	employee.department_id');
		$this->db->join('bank','bank.bank_id						=	employee.bank_id');

		return $this->db->limit($limit, $start)->get_where('employee',$where)->num_rows();
	}
	
	function get_katotra_report_count($department_wise,$month,$limit=100000, $start=0){

		if($department_wise	==	1){
			return $this->db->limit($limit, $start)->get_where('department',array('status'=>1))->num_rows();
		}
		else {
			return 1;		
		}
	}

	function get_katotra_report($department_wise,$month,$limit=100000, $start=0){
	
		if($department_wise	==	1){
			
			$department_list	=	$this->db->limit($limit, $start)->get_where('department',array('status'=>1))->result_array();
			
			$temp	=	array();
			foreach($department_list as $key=>$val){
				
				$temp[$key]['department_name']		=	$val['name'];
				$temp[$key]['GPF']					=	0;
				$temp[$key]['GPF_Loan']				=	0;
				$temp[$key]['family_benifit_fund']	=	0;
				$temp[$key]['grain_loan']			=	0;
				$temp[$key]['professional_tax']		=	0;
				$temp[$key]['bima_premium']			=	0;
				$temp[$key]['CTD']					=	0;
				$temp[$key]['mo_cycle_loan']		=	0;
				$temp[$key]['cycle_loan']			=	0;
				$temp[$key]['house_loan']			=	0;
				$temp[$key]['napa_bhandar']			=	0;
				$temp[$key]['harijan_pedi']			=	0;
				$temp[$key]['home_rent']			=	0;
				$temp[$key]['water_rent']			=	0;
				$temp[$key]['electricity_rent']		=	0;

				
				$this->db->join('designation','designation.designation_id	=	employee.designation_id');
				$employee_list	=	$this->db->get_where('employee',array('employee.department_id'=>$val['department_id']))->result_array();
				
				foreach($employee_list as $key2=>$val2){
				
					$temp[$key]['GPF']					=	$temp[$key]['GPF']					+	$val2['GPF'];
					$temp[$key]['GPF_Loan']				=	$temp[$key]['GPF_Loan']				+	$val2['GPF_Loan'];
					$temp[$key]['family_benifit_fund']	=	$temp[$key]['family_benifit_fund']	+	$val2['family_benifit_fund'];
					$temp[$key]['grain_loan']			=	$temp[$key]['grain_loan']			+	$val2['grain_loan'];
					$temp[$key]['professional_tax']		=	$temp[$key]['professional_tax']		+	$val2['professional_tax'];
					$temp[$key]['bima_premium']			=	$temp[$key]['bima_premium']			+	$val2['bima_premium'];
					$temp[$key]['CTD']					=	$temp[$key]['CTD']					+	$val2['CTD'];
					$temp[$key]['mo_cycle_loan']		=	$temp[$key]['mo_cycle_loan']		+	$val2['mo_cycle_loan'];
					$temp[$key]['cycle_loan']			=	$temp[$key]['cycle_loan']			+	$val2['cycle_loan'];
					$temp[$key]['house_loan']			=	$temp[$key]['house_loan']			+	$val2['house_loan'];
					$temp[$key]['napa_bhandar']			=	$temp[$key]['napa_bhandar']			+	$val2['napa_bhandar'];
					$temp[$key]['harijan_pedi']			=	$temp[$key]['harijan_pedi']			+	$val2['harijan_pedi'];
					$temp[$key]['home_rent']			=	$temp[$key]['home_rent']			+	$val2['home_rent'];
					$temp[$key]['water_rent']			=	$temp[$key]['water_rent']			+	$val2['water_rent'];
					$temp[$key]['electricity_rent']		=	$temp[$key]['electricity_rent']		+	$val2['electricity_rent'];
				}
			}
			return $temp;
		}
		else {							
			$temp['GPF']					=	0;
			$temp['GPF_Loan']				=	0;
			$temp['family_benifit_fund']	=	0;
			$temp['grain_loan']				=	0;
			$temp['professional_tax']		=	0;
			$temp['bima_premium']			=	0;
			$temp['CTD']					=	0;
			$temp['mo_cycle_loan']			=	0;
			$temp['cycle_loan']				=	0;
			$temp['house_loan']				=	0;
			$temp['napa_bhandar']			=	0;
			$temp['harijan_pedi']			=	0;
			$temp['home_rent']				=	0;
			$temp['water_rent']				=	0;
			$temp['electricity_rent']		=	0;


			$this->db->join('designation','designation.designation_id	=	employee.designation_id');
			$employee_list	=	$this->db->get_where('employee',array('employee.status'=>1))->result_array();
			
			foreach($employee_list as $key2=>$val2){
			
				$temp['GPF']					=	$temp['GPF']					+	$val2['GPF'];
				$temp['GPF_Loan']				=	$temp['GPF_Loan']				+	$val2['GPF_Loan'];
				$temp['family_benifit_fund']	=	$temp['family_benifit_fund']	+	$val2['family_benifit_fund'];
				$temp['grain_loan']				=	$temp['grain_loan']				+	$val2['grain_loan'];
				$temp['professional_tax']		=	$temp['professional_tax']		+	$val2['professional_tax'];
				$temp['bima_premium']			=	$temp['bima_premium']			+	$val2['bima_premium'];
				$temp['CTD']					=	$temp['CTD']					+	$val2['CTD'];
				$temp['mo_cycle_loan']			=	$temp['mo_cycle_loan']			+	$val2['mo_cycle_loan'];
				$temp['cycle_loan']				=	$temp['cycle_loan']				+	$val2['cycle_loan'];
				$temp['house_loan']				=	$temp['house_loan']				+	$val2['house_loan'];
				$temp['napa_bhandar']			=	$temp['napa_bhandar']			+	$val2['napa_bhandar'];
				$temp['harijan_pedi']			=	$temp['harijan_pedi']			+	$val2['harijan_pedi'];
				$temp['home_rent']				=	$temp['home_rent']				+	$val2['home_rent'];
				$temp['water_rent']				=	$temp['water_rent']				+	$val2['water_rent'];
				$temp['electricity_rent']		=	$temp['electricity_rent']		+	$val2['electricity_rent'];
			}
			
			return $temp;
		}
	}

	function get_loan_report_count($month){

		$this->db->join('designation','designation.designation_id	=	employee.designation_id');
		return $this->db->get_where('employee',array('employee.status'=>1))->num_rows();		
	}

	function get_loan_report($month,$limit=100000, $start=0){
		
		$this->db->select('employee.name as employee_name, designation.*');
		$this->db->join('designation','designation.designation_id	=	employee.designation_id');
		return $this->db->limit($limit, $start)->get_where('employee',array('employee.status'=>1))->result_array();		
	}

	function get_monthly_employee_report_count($department_id,$employee_id,$month_from,$month_to){

		return 50;
	}

	function get_monthly_employee_report($department_id,$employee_id,$month_from,$month_to,$limit=100000,$start){

/*		if($department_id	!=	'All'){
		
			$where	=	array('department_id'=>$department_id,'status'=>1);
		}
		else {
		
			$where	=	array('status'=>1);
		}

		$department_list	=	$this->db->get_where('department',$where)->result_array();
		
		$temp	=	array();
		foreach($department_list as $key=>$dept){
		
			$this->db->join('designation','designation.designation_id	=	employee.designation_id');	

			$emp_data	=	$this->db->limit($limit, $start)->get_where('employee',
							array('employee.department_id'=>$dept['department_id']))->result_array();

			foreach($emp_data as $val){
			
				
			}
		}			
*/	}	

	function get_loan_full_report_count($loan_id,$month){
		
		if($loan_id	==	'All'){
		
			$min_date	=	 date('Y-m-d',strtotime($month));
			$max_date	=	 date('Y-m-t',strtotime($month));
			
			$this->db->select('employee.name as employee_name, loan.name as loan_name, employee_loan.*');
			$this->db->join('employee','employee.employee_id	=	employee_loan.employee_id');
			$this->db->join('loan','loan.loan_id	=	employee_loan.loan_id');
			return $this->db->order_by('id','ASC')->limit($limit, $start)->get_where('employee_loan',array('employee_loan.status'=>1,'employee_loan.month >='=>$min_date,'employee_loan.month <='=>$max_date))->num_rows();
		}
		else {
		
			$min_date	=	 date('Y-m-d',strtotime($month));
			$max_date	=	 date('Y-m-t',strtotime($month));
			
			$this->db->select('employee.name as employee_name, employee_loan.*');
			$this->db->join('employee','employee.employee_id	=	employee_loan.employee_id');
			return $this->db->order_by('id','ASC')->limit($limit, $start)->get_where('employee_loan',array('employee_loan.status'=>1,'employee_loan.loan_id'=>$loan_id,'employee_loan.month >='=>$min_date,'employee_loan.month <='=>$max_date))->num_rows();
		}
	}
	
	function get_loan_full_report($loan_id,$month,$limit=100000, $start=0){
		
		if($loan_id	==	'All'){
		
			$min_date	=	 date('Y-m-d',strtotime($month));
			$max_date	=	 date('Y-m-t',strtotime($month));
			
			$this->db->select('employee.name as employee_name, loan.name as loan_name, employee_loan.*');
			$this->db->join('employee','employee.employee_id	=	employee_loan.employee_id');
			$this->db->join('loan','loan.loan_id	=	employee_loan.loan_id');
			return $this->db->order_by('id','ASC')->limit($limit, $start)->get_where('employee_loan',array('employee_loan.status'=>1,'employee_loan.month >='=>$min_date,'employee_loan.month <='=>$max_date))->result_array();
		}
		else {
		
			$min_date	=	 date('Y-m-d',strtotime($month));
			$max_date	=	 date('Y-m-t',strtotime($month));
			
			$this->db->select('employee.name as employee_name, employee_loan.*');
			$this->db->join('employee','employee.employee_id	=	employee_loan.employee_id');
			return $this->db->order_by('id','ASC')->limit($limit, $start)->get_where('employee_loan',array('employee_loan.status'=>1,'employee_loan.loan_id'=>$loan_id,'employee_loan.month >='=>$min_date,'employee_loan.month <='=>$max_date))->result_array();
		}
	}
	
	function get_allowance_description_report_count($department_id,$month,$limit=100000, $start=0){
		
		if($department_id	!=	'All'){
		
			$where	=	array('department_id '=> $department_id,'status'=>1);
		}
		else {
		
			$where	=	array('status'=>1);
		}
		return $this->db->limit($limit, $start)->get_where('department',$where)->num_rows();
	}

	function get_allowance_description_report($department_id,$month,$limit=100000,$start=0){
		
		if($department_id	!=	'All'){
		
			$where	=	array('department_id '=> $department_id,'status'=>1);
		}
		else {
		
			$where	=	array('status'=>1);
		}

		$department_list	=	$this->db->limit($limit, $start)->get_where('department',$where)->result_array();
		
		foreach($department_list as $key=>$dept){
			
			$temp[$key]['department_name']			=	$dept['name'];
			$temp[$key]['current_salary']			=	0;
			$temp[$key]['special_salary']			=	0;

			$temp[$key]['dearly_allowance']			=	0;
			$temp[$key]['home_rent_allowance']		=	0;
			$temp[$key]['city_damage_allowance']	=	0;
			
			$temp[$key]['medical_allowance']		=	0;
			$temp[$key]['interim_relief']			=	0;
			$temp[$key]['travelling_allowance']		=	0;

			$this->db->join('designation','designation.designation_id	=	employee.designation_id');	
			$emp_data	=	$this->db->get_where('employee',array('employee.department_id'=>$dept['department_id'],'employee.date_of_joining <'=>date('Y-m-d',strtotime('01-'.$month))))->result_array();
			
			foreach($emp_data	as $val){
			
				$temp[$key]['current_salary']			+=	$val['current_salary'];
				$temp[$key]['special_salary']			+=	$val['special_salary'];
	
				$temp[$key]['dearly_allowance']			+=	$val['dearly_allowance'];
				$temp[$key]['home_rent_allowance']		+=	$val['home_rent_allowance'];
				$temp[$key]['city_damage_allowance']	+=	$val['city_damage_allowance'];
				
				$temp[$key]['medical_allowance']		+=	$val['medical_allowance'];
				$temp[$key]['interim_relief']			+=	$val['interim_relief'];
				$temp[$key]['travelling_allowance']		+=	$val['travelling_allowance'];
	
			}
		}
		return $temp;
	}

	function get_bank_name($bank_id){
		return $this->db->get_where('bank',array('bank_id'=>$bank_id))->row()->name;
	}
	function get_branch_name($bank_id){
		return $this->db->get_where('bank',array('bank_id'=>$bank_id))->row()->branch;
	}
	function get_department_name($department_id){
		return $this->db->get_where('department',array('department_id'=>$department_id))->row()->name;
	}
	function get_employee_name($employee_id){
		return $this->db->get_where('employee',array('employee_id'=>$employee_id))->row()->name;
	}
	function get_loan_name($loan_id){
		return $this->db->get_where('loan',array('loan_id'=>$loan_id))->row()->name;
	}

	function get_full_employee_list(){
		return $this->db->get_where('employee',array('status'=>1))->result_array();
	}

	function get_pay_band($designation_id){
	
		$six_pay_grade_id	=	$this->db->get_where('designation',array('designation_id'=>$designation_id))->row('6_pay_grade_id');
		$six_pay			=	$this->db->get_where('vetan_shreni',array('six_pay_grade_id'=>$six_pay_grade_id))->row();
		return $six_pay->six_min_salary.' - '.$six_pay->six_max_salary.' - '.$six_pay->six_grade_pay;
	}	

	function get_vetan_shreni($six_pay_grade_id){
	
		return $this->db->get_where('vetan_shreni',array('six_pay_grade_id'=>$six_pay_grade_id))->row();
	}	

/*	function update_date(){
	
		$list	=	$this->db->get('emp_loan_1')->result_array();
		
		foreach($list as $val){
					
			$this->db->where('id',$val['id'])->update('emp_loan_1',array('MonthYear'=>date('Y-m-d',strtotime("30-12-1899" . " +".$val['MonthYear']." days"))));
		}
	}*/
	
	function delete_item($id,$table){
		
		$this->db->delete($table,array('id'=>$id));
		return 1;
	}
	
	function get_synchronization_status(){
	
		return $this->db->get_where('pay_slip',array('month'=>date('F-Y')))->num_rows();
	}
	
	function get_synchronization_status2(){
	
		return $this->db->get_where('increment_sync_table',array('month'=>date('F-Y')))->num_rows();
	}
	
	function pay_slip_synchronization(){
	
		if($this->get_synchronization_status()	==	0){
		
		
			$employees	=	$this->db->get_where('employee',array('status'=>1))->result_array();
	
			if($employees){
				
				foreach($employees as $employee){

					if($this->db->get_where('salary',array('employee_id'=>$employee['employee_id'],'month'=>date('F-Y')))->num_rows()	==	0){
				
						$arr['employee_id']				=	$employee['employee_id'];
						$arr['department_id']			=	$employee['department_id'];
						$arr['designation_id']			=	$employee['designation_id'];
						$arr['month']					=	date('F-Y');
						$arr['bank']					=	$employee['bank_id'];
						$arr['branch']					=	0;					
						$arr['account_number']			=	$employee['bank_account_number'];
						$arr['attendance']				=	31;
						$arr['core_salary']				=	0;
						$arr['dearness_core_salary']	=	0;
						$arr['total_core_balance']		=	0;
						$arr['cash_payment']			=	0;
						$arr['special_salary']			=	0;
						$arr['personal_salary']			=	0;
						$arr['salary_grade']			=	0;
						$arr['6_pay_grade']				=	0;
						$arr['holidays']				=	0;
						$arr['zero_salary']				=	0;
						$arr['dearly_allowance']		=	0;
						$arr['home_rent_allowance']		=	0;
						$arr['city_damage_allowance']	=	0;
						$arr['medical_allowance']		=	0;
						$arr['interim_relief']			=	0;
						$arr['travelling_allowance']	=	0;
						$arr['GPF']						=	0;
						$arr['GPF_Loan']				=	0;
						$arr['family_benifit_fund']		=	0;
						$arr['grain_loan']				=	0;
						$arr['professional_tax']		=	0;
						$arr['bima_premium']			=	0;
						$arr['CTD']						=	0;
						$arr['mo_cycle_loan']			=	0;
						$arr['cycle_loan']				=	0;
						$arr['house_loan']				=	0;
						$arr['napa_bhandar']			=	0;
						$arr['harijan_pedi']			=	0;
						$arr['home_rent']				=	0;
						$arr['water_rent']				=	0;
						$arr['electricity_rent']		=	0;
	
						$this->db->insert('salary',$arr);
					}
				}
			}
	
			$this->db->insert('pay_slip',array('month'=>date('F-Y')));
		}
	}

	function increment_synchronization(){
	
		$this->db->insert('increment_sync_table',array('month'=>date('F-Y')));
	}

}
?>