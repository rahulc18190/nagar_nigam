<?php

class HOME_Controller extends CI_Controller {
	//Site global layout
	public $layout_view = 'template/template';

	function __construct() {
		parent::__construct();

		$this->load->library('layout');

		$this->layout->css(base_url().'assets/css/style.css');
		$this->layout->css(base_url().'assets/css/pagination.css');
		$this->layout->js(base_url().'assets/js/jquery.min.js');
		$this->layout->js(base_url().'assets/js/scrolltopcontrol.js');
		$this->layout->js(base_url().'assets/js/script.js');
		$this->layout->js('http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js');
	}
}
?>