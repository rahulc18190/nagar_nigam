<?php
class Others extends HOME_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model("main_model","main");		
		$this->load->library('pagination');		

		$config['full_tag_open'] 	= '<ul class="pagination pagination-sm no-margin pull-right">';
		$config['full_tag_close'] 	= '</ul>';
		$config['first_link'] 		= 'First';
		$config['first_tag_open'] 	= '<li>';
		$config['first_tag_close'] 	= '</li>';
		$config['last_link'] 		= 'Last';
		$config['last_tag_open'] 	= '<li>';
		$config['last_tag_close'] 	= '</li>';
		$config['next_link'] 		= 'Next &gt;';
		$config['next_tag_open'] 	= '<li>';
		$config['next_tag_close'] 	= '</li>';
		$config['prev_link'] 		= '&lt; Prev';
		$config['prev_tag_open'] 	= '<li>';
		$config['prev_tag_close'] 	= '</li>';
		$config['cur_tag_open'] 	= '<li class="active"><a href="#">';
		$config['cur_tag_close'] 	= '</a></li>';
		$config['num_tag_open'] 	= '<li>';
		$config['num_tag_close'] 	= '</li>';
		$this->pagination->initialize($config);

		if(!$this->session->userdata('is_admin_login')){
			redirect('home');
		}	
	}
	
	public function index(){
		$this->layout->view('index');
	}

	public function transfer(){
		$data['menu']			=	'others';
		$data['sub_menu']		=	'transfer';
		$data['department']		=	$this->main->get_department_list();
		$this->layout->view('others/transfer',$data);
	}

	public function new_transfer(){
		
		if($this->input->post()){

			$arr['old_department']	=	$this->input->post('department_id_from');
			$arr['old_designation']	=	$this->input->post('designation_id_from');
			$arr['employee_id']		=	$this->input->post('employee_id');
			$arr['new_department']	=	$this->input->post('department_id_to');
			$arr['new_designation']	=	$this->input->post('designation_id_to');
			
			$this->main->transfer($arr);
		}
		
		redirect('others/transfer');
	}

	public function view_transfer(){
		
		$config['base_url'] = site_url('others/view_transfer');
		$config['total_rows'] = $this->main->get_entry_count('transfer');
		$config['per_page'] = 5;
		
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

		$data["count"] 			= 	$this->uri->segment(3)+1;
		$data["links"] 			= 	$this->pagination->create_links();
		$data['menu']			=	'others';
		$data['sub_menu']		=	'transfer';
		$data['transfer']		=	$this->main->get_transfer_list($config["per_page"], $page);
		$this->layout->view('others/view_transfer',$data);
	}

	public function loan(){

		$config['base_url'] 	= site_url('others/loan');
		$config['total_rows'] 	= $this->main->get_loan_list_count();
		$config['per_page'] 	= 20;
		
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

		$data["count"] 			= 	$this->uri->segment(3)+1;
		$data["links"] 			= 	$this->pagination->create_links();

		$data['menu']			=	'others';
		$data['sub_menu']		=	'loan';
		$data['loan']			=	$this->main->get_loan_list($config["per_page"], $page);
		$data['loan_id']		=	$this->main->get_loan_id();

		$this->layout->view('others/loan',$data);
	}

	public function add_loan(){
	
		if($this->input->post()){

			$arr['loan_id']		=	$this->input->post('loan_id');
			$arr['name']		=	$this->input->post('name');			
			$this->main->add_loan($arr);
		}		
		redirect('others/loan');
	}
	
	public function edit_loan(){
	
		if($this->input->post()){

			$arr['name']		=	$this->input->post('name');			
			$arr['id']		=	$this->input->post('id');			
			$this->main->edit_loan($arr);
		}		
	}

	public function classs(){
		$data['menu']			=	'others';
		$data['sub_menu']		=	'class';
		$data['class']			=	$this->main->get_classs();
		$data['class_id']		=	$this->main->get_class_id();

		$this->layout->view('others/grade',$data);
	}
	
	public function add_class(){
	
		if($this->input->post()){

			$arr['class_id']	=	$this->input->post('class_id');
			$arr['name']		=	$this->input->post('name');			
			$this->main->add_class($arr);
		}		
		redirect('others/classs');
	}
	
	public function edit_class(){
	
		if($this->input->post()){

			$arr['name']		=	$this->input->post('name');			
			$arr['id']			=	$this->input->post('id');			
			$this->main->edit_class($arr);
		}		
	}

	public function get_designation_from(){
		
		if($this->input->post()){
			$arr	=	$this->main->get_designation($this->input->post('department_id'));
			if($arr	!= ''){
				echo '<select name="designation_id_from" id="designation_id_from" onchange="get_employee(this.value)">
						<option></option>';
						foreach($arr as $designation){
							echo "<option value='".$designation['designation_id']."'>".$designation['name']."</option>";	
						}
				echo '</select>';
			}
		}
	}
	
	public function get_designation_to(){
		
		if($this->input->post()){
			$arr	=	$this->main->get_designation($this->input->post('department_id'));
			if($arr	!= ''){
				echo '<select name="designation_id_to" id="designation_id_to">
						<option></option>';
						foreach($arr as $designation){
							echo "<option value='".$designation['designation_id']."'>".$designation['name']."</option>";	
						}
				echo '</select>';
			}
		}
	}
	
	public function get_employee(){
		
		if($this->input->post()){
			$arr	=	$this->main->get_employee($this->input->post('designation_id'));
			if($arr	!= ''){
				echo '<select name="employee_id" id="employee_id">
						<option></option>';
						foreach($arr as $employee){
							echo "<option value='".$employee['employee_id']."'>".$employee['name']."</option>";	
						}
				echo '</select>';
			}
		}
	}

	public function vetan_shreni(){
		$data['menu']				=	'others';
		$data['sub_menu']			=	'vetan_shreni';
		$data['vetan_shreni_id']	=	$this->main->get_vetan_shreni_id();
		$this->layout->view('others/vetan_shreni',$data);
	}
	
	public function increment(){
	
		$data['menu']							=	'others';
		$data['sub_menu']						=	'increment';
		$data['dearly_allowance_increment']		=	$this->main->get_increment();
		$this->layout->view('others/increment',$data);
	}

	public function increment_synchronization(){
	
		$data['menu']				=	'others';
		$data['sub_menu']			=	'increment_synchronization';
		$data['status']				=	$this->main->get_synchronization_status2();
		$this->layout->view('others/increment_synchronization',$data);
	}
	
	public function start_synchronization(){
		
		if(date('m')!=7){
			echo 0;
		}
		else {
			$this->main->increment_synchronization();
			echo 1;
		}
	}

	public function edit_increment(){
	
		$data['dearly_allowance_increment']		=	$this->main->edit_increment($this->input->post('dearly_allowance_increment'));
		redirect(site_url('others/increment'));
	}
	
	public function add_vetan_shreni(){
	
		if($this->input->post()){
			
			foreach($this->input->post() as $key=>$val){
			
				$arr[$key]	=	$val;
			}
			
			$this->main->add_item('vetan_shreni',$arr);
			$this->session->set_flashdata('success_msg','Success! Record added successfully.');
			redirect(site_url('others/vetan_shreni'));
		}	
	}
}
?>