<?php
class Pay_slip extends HOME_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model("main_model","main");		

		if(!$this->session->userdata('is_admin_login')){
			redirect('home');
		}	
	}
	
	public function index(){

		$data['menu']			=	'pay_slip';
		$data['sub_menu']		=	'pay_slip';
		$data['department']		=	$this->main->get_department_list();
		$this->layout->view('pay_slip/pay_slip_entry',$data);
	}

	public function pay_slip_synchronization(){

		$data['menu']			=	'pay_slip';
		$data['sub_menu']		=	'pay_slip_synchronization';
		$data['status']			=	$this->main->get_synchronization_status();
		$this->layout->view('pay_slip/pay_slip_synchronization',$data);
	}

	public function start_synchronization(){
		
		if(date('d')<20){
			echo 0;
		}
		else {
			$this->main->pay_slip_synchronization();
			echo 1;
		}
	}

	public function get_employee(){
		
		if($this->input->post()){
			$arr	=	$this->main->get_employee_by_department_id2($this->input->post('department_id'));
			if($arr	!= ''){
				echo '<select name="employee_id" id="employee_id" onchange=get_employee_details(this.value);>
						<option></option>';
						foreach($arr as $employee){
							echo "<option value='".$employee['employee_id']."'>".$employee['name']."</option>";	
						}
				echo '</select>';
			}
		}
	}

	public function get_employee_details(){
		$arr	=	$this->main->get_employee_details($this->input->post('employee_id'));
		
		$str	=	"{";

		$str	.=	'"designation_name":"'.$arr->designation_name.'",';
		$str	.=	'"bank_name":"'.$arr->bank_name.'",';
		$str	.=	'"branch":"'.$arr->branch.'",';
		$str	.=	'"account_number":"'.$arr->bank_account_number.'",';
		$str	.=	'"six_pay_band":"'.$arr->six_min_salary.'-'.$arr->six_max_salary.'-'.$arr->six_grade_pay.'"';

		$str	.=	"}";
		
		echo $str;
	}
}
?>