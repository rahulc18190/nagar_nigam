<?php
class Report extends HOME_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model("main_model","main");		

		if(!$this->session->userdata('is_admin_login')){
			redirect('home');
		}	
		$this->load->library('pagination');		

		$config['full_tag_open'] 	= '<ul class="pagination pagination-sm no-margin pull-right">';
		$config['full_tag_close'] 	= '</ul>';
		$config['first_link'] 		= 'First';
		$config['first_tag_open'] 	= '<li>';
		$config['first_tag_close'] 	= '</li>';
		$config['last_link'] 		= 'Last';
		$config['last_tag_open'] 	= '<li>';
		$config['last_tag_close'] 	= '</li>';
		$config['next_link'] 		= 'Next &gt;';
		$config['next_tag_open'] 	= '<li>';
		$config['next_tag_close'] 	= '</li>';
		$config['prev_link'] 		= '&lt; Prev';
		$config['prev_tag_open'] 	= '<li>';
		$config['prev_tag_close'] 	= '</li>';
		$config['cur_tag_open'] 	= '<li class="active"><a href="#">';
		$config['cur_tag_close'] 	= '</a></li>';
		$config['num_tag_open'] 	= '<li>';
		$config['num_tag_close'] 	= '</li>';
		$this->pagination->initialize($config);
	}
	
	public function index(){
		$this->layout->view('index');
	}

	public function reports(){

		$this->session->unset_userdata('report');

		$data['menu']			=	'reports';
		$data['sub_menu']		=	'reports';
		$data['bank']			=	$this->main->get_bank_list_group_by();
		$data['department']		=	$this->main->get_department_list();
		$data['employee']		=	$this->main->get_employee_list();
		$this->layout->view('report/reports',$data);
	}

	public function get_employee(){
	
		if($this->input->post()){
			$arr	=	$this->main->get_employee_by_department_id($this->input->post('department_id'));
			if($arr	!= ''){
				echo '<select name="employee_id_pay_sleep_report" id="employee_id_pay_sleep_report">
                        <option value="All">lHkh deZpkjh </option>';

						foreach($arr as $employee){
							echo "<option value='".$employee['employee_id']."'>".$employee['name']."</option>";	
						}
				echo '</select>';
			}
		}
	}

	public function get_report(){

		// Bank Report Started

		if($this->input->post('report')	==	'bank_report' || $this->session->userdata('report') == 'bank_report'){

			if($this->input->post()){
				
				$this->session->set_userdata('report',$this->input->post('report'));
				$this->session->set_userdata('bank_id',$this->input->post('bank_id_bank_report'));
				$this->session->set_userdata('month',$this->input->post('month_bank_report'));
			}	
			
			$config['base_url'] 	= site_url('report/get_report');
			$config['total_rows'] 	= $this->main->get_bank_report_count($this->session->userdata('bank_id'),
																		$this->session->userdata('month'));
			$config['per_page'] = 30;
			
			$this->pagination->initialize($config);
			
			$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
	 
			$data["count"] 			= 	$this->uri->segment(3)+1;
			$data["links"] 			= 	$this->pagination->create_links();
			$data['menu']			=	'reports';
			$data['sub_menu']		=	'reports';
			$data['month']			=	$this->session->userdata('month');
			$data['bank']			=	$this->main->get_bank_name($this->session->userdata('bank_id'));
			$data['branch']			=	$this->main->get_branch_name($this->session->userdata('bank_id'));
			$data['bank_report']	=	$this->main->get_bank_report($this->session->userdata('bank_id'),
																	$this->session->userdata('month'),
																	$config["per_page"], $page);
			$this->layout->view('report/bank_report',$data);		
		}

		// Bank Report Ended

		// Department (Employee) Report Started

		elseif($this->input->post('report')	==	'department_report' || $this->session->userdata('report') == 'department_report'){

			if($this->input->post()){
				
				$this->session->set_userdata('report',$this->input->post('report'));
				$this->session->set_userdata('department_id',$this->input->post('department_id_department_report'));
				$this->session->set_userdata('month',$this->input->post('month_department_report'));
			}	
			
			$config['base_url'] 	= site_url('report/get_report');
			$config['total_rows'] 	= $this->main->get_department_report_count($this->session->userdata('department_id'),
																		$this->session->userdata('month'));
			$config['per_page'] = 30;
			
			$this->pagination->initialize($config);
			
			$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
	
			$data["count"] 				= 	$this->uri->segment(3)+1;
			$data["links"] 				= 	$this->pagination->create_links();
			$data['menu']				=	'reports';
			$data['sub_menu']			=	'reports';
			$data['month']				=	$this->session->userdata('month');
			$data['department']			=	$this->main->get_department_name($this->session->userdata('department_id'));
			$data['department_report']	=	$this->main->get_department_report($this->session->userdata('department_id'),$this->session->userdata('month'),$config["per_page"], $page);
			$this->layout->view('report/department_report',$data);
		}

		// Department (Employee) Report Ended
		
		// Department Report Started

		elseif($this->input->post('report')	==	'department_report_2' || $this->session->userdata('report') == 'department_report_2'){

			if($this->input->post()){
				
				$this->session->set_userdata('report',$this->input->post('report'));

				if($this->input->post('vetak_patrak_department_report_2')	==	1){
					$this->session->set_userdata('vetak_patrak',true);
				}
				else {
					$this->session->unset_userdata('vetak_patrak');
				}
				
				$this->session->set_userdata('department_id',$this->input->post('department_department_report_2'));
				$this->session->set_userdata('month',$this->input->post('month_department_report_2'));
			}	
			
			$config['base_url'] 	= site_url('report/get_report');
			$config['total_rows'] 	= $this->main->get_department_report2_count(
																	$this->session->unset_userdata('vetak_patrak'),
																	$this->session->userdata('department_id'),
																	$this->session->userdata('month'));
			$config['per_page'] = 30;
			
			$this->pagination->initialize($config);
			
			$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
	
			$data["count"] 				= 	$this->uri->segment(3)+1;
			$data["links"] 				= 	$this->pagination->create_links();
			$data['menu']				=	'reports';
			$data['sub_menu']			=	'reports';
			$data['month']				=	$this->session->userdata('month');

			$data['department_report2']	=	$this->main->get_department_report2(
			
																	$this->session->unset_userdata('vetak_patrak'),
																	$this->session->userdata('department_id'),
																	$this->session->userdata('month'),
																	$config["per_page"], $page);
			$this->layout->view('report/department_report2',$data);
		}

		// Department Report Ended
		
		// Pay Sleep Report Started

		elseif($this->input->post('report')	==	'pay_sleep_report' || $this->session->userdata('report') == 'pay_sleep_report'){

			if($this->input->post()){
				
				$this->session->set_userdata('report',$this->input->post('report'));
				$this->session->set_userdata('department_id',$this->input->post('department_id_pay_sleep_report'));
				$this->session->set_userdata('employee_id',$this->input->post('employee_id_pay_sleep_report'));
				$this->session->set_userdata('month',$this->input->post('month_pay_sleep_report'));
			}	
			
			$config['base_url'] 	= site_url('report/get_report');
			$config['total_rows'] 	= $this->main->get_pay_sleep_report_count(
																$this->session->userdata('department_id'),
																$this->session->userdata('employee_id'),
																$this->session->userdata('month'));
			
			$config['per_page'] = 5;
			
			$this->pagination->initialize($config);
			
			$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
	
			$data["count"] 				= 	$this->uri->segment(3)+1;
			$data["links"] 				= 	$this->pagination->create_links();
			$data['menu']				=	'reports';
			$data['sub_menu']			=	'reports';
			$data['month']				=	$this->session->userdata('month');
			$data['department']			=	$this->main->get_department_name($this->session->userdata('department_id'));
			$data['employee']			=	$this->main->get_employee_name($this->session->userdata('employee_id'));

			$data['pay_sleep_report']	=	$this->main->get_pay_sleep_report(
																$this->session->userdata('department_id'),
																$this->session->userdata('employee_id'),
																$this->session->userdata('month'),$config["per_page"], $page);
			
			$this->layout->view('report/pay_sleep_report',$data);
		}

		// Pay Sleep Report Ended

		
		// Katotra Report Started

		elseif($this->input->post('report')	==	'katotra_report' || $this->session->userdata('report') == 'katotra_report'){

			if($this->input->post()){
				
				$this->session->set_userdata('report',$this->input->post('report'));
				
				if($this->input->post('department_wise_katotra_report')	==	1){
					$this->session->set_userdata('department_wise',true);
				}
				else {
					$this->session->unset_userdata('department_wise');
				}
				
				$this->session->set_userdata('month',$this->input->post('month_katotra_report'));
			}	
			
			$config['base_url'] 	= site_url('report/get_report');
			$config['total_rows'] 	= $this->main->get_katotra_report_count(
																$this->session->userdata('department_wise'),
																$this->session->userdata('month'));
			
			$config['per_page'] = 5;
			
			$this->pagination->initialize($config);
			
			$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
	
			$data["count"] 				= 	$this->uri->segment(3)+1;
			$data["links"] 				= 	$this->pagination->create_links();
			$data['menu']				=	'reports';
			$data['sub_menu']			=	'reports';
			$data['month']				=	$this->session->userdata('month');

			$data['katotra_report']		=	$this->main->get_katotra_report(
																$this->session->userdata('department_wise'),
																$this->session->userdata('month'),$config["per_page"], $page);
			
			if($this->session->userdata('department_wise')){
				
				$this->layout->view('report/katotra_report1',$data);
			}
			else {
			
				$this->layout->view('report/katotra_report2',$data);
			}
		}

		// Katotra Report Ended

		
		// Loan Report Started

		elseif($this->input->post('report')	==	'loan_report' || $this->session->userdata('report') == 'loan_report'){

			if($this->input->post()){
				
				$this->session->set_userdata('report',$this->input->post('report'));
				$this->session->set_userdata('month',$this->input->post('month_loan_report'));
			}	
			
			$config['base_url'] 	= site_url('report/get_report');
			$config['total_rows'] 	= $this->main->get_loan_report_count($this->session->userdata('month'));
			
			$config['per_page'] = 5;
			
			$this->pagination->initialize($config);
			
			$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
	
			$data["count"] 				= 	$this->uri->segment(3)+1;
			$data["links"] 				= 	$this->pagination->create_links();
			$data['menu']				=	'reports';
			$data['sub_menu']			=	'reports';
			$data['month']				=	$this->session->userdata('month');

			$data['loan_report']		=	$this->main->get_loan_report($this->session->userdata('month'),$config["per_page"], $page);
			
			$this->layout->view('report/loan_report',$data);
		}

		// Loan Report Ended

		// Monthly Employee Report Started

		elseif($this->input->post('report')	==	'monthly_employee_report' || $this->session->userdata('report') == 'monthly_employee_report'){

			if($this->input->post()){
				
				$this->session->set_userdata('report',$this->input->post('report'));

				$this->session->set_userdata('department_id',$this->input->post('department_id_monthly_employee_report'));
				
				$this->session->set_userdata('employee_id',$this->input->post('employee_id_monthly_employee_report'));

				$this->session->set_userdata('month_from',$this->input->post('month_from_monthly_employee_report'));
				$this->session->set_userdata('month_to',$this->input->post('month_to_monthly_employee_report'));
			}	
			
			$config['base_url'] 	= site_url('report/get_report');
			$config['total_rows'] 	= $this->main->get_monthly_employee_report_count(
																$this->session->userdata('department_id'),
																$this->session->userdata('employee_id'),
																$this->session->userdata('month_from'),
																$this->session->userdata('month_to'));
			
			$config['per_page'] 	= 20;
			
			$this->pagination->initialize($config);
			
			$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
	
			$data["count"] 						= 	$this->uri->segment(3)+1;
			$data["links"] 						= 	$this->pagination->create_links();
			$data['menu']						=	'reports';
			$data['sub_menu']					=	'other_reports';
			$data['month_from']					=	$this->session->userdata('month_from');
			$data['month_to']					=	$this->session->userdata('month_to');

			$data['monthly_employee_report']	=	$this->main->get_monthly_employee_report_count(
																$this->session->userdata('department_id'),
																$this->session->userdata('employee_id'),
																$this->session->userdata('month_from'),
																$this->session->userdata('month_to'),
																$config["per_page"], $page);
			
			
			$this->layout->view('report/monthly_employee_report',$data);
		}

		// Monthly Employee Report Ended

		// Loan Full Report Started

		elseif($this->input->post('report')	==	'loan_full_report' || $this->session->userdata('report') == 'loan_full_report'){

			if($this->input->post()){
				
				$this->session->set_userdata('report',$this->input->post('report'));
				$this->session->set_userdata('loan_id',$this->input->post('loan_id_loan_full_report'));
				$this->session->set_userdata('month',$this->input->post('month_loan_full_report'));
			}	
			
			$config['base_url'] 	= site_url('report/get_report');
			$config['total_rows'] 	= $this->main->get_loan_full_report_count(
																$this->session->userdata('loan_id'),
																$this->session->userdata('month'));
			
			$config['per_page'] 	= 5;
			
			$this->pagination->initialize($config);
			
			$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
	
			$data["count"] 							= 	$this->uri->segment(3)+1;
			$data["links"] 							= 	$this->pagination->create_links();
			$data['menu']							=	'reports';
			$data['sub_menu']						=	'other_reports';
			$data['month']							=	$this->session->userdata('month');
			$data['loan']							=	$this->main->get_loan_name($this->session->userdata('loan_id'));

			$data['loan_full_report']				=	$this->main->get_loan_full_report(
																$this->session->userdata('loan_id'),
																$this->session->userdata('month'),
																$config["per_page"], $page);
			
			
			//$this->output->enable_profiler(true);
			$this->layout->view('report/loan_full_report',$data);
		}

		// Loan Full Report Ended

		// Allowance Description Report Started

		elseif($this->input->post('report')	==	'allowance_description_report' || $this->session->userdata('report') == 'allowance_description_report'){

			if($this->input->post()){
				
				$this->session->set_userdata('report',$this->input->post('report'));
				$this->session->set_userdata('department_id',$this->input->post('department_id_allowance_description_report'));
				$this->session->set_userdata('month',$this->input->post('month_allowance_description_report'));
			}	
			
			$config['base_url'] 	= site_url('report/get_report');
			$config['total_rows'] 	= $this->main->get_allowance_description_report_count(
																$this->session->userdata('department_id'),
																$this->session->userdata('month'));
			
			$config['per_page'] = 5;
			
			$this->pagination->initialize($config);
			
			$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
	
			$data["count"] 							= 	$this->uri->segment(3)+1;
			$data["links"] 							= 	$this->pagination->create_links();
			$data['menu']							=	'reports';
			$data['sub_menu']						=	'other_reports';
			$data['month']							=	$this->session->userdata('month');

			$data['allowance_description_report']	=	$this->main->get_allowance_description_report(
																$this->session->userdata('department_id'),
																$this->session->userdata('month'),
																$config["per_page"], $page);
			
			
			$this->layout->view('report/allowance_description_report',$data);
		}

		// Allowance Description Report Ended
	}

	public function get_employee2(){
	
		if($this->input->post()){
			$arr	=	$this->main->get_employee_by_department_id($this->input->post('department_id'));
			if($arr	!= ''){
				echo '<select name="employee_id_monthly_employee_report" id="employee_id_monthly_employee_report">
                        <option></option>';
						foreach($arr as $employee){
							echo "<option value='".$employee['employee_id']."'>".$employee['name']."</option>";	
						}
				echo '</select>';
			}
		}
	}

	public function other_reports(){

		$data['menu']			=	'reports';
		$data['sub_menu']		=	'other_reports';
		$data['department']		=	$this->main->get_department_list();
		$data['loan']			=	$this->main->get_loan_list();
		$this->layout->view('report/other_reports',$data);
	}

	function generate_pdf($report_name)
	{
		$this->load->helper(array('My_Pdf'));
		
		if($report_name=="bank_report"){

			$data['month']			=	$this->session->userdata('month');
			$data['bank']			=	$this->main->get_bank_name($this->session->userdata('bank_id'));
			$data['branch']			=	$this->main->get_branch_name($this->session->userdata('bank_id'));
			$data['bank_report']	=	$this->main->get_bank_report($this->session->userdata('bank_id'),
																	$this->session->userdata('month'));
			$pdf_data = $this->load->view('report/pdf/bank_report',$data,true);		
		}
		create_pdf($pdf_data);
	}
	

	function generate_excel($report_name){
				
		if($report_name=="bank_report"){
		
			$bank_report	=	$this->main->get_bank_report($this->session->userdata('bank_id'),$this->session->userdata('month'));

			$excel = "Sr. \t deZpkjh dk uke \t vdkmaV uacj \t osru \t cSad dk uke \t czkap dk uke \n";

			foreach($bank_report as $key=>$val){
			
				$excel	.=	($key+1)." \t ";
				$excel	.=	$val['employee_name']." \t ";
				$excel	.=	$val['bank_account_number']." \t ";
				$excel	.=	$val['current_salary']." \t ";
				$excel	.=	$val['bank_name']." \t ";
				$excel	.=	$val['branch']." \n ";
			}
		}
		elseif($report_name=="department_report"){
		
			$department_report	=	$this->main->get_department_report($this->session->userdata('department_id'),$this->session->userdata('month'));

			$excel = "Sr. \t deZpkjh dk uke \t in dk uke \t osru + Hk�ks \t dVks=k \t Total \n";

			foreach($department_report as $key=>$val){
			
				$salary_plus_allowance	=	$val['special_salary']+
											$val['dearly_allowance']+	
											$val['home_rent_allowance']+	
											$val['city_damage_allowance']+	
											$val['medical_allowance']+	
											$val['interim_relief']+	
											$val['travelling_allowance']+
											$val['current_salary'];
	
				$katotra				=	$val['GPF']+
											$val['GPF_Loan']+	
											$val['family_benifit_fund']+	
											$val['grain_loan']+	
											$val['professional_tax']+	
											$val['bima_premium']+	
											$val['CTD']+	
											$val['mo_cycle_loan']+	
											$val['cycle_loan']+	
											$val['house_loan']+	
											$val['napa_bhandar']+	
											$val['harijan_pedi']+	
											$val['home_rent']+	
											$val['water_rent']+	
											$val['electricity_rent'];	

				$excel	.=	($key+1)." \t ";
				$excel	.=	$val['employee_name']." \t ";
				$excel	.=	$val['name']." \t ";
				$excel	.=	$salary_plus_allowance." \t ";
				$excel	.=	$katotra." \t ";
				$excel	.=	($salary_plus_allowance-$katotra)." \n ";
			}
		}

		header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
		header("Content-Disposition: attachment; filename=".$report_name."_".$this->session->userdata('month').".xls");
		echo $excel;
	}
}
?>