<?php
class Data_entry extends HOME_Controller {

	public function __construct(){
		parent::__construct();

		if(!$this->session->userdata('is_admin_login')){
			redirect('home');
		}	

		$this->load->model("main_model","main");		
		$this->load->library('pagination');		

		$config['full_tag_open'] 	= '<ul class="pagination pagination-sm no-margin pull-right">';
		$config['full_tag_close'] 	= '</ul>';
		$config['first_link'] 		= 'First';
		$config['first_tag_open'] 	= '<li>';
		$config['first_tag_close'] 	= '</li>';
		$config['last_link'] 		= 'Last';
		$config['last_tag_open'] 	= '<li>';
		$config['last_tag_close'] 	= '</li>';
		$config['next_link'] 		= 'Next &gt;';
		$config['next_tag_open'] 	= '<li>';
		$config['next_tag_close'] 	= '</li>';
		$config['prev_link'] 		= '&lt; Prev';
		$config['prev_tag_open'] 	= '<li>';
		$config['prev_tag_close'] 	= '</li>';
		$config['cur_tag_open'] 	= '<li class="active"><a href="#">';
		$config['cur_tag_close'] 	= '</a></li>';
		$config['num_tag_open'] 	= '<li>';
		$config['num_tag_close'] 	= '</li>';
		$this->pagination->initialize($config);
	}
	
	public function index(){
		$this->layout->view('index');
	}

/*	public function update_date(){

		$data['bank']			=	$this->main->update_date();
	}*/

	public function add_bank(){

		$data['menu']			=	'data_entry';
		$data['sub_menu']		=	'bank';
		$data['bank']			=	$this->main->get_bank_list_group_by();
		$this->layout->view('data_entry/add_bank',$data);
	}

	public function view_bank(){
	
/*		$config['base_url'] = site_url('data_entry/view_bank');
		$config['total_rows'] = $this->main->get_bank_count();
		$config['per_page'] = 15;
		
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

		$data["count"] 			= 	$this->uri->segment(3)+1;
		$data["links"] 			= 	$this->pagination->create_links();*/
		
		$data['menu']			=	'data_entry';
		$data['sub_menu']		=	'bank';
		$data['bank']			=	$this->main->get_bank_list();
		$this->layout->view('data_entry/view_bank',$data);
	}

	public function add_department(){
		$data['menu']			=	'data_entry';
		$data['sub_menu']		=	'department';
		$this->layout->view('data_entry/add_department',$data);
	}

	public function view_department(){
	
/*		$config['base_url'] = site_url('data_entry/view_department');
		$config['total_rows'] = $this->main->get_department_count();
		$config['per_page'] = 15;
		
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

		$data["count"] 			= 	$this->uri->segment(3)+1;
		$data["links"] 			= 	$this->pagination->create_links();
*/		
		$data['menu']			=	'data_entry';
		$data['sub_menu']		=	'department';
		$data['department']		=	$this->main->get_department_list();
		$this->layout->view('data_entry/view_department',$data);
	}

	public function add_designation(){

		$data['menu']			=	'data_entry';
		$data['sub_menu']		=	'designation';
		$data['six_pay_scale']	=	$this->main->get_six_pay_scale_list();
		$data['department']		=	$this->main->get_department_list();
		$data['class']			=	$this->main->get_class_list();
		$this->layout->view('data_entry/add_designation',$data);
	}

	public function get_vetan_shreni(){

		$vetan_shreni			=	$this->main->get_vetan_shreni($this->input->post('six_pay_grade_id'));
		
		$str	=	"{"; 
		
		$str	.=	'"minimum_salary":"'.$vetan_shreni->minimum_salary.'",';
		$str	.=	'"increment":"'.$vetan_shreni->increment.'",';
		$str	.=	'"maximum_salary":"'.$vetan_shreni->maximum_salary.'",';
		$str	.=	'"increment_2":"'.$vetan_shreni->increment_2.'",';
		$str	.=	'"maximum_salary_2":"'.$vetan_shreni->maximum_salary_2.'"';

		$str	.=	"}"; 
		
		echo $str;
	}

	public function view_designation(){
	
		if($this->session->userdata('expire2') && time()>$this->session->userdata('expire2')){
			$this->session->unset_userdata('expire2');
			$this->session->unset_userdata('department_id2');
		}				

		$data['department']		=	$this->main->get_department_list();
		$data['menu']			=	'data_entry';
		$data['sub_menu']		=	'designation';
		$this->layout->view('data_entry/view_designation',$data);
	}

	public function add_employee($id = 0){
	
		$data['menu']				=	'data_entry';
		$data['sub_menu']			=	'employee';
		$data['designation']		=	$this->main->get_designation_list();
		$data['department']			=	$this->main->get_department_list();
		$data['bank']				=	$this->main->get_bank_list();

		if($id	==	0){
			$data['employee_id']		=	$this->main->get_employee_id();
		}
		else {
			$data['employee_data']		=	$this->main->get_employee_by_id($id);
			$data['branch']				=	$this->main->get_branch_list($data['employee_data']->bank_id);
		}
		
		$this->layout->view('data_entry/add_employee',$data);
	}

	public function get_pay_band(){
		
		echo $this->main->get_pay_band($this->input->post('designation_id'));
	}

	public function view_employee(){
	
		if($this->session->userdata('expire') && time()>$this->session->userdata('expire')){
			$this->session->unset_userdata('expire');
			$this->session->unset_userdata('department_id');
		}				

		$data['department']			=	$this->main->get_department_list();
		$data['menu']				=	'data_entry';
		$data['sub_menu']			=	'employee';
		$this->layout->view('data_entry/view_employee',$data);
	}

	public function add_item($table_name){
		
		if($this->input->post()){
			
			if($table_name	==	'designation'){	
				
				$arr['designation_id']	=	$this->main->get_designation_id();
			}			
			else if($table_name	==	'bank'){	
				
				$arr['bank_id']			=	$this->main->get_bank_id();
			}
			else if($table_name	==	'department'){	
				
				$arr['department_id']	=	$this->main->get_department_id();
			}
			else if($table_name	==	'employee'){	
				
				$arr['employee_id']	=	$this->main->get_employee_id();
			}
			else if($table_name	==	'employee_loan'){	
				
				$arr['month']	=	date('F-Y');
			}
			
			foreach($this->input->post() as $key=>$val){
			
				if($key	==	'operation' || $key	==	'bankid' || $key	==	'submit'){

					continue;
				}
				elseif($key	==	'date_of_joining' || $key	==	'date_of_birth'){
				
					$arr[$key]	=	date('Y-m-d',strtotime($val));
					continue;
				}
				elseif($key	==	'name_prefix'){
					
					continue;
				}
				elseif($key	==	'name'){
					
					$arr[$key]	=	$this->input->post('name_prefix')." ".$val;
					continue;
				}

				$arr[$key]	=	$val;
			}
			
			$this->main->add_item($table_name,$arr);
			$this->session->set_flashdata('success_msg','Success! Record added successfully.');
			redirect(site_url('data_entry/add_'.$table_name));
		}	
	}

	public function edit_employee(){
		
		if($this->input->post()){
			
			foreach($this->input->post() as $key=>$val){
			
				if($key	==	'operation' || $key	==	'bankid' || $key	==	'submit'){

					continue;
				}
				elseif($key	==	'date_of_joining' || $key	==	'date_of_birth'){
				
					$arr[$key]	=	date('Y-m-d',strtotime($val));
					continue;
				}
				elseif($key	==	'name_prefix'){
					
					continue;
				}
				elseif($key	==	'name'){
					
					$arr[$key]	=	$this->input->post('name_prefix')." ".$val;
					continue;
				}

				$arr[$key]	=	$val;
			}
			
			$this->main->edit_employee($arr);
			$this->session->set_flashdata('success_msg','Success! Record updated successfully.');
			redirect(site_url('data_entry/view_employee'));
		}	
	}

	public function get_branch(){
		
		if($this->input->post()){
			$arr	=	$this->main->get_branch($this->input->post('bank_name'));
			if($arr	!= ''){
				echo '<select name="bank_id" id="bank_id">
						<option></option>';
						foreach($arr as $bank){
							echo "<option value='".$bank['bank_id']."'>".$bank['branch']."</option>";	
						}
				echo '</select>';
			}
		}
	}
	
	public function get_designation(){
		
		if($this->input->post()){
			$arr	=	$this->main->get_designation($this->input->post('department_id'));
			if($arr	!= ''){
				echo '<select name="designation_id" id="designation_id" onchange="get_pay_band(this.value)">
						<option></option>';
						foreach($arr as $designation){
							echo "<option value='".$designation['designation_id']."'>".$designation['name']."</option>";	
						}
				echo '</select>';
			}
		}
	}
	
	public function add_employee_loan() {
	
		//$data['menu']			=	'data_entry';
		//$data['sub_menu']		=	'employee';
		$data['employee']		=	$this->main->get_full_employee_list();
		$data['loan']			=	$this->main->get_loan_list();
		$this->layout->view('data_entry/add_employee_loan',$data);
	}
	
	public function get_employee(){
		
/*		$config['base_url'] = site_url('data_entry/view_employee');
		$config['total_rows'] = $this->main->get_employee_count();
		$config['per_page'] = 15;
		
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

		$data["count"] 			= 	$this->uri->segment(3)+1;
		$data["links"] 			= 	$this->pagination->create_links();
		$data['employee']		=	$this->main->get_employee_list($config["per_page"], $page);
*/
		$this->session->set_userdata('department_id',$this->input->post('department_id'));

		if($this->input->post('status')	==	1){
			$this->session->set_userdata('expire',time()+30*60);
		}				

		$data['employee']		=	$this->main->get_employee_by_department_id($this->session->userdata('department_id'));
		
		$this->load->view('data_entry/ajax-employee-list',$data);
	}
	
	public function get_designation_by_department_id(){

/*		$config['base_url'] = site_url('data_entry/view_designation');
		$config['total_rows'] = $this->main->get_designation_count();
		$config['per_page'] = 15;
		
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

		$data["count"] 			= 	$this->uri->segment(3)+1;
		$data["links"] 			= 	$this->pagination->create_links();
		$data['designation']	=	$this->main->get_designation_list($config["per_page"], $page);
*/
		$this->session->set_userdata('department_id2',$this->input->post('department_id'));

		if($this->input->post('status')	==	1){
			$this->session->set_userdata('expire2',time()+30*60);
		}				

		$data['designation']		=	$this->main->get_designation_by_department_id($this->session->userdata('department_id2'));
		
		$this->load->view('data_entry/ajax-designation-list',$data);
	}
	
	public function delete_item(){
	
		echo $this->main->delete_item($this->input->post('id'),$this->input->post('table'));
	}
}
?>