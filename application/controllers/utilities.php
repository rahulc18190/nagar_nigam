<?php
class Utilities extends HOME_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model("main_model","main");		

		if(!$this->session->userdata('is_admin_login')){
			redirect('home');
		}	
	}
	
	public function index(){

		$this->layout->view('index');
	}

	public function create_user($id=0){

		$data['menu']				=	'utilities';
		$data['sub_menu']			=	'create_user';

		if($id	!= 0){
			$data['user_detail']	=	$this->main->get_user_detail($id);
		}
		
		$this->layout->view('utilities/create_user',$data);
	}

	public function view_user(){

		$data['menu']			=	'utilities';
		$data['sub_menu']		=	'create_user';
		$data['user']			=	$this->main->get_user_list();
		$this->layout->view('utilities/view_user',$data);
	}
	
	public function check_availability(){

		echo $this->main->check_availability($this->input->post('username'));
	}

	public function insert_user($id=0){

		if($this->input->post()){
		
			foreach($this->input->post() as $key=>$val){
				
				if($key	==	'password'){

					$arr[$key]	=	base64_encode($val);
					continue;					
				}
				
				$arr[$key]	=	$val;
			}
		
			$temp=$this->main->create_user($arr,$id);
			
			if($temp == 1){
				if($id	==	0){
					$this->session->set_flashdata("success_msg", "Success! User Created.");
				}
				else {
					$this->session->set_flashdata("success_msg", "Success! User Updated.");
				}
			}
			else{
				$this->session->set_flashdata("failure_msg", "Failure! Unexpected error occured.");
			}
			redirect(site_url('utilities/create_user'));
		}
	}

	function delete_user(){
		echo $this->main->delete_user($this->input->post('id'));
	}

	function change_user_status(){
		echo $this->main->change_user_status($this->input->post('status'),$this->input->post('id'));
	}

	public function change_password(){

		$data['menu']			=	'utilities';
		$data['sub_menu']		=	'change_password';
		$this->layout->view('utilities/change_password',$data);
	}

	public function insert_new_password(){
		
		if($this->input->post()){
		
			$arr=array('old_password'=>$this->input->post('old_password'),'new_password'=>$this->input->post('new_password'));

			$temp=$this->main->change_password($arr);
			
			if($temp == 1){
				$this->session->set_flashdata("success_msg", "Success! Password changed successfully.");
			}
			else{
				$this->session->set_flashdata("failure_msg", "Failure! Incorrect old password.");
			}
			redirect(site_url('utilities/change_password'));
		}
	}
}
?>