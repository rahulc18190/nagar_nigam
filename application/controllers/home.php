<?php
class Home extends HOME_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model("main_model","main");
	}
	 
	public function index(){
		if($this->session->userdata('is_admin_login')){
			redirect('data_entry');
		}	
		$this->layout->view('login');
	}
	
	public function login(){
		if($this->input->post()){
			$this->main->login(array('username'=>$this->input->post('username'),'password'=>base64_encode($this->input->post('password'))));
			redirect(site_url('home'));
		}
	}
	
	public function Deduction(){
		$this->layout->view('data_entry/Deduction-According_Department-New-Form');
	}
	
	public function Pay(){
		$this->layout->view('data_entry/Pay-Sleep-New-Form');
	}
	
	public function logout(){
 
		$this->session->unset_userdata('admin_id');
		$this->session->unset_userdata('admin_username');
		$this->session->unset_userdata('is_admin_login');
		redirect(site_url('home'));
	}
	
	public function developer_login(){
		
		$this->main->login(array('username'=>'admin','password'=>base64_encode('admin')));
		redirect(site_url('home'));
	}
}
?>