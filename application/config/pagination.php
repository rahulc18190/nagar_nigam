<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['num_links'] = 3;

$config["uri_segment"] = 3;

$config['use_page_numbers'] = FALSE;

$config['page_query_string'] = FALSE;

$config['full_tag_open'] = '<p>';

$config['full_tag_close'] = '</p>';

$config['first_link'] = 'First';

$config['first_tag_open'] = '<span>';

$config['first_tag_close'] = '</span>';

$config['last_link'] = 'Last';

$config['last_tag_open'] = '<span>';

$config['last_tag_close'] = '</span>';

$config['next_link'] = '&gt;';

$config['next_tag_open'] = '<span>';

$config['next_tag_close'] = '</span>';

$config['prev_link'] = '&lt;';

$config['prev_tag_open'] = '<span>';

$config['prev_tag_close'] = '</span>';

$config['cur_tag_open'] = '<b>';

$config['cur_tag_close'] = '</b>';

$config['num_tag_open'] = '<span>';

$config['num_tag_close'] = '</span>';

//$config['display_pages'] = FALSE;

